importScripts('https://www.gstatic.com/firebasejs/7.5.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/7.5.0/firebase-messaging.js');
const firebaseConfig = {
    apiKey: "AIzaSyBPERpEbsqj3RkmS5BVc6Ayn0kAiKfzl5w",
    authDomain: "einsurance-81201.firebaseapp.com",
    databaseURL: "https://einsurance-81201.firebaseio.com",
    projectId: "einsurance-81201",
    storageBucket: "einsurance-81201.appspot.com",
    messagingSenderId: "88179175334",
    appId: "1:88179175334:web:c7cf12ffd2685ba69d6967",
    measurementId: "G-NBGBQH1S5Y"
};
firebase.initializeApp(firebaseConfig);
const messaging = firebase.messaging();