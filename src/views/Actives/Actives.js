import React from 'react';

class Active extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            fixedLabel: false,
        }
    }

    changeLabel = () => {
        this.setState({
            fixedLabel: !this.state.fixedLabel,
        });
    }
    render() {
        return(
            <div>
                 <div className="main">
                    <div className={`actives-header ${this.state.fixedLabel? 'label-fixed': ''}`}>Hoạt động</div>
                    <button onClick={this.changeLabel} >CLICK</button>
                </div>
                <div className='active-content'>
                    <div className="active-row">
                        <div><span className="text1">Admin</span><span className="text2"> đã xóa khuyến mãi:</span><span className="text3"> Magna cursus magna.</span></div>
                        <div className="time1">12/10/2019</div>
                    </div>
                    <div className="active-row">
                        <div><span className="text1">Admin</span><span className="text2"> đã xóa khuyến mãi:</span><span className="text3"> Magna cursus magna.</span></div>
                        <div className="time1">12/10/2019</div>
                    </div>
                    <div className="active-row">
                        <div><span className="text1">Admin</span><span className="text2"> đã xóa khuyến mãi:</span><span className="text3"> Magna cursus magna.</span></div>
                        <div className="time1">12/10/2019</div>
                    </div>
                    <div className="active-row">
                        <div><span className="text1">Admin</span><span className="text2"> đã xóa khuyến mãi:</span><span className="text3"> Magna cursus magna.</span></div>
                        <div className="time1">12/10/2019</div>
                    </div>
                    <div className="active-row">
                        <div><span className="text1">Admin</span><span className="text2"> đã xóa khuyến mãi:</span><span className="text3"> Magna cursus magna.</span></div>
                        <div className="time1">12/10/2019</div>
                    </div>
                    <div className="active-row">
                        <div><span className="text1">Admin</span><span className="text2"> đã xóa khuyến mãi:</span><span className="text3"> Magna cursus magna.</span></div>
                        <div className="time1">12/10/2019</div>
                    </div>
                    <div className="active-row">
                        <div><span className="text1">Admin</span><span className="text2"> đã xóa khuyến mãi:</span><span className="text3"> Magna cursus magna.</span></div>
                        <div className="time1">12/10/2019</div>
                    </div>
                    <div className="active-row">
                        <div><span className="text1">Admin</span><span className="text2"> đã xóa khuyến mãi:</span><span className="text3"> Magna cursus magna.</span></div>
                        <div className="time1">12/10/2019</div>
                    </div>
                    <div className="active-row">
                        <div><span className="text1">Admin</span><span className="text2"> đã xóa khuyến mãi:</span><span className="text3"> Magna cursus magna.</span></div>
                        <div className="time1">12/10/2019</div>
                    </div>
                    <div className="active-row">
                        <div><span className="text1">Admin</span><span className="text2"> đã xóa khuyến mãi:</span><span className="text3"> Magna cursus magna.</span></div>
                        <div className="time1">12/10/2019</div>
                    </div>
                    <div className="active-row">
                        <div><span className="text1">Admin</span><span className="text2"> đã xóa khuyến mãi:</span><span className="text3"> Magna cursus magna.</span></div>
                        <div className="time1">12/10/2019</div>
                    </div>
                    <div className="active-row">
                        <div><span className="text1">Admin</span><span className="text2"> đã xóa khuyến mãi:</span><span className="text3"> Magna cursus magna.</span></div>
                        <div className="time1">12/10/2019</div>
                    </div>
                </div>
            </div>
        )
    }
}
export default Active;