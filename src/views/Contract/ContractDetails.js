import React, { Component } from 'react';
import { Row, Col, Input, Button } from 'reactstrap'
import Select from "react-select";
import { SingleDatePicker } from "react-dates";
import EditorText from "../Product/EditorText";
import { transactionServices } from "../../services/TransactionServices";
import { Tab, Tabs, } from 'react-bootstrap'
import ContractDetailHistory from './ContractDetailHistory';
import ConTracDetailFile from './ContractDetailFile';
import { ContractServices } from '../../services/ContractServices';
import {common} from "../../utils/Common";
import { Editor } from '@tinymce/tinymce-react';
import {Toast} from "../../utils/Toast";

class ContractDetails extends Component {
    constructor(props) {
        super(props);
        this.state = {
            date: null,
            focused: false,
            data: null,
            key: 'thongtin',
            title: null,
            provider_title:"nhan_tho",
            cost_per_term:null,
            transaction_code:null,
            due_date:null,
            owner:[],
            beneficiary:[],
            extra_items:[],
            started_date:null,
            disabled:false,
        }
    }
    componentDidMount() {
        this.getTransactionDetails();
    }

    getTransactionDetails = async () => {
        let uid = this.props.match.params.uid;
        let [success, body] = await transactionServices.getTransactionDetails(uid);
        if (success) {
            this.setState({ data: body.data });
        } else {
            if(body.errorCode === 401){
                await common.handleExpiredToken();
            } else {
                Toast.Fail(body && body.message);
            }
        }
    };
    handleChangeInput = (e) => {
        this.setState({ [e.target.name]: e.target.value })
    }
    handleEditorChange = (event) => {
        this.setState({ fullDescription: event.target.getContent() })
    };
    handleSelect = (key) => {
        this.setState({ key });
        window.scroll({ top: 0, left: 0, behavior: 'smooth' });
    }
    buttonSave = async () => {
      let inputFile = document.getElementById('contract-file');
       let data = new FormData();
       try {
        data.append('title',this.state.data.title);
        data.append('product_type',this.state.provider_title);
        data.append('term',this.state.data.cost_per_term);
        data.append('contract_code',this.state.data.transaction_code);
        data.append('due_date',this.state.data.due_date);
        data.append('owner',JSON.stringify(this.state.data.owner));
        data.append('beneficiary',JSON.stringify(this.state.data.beneficiary));
        data.append('extra_items',JSON.stringify(this.state.data.extra_items));
        data.append('started_date',this.state.data.started_date);
       data.append('file_list',inputFile.files[0]);
        let body = await ContractServices.postContractDetailt(data);
        if(body){
         Toast.Success('Soạn hợp đồng thanh cong');
         window.location = '#/contract-list';
        this.updateStatus('signed')
        }else {
         if(body.errorCode === 401){
             await common.handleExpiredToken();
         } else {
             Toast.Fail(body&&body.message);
         }
     }
       } catch (error) {
           console.log(error);
       }
    };
   
    onclickSave = () => {
        if(this.state.data.status === 'paid'){
            return(
                <div className="contract-luu">
                <div className="contract-detail-button" onClick ={this.buttonSave}
                >Lưu
                </div>
            </div>
            )
        }
    
    }

    updateStatus = async (type) => {
        let uid=this.props.match.params.uid;
        let [success, body] = await transactionServices.updateStatusETransaction(uid,type);
        if(success){
            Toast.Success('Change status ok');
        } else {
            if(body.errorCode === 401){
                await common.handleExpiredToken();
            } else {
                Toast.Fail(body && body.message);
            }
        }
    };
    
    render() {
        const { data } = this.state
        return (
            <div>
                {data &&
                    <div>
                        <div>
                            <div className='contract-info'>
                                <div className='d-flex align-items-center'>
                                    <div className='code-title'>Manu life</div>
                                    <div className='contract-status'>{data.transaction_code}</div>
                                </div>
                                <div className='product-name'>{data.title}</div>
                                <div className='product-cost'>{data.cost_per_package}</div>
                                <div className='d-flex align-items-center'>
                                    {/* <div className='product-status'>{data.status}</div> */}
                                </div>
                            </div>
                            <div className="contract-content">
                                <div className="container-fluid">
                                    <Tabs
                                        activeKey={this.state.key} onSelect={this.handleSelect}
                                        className="tab-carrer">
                                        <Tab eventKey={"thongtin"} title="Thông tin hợp đồng" className="contract-title" id="left">
                                            <div className="contract-details">
                                                <Row className="contract-detail-row">
                                                    <Col sm={7} className="contract-detail-col">
                                                        <div className="contract-detail-description">
                                                            <div className='contract-detail-title'>Mô tả</div>
                                                            <Input
                                                                type="textarea"
                                                                className='contract-detail-input'
                                                                name='shortDescription'
                                                                value={data.short_description}
                                                                onChange={this.handleChangeInput}
                                                                placeholder='Thêm mô tả'
                                                                disabled={true}
                                                            />
                                                        </div>

                                                        <div className='contract-detail-title'>Nội dung</div>
                                                         <Editor
                                                                value={data.full_description}
                                                                onChange={this.handleEditorChange}
                                                                height={300}
                                                                disabled={true}
                                                            />
                                                       {/*"chỉnh theo format của css" <div className="contract-full-description" dangerouslySetInnerHTML={{ __html: data.full_description }}></div> */}  
                                                        <div className='contract-content-title'>Gói giá đã chọn</div>
                                                        <div>
                                                            <div className='supplementary-contract-header'>
                                                                <div> Gói 1</div>
                                                            </div>
                                                            <div className='supplementary-product-content'>
                                                                <Row>
                                                                    <Col sm={4}>
                                                                        <div>Giá:</div>
                                                                        <div>Thời gian bảo vệ:</div>
                                                                        <div>Thời gian đóng phí:</div>
                                                                    </Col>
                                                                    <Col sm={8}>
                                                                        <div>{data.cost_per_package} {data.unit}</div>
                                                                        <div>{data.protected_time}</div>
                                                                        <div>{data.paid_time}</div>
                                                                    </Col>
                                                                </Row>
                                                            </div>
                                                        </div>
                                                        {data.extra_items && data.extra_items.length !== 0 && data.extra_items.map((item) => {
                                                            return (
                                                                <div>
                                                                    <div className='contract-content-title'>Sản phẩm bổ trợ</div>
                                                                    <div>
                                                                        <div className='supplementary-contract-header'>
                                                                            <div>{item.title}</div>
                                                                        </div>
                                                                        <div className='supplementary-product-content'>
                                                                            <Row>
                                                                                <Col sm={4}>
                                                                                    <div>Giá:</div>
                                                                                    <div>Mô tả:</div>
                                                                                </Col>
                                                                                <Col sm={8}>
                                                                                    <div>{item.cost} {item.unit}</div>
                                                                                    <div>{item.short_description}</div>
                                                                                </Col>
                                                                            </Row>
                                                                            <a href='#' className='detail-link'>Xem chi tiết</a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            )
                                                        })}
                                                        <Row className="contract-product-row">
                                                            <Col sm={5}>
                                                                <div className='contract-detail-title'>Loại sản phẩm</div>
                                                                <div className="contract-product-title">{data.paid_time}</div>
                                                            </Col>
                                                            <Col sm={7}>
                                                                <div className='contract-detail-title'>Phí mỗi kỳ</div>
                                                                <div className="contract-product-title">{data.cost_per_term}</div>
                                                            </Col>
                                                        </Row>
                                                        {data.owner &&
                                                            <div>
                                                                <div className='contract-content-title'>Chủ sở hữu</div>
                                                                <div className='product-owner'>
                                                                    <Row>
                                                                        <Col sm={4}>
                                                                            {data.owner.full_name && <div>Tên đầy đủ</div>}
                                                                            {data.owner.person_id && <div>CMND/CCCD</div>}
                                                                            {data.owner.gender && <div>Giới tính</div>}
                                                                            {data.owner.full_address && <div>Địa chỉ</div>}
                                                                            {data.owner.date_of_birth && <div>Ngày sinh</div>}
                                                                            {data.owner.phone_number && <div>Số điện thoại</div>}
                                                                            {data.owner.email && <div>Email</div>}
                                                                            {data.owner.occupation && <div>Nghề nghiệp</div>}
                                                                        </Col>
                                                                        <Col sm={8} className='product-owner-detail'>
                                                                            <div>{data.owner.full_name}</div>
                                                                            <div>{data.owner.person_id}</div>
                                                                            <div>{data.owner.gender}</div>
                                                                            <div>{data.owner.full_address}</div>
                                                                            <div>{data.owner.date_of_birth}</div>
                                                                            <div>{data.owner.phone_number}</div>
                                                                            <div>{data.owner.email}</div>
                                                                            <div>{data.owner.occupation}</div>
                                                                        </Col>
                                                                    </Row>
                                                                </div>
                                                            </div>
                                                        }
                                                        {data.beneficiary &&
                                                            <div>
                                                                <div className='contract-content-title'>Người thụ hưởng</div>
                                                                <div className='product-owner'>
                                                                    <Row>
                                                                        <Col sm={4}>
                                                                            {data.beneficiary.full_name && <div>Tên đầy đủ</div>}
                                                                            {data.beneficiary.person_id && <div>CMND/CCCD</div>}
                                                                            {data.beneficiary.gender && <div>Giới tính</div>}
                                                                            {data.beneficiary.full_address && <div>Địa chỉ</div>}
                                                                            {data.beneficiary.date_of_birth && <div>Ngày sinh</div>}
                                                                            {data.beneficiary.phone_number && <div>Số điện thoại</div>}
                                                                            {data.beneficiary.email && <div>Email</div>}
                                                                            {data.beneficiary.occupation && <div>Nghề nghiệp</div>}
                                                                        </Col>
                                                                        <Col sm={8} className='product-owner-detail'>
                                                                            <div>{data.beneficiary.full_name}</div>
                                                                            <div>{data.beneficiary.person_id}</div>
                                                                            <div>{data.beneficiary.gender}</div>
                                                                            <div>{data.beneficiary.full_address}</div>
                                                                            <div>{data.beneficiary.date_of_birth}</div>
                                                                            <div>{data.beneficiary.phone_number}</div>
                                                                            <div>{data.beneficiary.email}</div>
                                                                            <div>{data.beneficiary.occupation}</div>
                                                                        </Col>
                                                                    </Row>
                                                                </div>
                                                            </div>
                                                        }
                                                        <div>
                                                            <div className="contract-list-file">Danh sách file</div>
                                                            <input
                                                                id='contract-file'
                                                                type='file'
                                                            />
                                                        </div>
                                                    </Col>
                                                    <Col sm={5} className="contract-detail-col">
                                                        <div className="contrac-detail-date">
                                                            <div className='contract-detail-title'>Ngày cấp giấy chứng nhận</div>
                                                            <div className='contract-date-time'>{data.owner.date_of_birth} </div>
                                                        </div>
                                                        <div className="contrac-detail-date">
                                                            <div className='contract-detail-title'>Ngày bắt đầu</div>
                                                            <div className='contract-date-time'>{data.started_date}</div>
                                                        </div>
                                                        <div className="contrac-detail-date">
                                                            <div className='contract-detail-title'>Ngày đáo hạn</div>
                                                            <div className='contract-date-time'> {data.due_date}</div>
                                                        </div>
                                                    </Col>
                                                </Row>
                                                {this.onclickSave()}
                                            </div>
                                        </Tab>
                                        <Tab eventKey={"lichsu"} title="Lịch sử hợp đồng" className="contract-title" id="center">
                                            <ContractDetailHistory handleSelect={key => this.handleSelect(key)} />
                                        </Tab>
                                        {/* <Tab eventKey={"danhsach"} title="Danh sách file" className="contract-title" id="right">
                                            <ConTracDetailFile handleSelect={key => this.handleSelect(key)} />
                                        </Tab> */}
                                    </Tabs>
                                </div>
                            </div>
                        </div>

                    </div>
                }
            </div>

        );
    }
}

export default ContractDetails;