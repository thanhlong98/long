import React, { Component } from 'react';
import IMSTable from "../../utils/IMSTable";
import { ContractServices } from '../../services/ContractServices';

class ContractDetailHistory extends Component {
    constructor(props) {
        super(props);
        this.state = {
            columns: [{
                dataField: 'code',
                text: 'Mã giao dịch',
                formatter:this.codeFormatter,
                headerClasses:'header-table-contract-code',
            }, {
                dataField: 'number',
                text: 'Số tiền',
                headerClasses:'header-table-contract-number',
                formatter:this.codeNumber,
            },
            {
                dataField: 'title',
                text: 'Nội dung',
                headerClasses:'header-table-contract-title',
                formatter:this.codeTitle,
            },
            {
                dataField: 'pay',
                text: 'Thanh toán',
                headerClasses:'header-table-contract-pay',
                formatter:this.codePay,
            },
        {
                dataField: 'date',
                text: 'Ngày',
                headerClasses:'header-table-contract-date',
                formatter:this.codeDate,
            }],
            paymentList: null,
            totalRow: null,
            page: 1,
            sizePerPage: 10,

        }
    }
    componentDidMount() {
        this.getPayMent(this.state.page, this.state.sizePerPage);
       
    }

    getPayMent = (page,sizePerPage) => {
        ContractServices.getPayMentList(page, sizePerPage)
            .then(data => {
                let paymentList = data.data.content.map((row, index)=>{
                    return(
                        {
                            code:"fdhiakwj",
                            number:row.amount+row.unit,
                            title:'Chưa chuyển khoản nha Ku',
                            pay:row.payment_method,
                            date: row.paid_at,

                        }
                    )
                });
                this.setState({
                    paymentList: paymentList,
                    totalRow: data.data.totalRows,
                    page: page,
                    sizePerPage: sizePerPage,
                })
            })
            .catch(err => {
                console.log(err);
            })
    };
    codeFormatter = (cell,row, rowIndex) =>{
        return(
            <div>
                     <div className="contractDetail-title">8980809809</div>
            </div>
        )
    }
    codeNumber = (cell,row, rowIndex) =>{
        let number =cell ?cell :null;
        return(
            <div>
                 <div className="contractNumber-title">{number}</div>
            </div>
        )
    }
    codeTitle = (cell,row, rowIndex) =>{
        let title =cell ?cell :null;
        return(
            <div>
                 <div className="contractTitle-title">{title}</div>
            </div>
        )
    }
    codePay = (cell,row, rowIndex) =>{
        let pay =cell ?cell :null;
        return(
            <div>
                 <div className="contractPay-title">{pay}</div>
            </div>
        )
    }
    codeDate = (cell,row, rowIndex) =>{
        let date =cell ?cell :null;
        return(
            <div>
                 <div className="contractDate-title">{date}</div>
            </div>
        )
    }
    // titleFormatter = (cell,row, rowIndex) => {
    //     console.log(row);
    //    let title = cell? cell :null;
    //     return(
    //         <div>
    //                 <div className="title-transaction">{title}</div>
    //         </div>
    //     )
    // };
    handleTableChange = (type, { page, sizePerPage }) => {
        this.getPayMent(page,sizePerPage);
    };
    render() {
        const { paymentList } = this.state;
        return (
            <div>
                {paymentList &&
                    <div className='product-page'>
                        
                        <IMSTable
                            data={this.state.paymentList}
                            columns={this.state.columns}
                            totalRow={this.state.totalRow}
                            page={this.state.page}
                            sizePerPage={this.state.sizePerPage}
                            onTableChange={this.handleTableChange}
                        />
                    </div>
                }
            </div>
        );
    }
}

export default ContractDetailHistory;