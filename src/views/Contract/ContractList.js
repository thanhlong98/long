import React, { Component } from 'react';
import IMSTable from "../../utils/IMSTable";
import {ContractServices} from '../../services/ContractServices';
import {transactionServices} from "../../services/TransactionServices";
import {common} from "../../utils/Common";
import {Toast} from "../../utils/Toast";

class ContractList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            columns: [{
                dataField: 'title',
                text: 'Tên hợp đồng',
                formatter:this.titleFormatter,
                headerClasses:'header-table-contractList-title'
            }, {
                dataField: 'code',
                text: 'Mã hợp đồng',
                headerClasses:'header-table-contractList-code',
                formatter: this.codeFormater,
            }, {
                dataField: 'customer',
                text: 'Khách hàng',
                headerClasses:'header-table-contractList-customer',
               // formatter:this.titleFormatter,
            },
            {
                dataField: 'status',
                text: 'Trạng thái',
                headerClasses:'header-table-contractList-status',
                formatter: this.codeStatus,
            },
        {
                dataField: 'update',
                text: 'Ngày cật nhập',
                headerClasses:'header-table-contractList-update'
            }],
            contractList: null,
            totalRow: null,
            page: 1,
            sizePerPage: 10,

        }
    }

    componentDidMount() {
        this.getContractListDetail(this.state.page, this.state.sizePerPage);
    }
    eTransaction = ()=>{
        let uid=this.props.match.params.uid;
        window.location='#/contract-detail/'+uid;
    }
    getTransactionDetails = async () => {
        let uid=this.props.match.params.uid;
        let [success, body] = await transactionServices.getTransactionDetails(uid);
        if(success){
            this.setState({data: body.data})
        } else {
            if(body.errorCode === 401){
                await common.handleExpiredToken();
            } else {
                Toast.Fail(body && body.message);
            }
        }
    };
    getContractListDetail = (page,sizePerPage) => {
        ContractServices.getContractList(page, sizePerPage)
         .then(data => {
                let contractList = data.data.content.map((contract, index)=>{
                    return(
                        {
                            title: [contract.title],
                             code:[contract.contract_code,contract.uid],
                             customer:'chua co',
                             status:contract.status,
                             update:'chua co',
                        }
                    )
                });
                this.setState({
                    contractList: contractList,
                    totalRow: data.data.totalRows,
                    page: page,
                    sizePerPage: sizePerPage,
                })
            })
            .catch(err => {
                console.log(err);
            })
    };
    titleFormatter = (cell,row, rowIndex) => {
      let title = cell? cell :null;
    
        return(
            <div>

                    <div className="contractList-title" >{title}</div>
            </div>
        )
    };

    codeFormater =(cell,row,rowIndex) =>{
        let code =cell[0]? cell[0] :null;
        let uid=cell[1] ? cell[1] :null;
        return(
            // <div className="contractList-code" onClick={this.eTransaction}>{code}</div>
            <a href={'#/contract-detail/'+ uid} className="contractList-code">{code}</a>

            
        )
    }
    codeStatus =(cell,row,rowIndex) => {
        let status = cell? cell : null;
        return(
            <div className={status+'-status'+' product-status transaction-detail-status'}>{status}</div>
        )
    }
    handleTableChange = (type, { page, sizePerPage }) => {
        this.getContractListDetail(page,sizePerPage);
    };
    render() {
        const { contractList } = this.state;
        return (
            <div>
                {contractList &&
                    <div className='product-page'>
                        <div className="product-content">
                            <div className="product-all"> Hợp đồng </div>
                        </div>
                        <IMSTable
                            data={this.state.contractList}
                            columns={this.state.columns}
                            totalRow={this.state.totalRow}
                            page={this.state.page}
                            sizePerPage={this.state.sizePerPage}
                            onTableChange={this.handleTableChange}
                        />
                    </div>
                }
            </div>
        );
    }
}

export default ContractList;