import React, { Component } from 'react';
import { UserServices } from '../../services/UserServices'
import IMSTable from "../../utils/IMSTable";

class ConTracDetailFile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            columns: [{
                dataField: 'title',
                text: 'Tiêu đề',
                formatter:this.titleFormatter,
                headerClasses:'header-table-title'
            }, {
                dataField: 'update',
                text: 'Cập nhập',
                headerClasses:'header-table-update'
            }, {
                dataField: 'type',
                text: 'Loại',
                headerClasses:'header-table-type'
            }],
            productList: null,
            totalRow: null,
            page: 1,
            sizePerPage: 10,

        }
    }
    componentDidMount() {
        this.getProductList();
    }
    getProductList = () => {
        this.getProductListFollowPageNUmber(this.state.page, this.state.sizePerPage);
    };

    getProductListFollowPageNUmber = (page,sizePerPage) => {
        UserServices.getProductList(page, sizePerPage)
            .then(data => {
                let productList = data.data.content.map((row, index)=>{
                    return(
                        {
                            title:[<img src={row.cover} alt=""/>,row.short_description,row.title,row.uid],
                            update: row.updated_at,
                            type: row.product_type,
                        }
                    )
                });
                this.setState({
                    productList: productList,
                    totalRow: data.data.totalRows,
                    page: page,
                    sizePerPage: sizePerPage,
                })
            })
            .catch(err => {
                console.log(err);
            })
    };

    titleFormatter = (cell,row, rowIndex) => {
        console.log(row);
       let   subTitle= cell[0]?cell[0]: null;
       let  title = cell[1]? cell[1]: null;
       let  thumbnail  = cell[2]? cell[2] :null;
       let productId = cell[3]? cell[3] :null;
        return(
            <div>
                <div className="product-title">
                    <div className="sub-title">{subTitle}</div>
                    <div className="thumbnail-lable">
                        <a href={'/product-details/'+productId} className="thumbnail-title">{thumbnail}</a>
                        <div className="title-lable">{title}</div>
                    </div>
                </div>
            </div>
        )
    };

    handleTableChange = (type, { page, sizePerPage }) => {
        this.getProductListFollowPageNUmber(page,sizePerPage);
    };

    goToAddProduct = () =>{
        window.location = '#/add-product';
    };

    render() {
        const { productList } = this.state;
        return (
            <div>
                {productList &&
                    <div className='product-page'>
                        <div className="product-content">
                            <div className="product-all"> Tất cả sản phẩm </div>
                            <button className="product-add"onClick={this.goToAddProduct}>Thêm mới</button>
                        </div>
                        <IMSTable
                            data={this.state.productList}
                            columns={this.state.columns}
                            totalRow={this.state.totalRow}
                            page={this.state.page}
                            sizePerPage={this.state.sizePerPage}
                            onTableChange={this.handleTableChange}
                        />
                    </div>
                }
            </div>
        );
    }
}

export default ConTracDetailFile;