import React, { Component } from 'react';
import { Row, Col } from 'reactstrap';
import { UserServices } from '../../services/UserServices'

class ProductGroups extends Component {
    constructor(props) {
        super(props);
        this.state = {
            productGroups: null,
            options: ['Chỉnh sửa',
                'Sao chép',
                'Xóa']
        }
    }
    componentDidMount() {
        this.getProductGroups()
    }
    getProductGroups = () => {
        UserServices.getProductGroups()
            .then(data => {
                this.setState({
                    productGroups: data.data
                })
            })
            .catch(err => {
            })
    };
    iconsGroup = () => {
        this.setState({
            options: '1',
        })
    }
    addProductGroups = () => {
        window.location = "#/add-product-group"
    }
    render() {
        const { productGroups } = this.state;
        return (
            <div>
                <div>
                {productGroups &&
                <div>
                     <div className="product-groups">
                     <span className="list-product">Nhóm sản phẩm</span>
                     <button className="product-groups-add" onClick={this.addProductGroups}>Thêm mới</button>
                 </div>
                 <div className="product-groups-content-list">
                 <Row className="groups-row">{
                      productGroups.content.map((product,index) => {
                          return(
                            <Col sm="4"  className="groups-col">
                                <div className="product">
                                    <div className="groups-img">
                                        <img src={require("../../assets/icons/Product-Groups.png")} alt="" width="100%" height="136px"/>
                                    </div>
                                    <div className='supplementary-product'>
                                        <div>{product.description}</div>
                                        <i className='icon-supplementary-product-groups ml-auto'/>
                                    </div>
                                </div>
                             </Col>
                          )
                      })
                     
                 }
                     
                 </Row>
                 </div>
                 </div>

                }
                </div>
               
            </div>
        );
    }
}

export default ProductGroups;