import React, { Component } from 'react';
import { Row, Col, Input } from 'reactstrap';

class AddProductGroups extends Component {
    constructor(props) {
        super(props);
        this.state = {
            titleProductGroups: null,
            shortDescription: null,
            file: null,
        }
    }
    handleChangePDInput = (event) => {
        this.setState({ [event.target.name]: event.target.value })
    };
    handleChangePDInput = (event) => {
        this.setState({ [event.target.name]: event.target.value })
    };
    handleChange = (event) => {
        this.setState({
          file: URL.createObjectURL(event.target.files[0])
        })
      }
    render() {
        return (
            <div>
                <div className="add-product-content">
                    <Row className="add-product-row">
                        <Col sm={6} className="add-product-col">
                            <div className="groups-product">
                                <div className='product-groups-title'>Tên sản phẩm</div>
                                <Input className
                                    defaultValue="ten san pham"
                                    className='product-groups-input'
                                    name='titleProductGroups'
                                    value={this.state.titleProductGroups}
                                    onChange={this.handleChangePDInput}
                                />
                            </div>
                            <div className='product-groups-title'>Mô tả</div>
                            <Input
                                type="textarea"
                                defaultValue="them mo ta"
                                className='product-groups-input'
                                name='shortDescription'
                                value={this.state.shortDescription}
                                onChange={this.handleChangePDInput}
                            />
                        </Col>
                        <Col sm={6} className="add-product-col">
                            <div>
                                <div className='product-add-imgs'>
                                    <div className='product-groups-detail-title'>Ảnh bìa</div>
                                    <div className='product-groups-add-img' onChange={this.handleChange}>Thêm ảnh bìa</div>
                                </div>
                                <img
                                    className='defaul-imgs'
                                    src={require("../../assets/images/defaul.png")} width="100%"
                                    height='196px'
                                />
                            </div>
                        </Col>
                    </Row>
                    <div className="btn-button">
                        <button className="btn-exit">HỦY</button>
                        <button className="btn-add">THÊM</button>
                    </div>
                </div>
            </div>
        );
    }
}

export default AddProductGroups;