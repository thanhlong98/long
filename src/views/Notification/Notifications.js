import React from 'react';
import {NotificationServices} from "../../services/NotificationServices";
import IMSTable from "../../utils/IMSTable";
import moment from "moment";
import {common} from "../../utils/Common";
import {Toast} from "../../utils/Toast";

class Notifications extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            page: 1,
            sizePerPage:10,
            data: null,
            columns: [{
                dataField: 'noti',
                text: 'noti',
                formatter:this.notificationFormatter,
                headerClasses: 'd-none'
            },  {
                dataField: 'update',
                text: 'Cập nhập',
                formatter:this.updateFormatter,
                headerClasses: 'd-none'
            }],
            totalRow: null,
        }
    }
    componentDidMount() {
        this.getNotificationList(this.state.page, this.state.sizePerPage);
        if(this.props.unread > 0) {
            this.props.onRead();
        }
    }

    getNotificationList = async (page,sizePerPage) => {
        let[success, body] = await NotificationServices.getNotificationList(page,sizePerPage);
        if(success){
            let notificationRow = body.data.content.map((row)=>{
                return(
                    {
                        noti: [row.notification_title, row.notification_body, row.sender],
                        update: moment(row.created_date).format('DD/MM/YYYY, HH:mm:ss')
                    }
                )
            });
            this.setState({
                data: notificationRow,
                totalRow: body.data.totalRows,
                page: page,
                sizePerPage: sizePerPage
            })
        } else {
            if(body.errorCode === 401){
                await common.handleExpiredToken();
            } else {
                Toast.Fail(body && body.message);
            }
        }
    };

    handleTableChange =(type, { page, sizePerPage })=>{
        this.getNotificationList( page, sizePerPage)
    };

    notificationFormatter = (cell,row) => {
        let body = cell[1]?cell[1]:null;
        let title = cell[0]?cell[0]:null;
        return(
            <div className='notification-row'>
                <div className='noti-type'>{title}</div>
                <div className='noti-status'>{body} </div>
            </div>
        )
    };

    updateFormatter = (cell) => {
        return <div className='update-notification'>{cell}</div>
    };

    render() {
        const {data, totalRow}=this.state;
        return(
            <div>
                {data &&
                <div>
                    <div className='product-content'>
                    <div className='product-all'>Thông báo</div>
                    </div>
                    <IMSTable
                        data={this.state.data}
                        columns={this.state.columns}
                        totalRow={this.state.totalRow}
                        page={this.state.page}
                        sizePerPage={this.state.sizePerPage}
                        onTableChange={this.handleTableChange}
                    />
                </div>
                }
            </div>
        )
    }
}
export default Notifications;