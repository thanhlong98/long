import React, { Component } from 'react';

class ContactUs extends Component {
    render() {
        return (
            <div>
                <div className="contact-body">
                    <div className="contact-title">Contact Us</div>
                    <div className="contact-detail">12 Best Free Html5 Contact Form & Contact Us Page Templates in 2018</div>
                        <div className="contact-content">A contact page is the best way for website visitors to contact you. Here are 12 of the best free HTML5 contact form & contact us page templates and examples in 2018.</div>
                        <div className="contact-content">More and more people do business online. A website is one of the best ways to display your products or brand. Giving your visitors the ability to contact you easily through a contact page is important to good customer service — web designers must pay attention to contact page design. The contact page is the best way for a visitor to reach out to you.</div>
                    <div className="contact-detail">1. General Inquiry Contact Form — Responsive contact us page design layout</div>
                    <div className="contact-content">Designer: EltonCris</div>
                    <div className="contact-content">Template features:</div>
                        <div className="contact-options">Simple white contact form</div>
                        <div className="contact-options">Fully responsive contact form design</div>
                        <div className="contact-options">Support customization</div>
                        <div className="contact-options">Adaptable to any web-page sidebar</div>
                        <div className="contact-options">Cloned 15547 times</div>
                    

                </div>
            </div>
        );
    }
}

export default ContactUs;