import React from 'react';
import {Col, Input} from "reactstrap";
import DefaultFooter from "../../containers/DefaultLayout/DefaultFooter"
import {UserServices} from "../../services/UserServices";
import {Toast} from "../../utils/Toast";
import {common} from "../../utils/Common";

class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            phoneNumber: null,
            password: null,
            errorPhoneNumber: false,
            errorPassword:false,
            isSubmitting: false,
        }
    }

    handlePhoneNumber = (event) => {
        this.setState({phoneNumber: event.target.value, errorPhoneNumber: false})
    };

    handlePassword = (event) => {
        this.setState({password: event.target.value, errorPassword:false})
    };

    handleSubmit = async (event) => {
        event.preventDefault();
        if(this.state.phoneNumber === null){
            this.setState({errorPhoneNumber: true})
        }
        if(this.state.password === null){
            this.setState({errorPassword: true})
        }
        if (this.state.phoneNumber && this.state.password) {
            this.setState({isSubmitting: true});
            let [success, body] = await UserServices.login(this.state.phoneNumber, this.state.password);
            if(success) {
                this.setState({isSubmitting: false});
                let token = body.data.access;
                let refreshToken = body.data.access;
                localStorage.setItem('token', token);
                localStorage.setItem('refreshToken', refreshToken);
                let [access, data] = await UserServices.getProvider();
                if(access){
                    if(data.data.role_name === 'provider_admin'){
                        window.location = '#/active';
                    } else {
                        Toast.Fail('Bạn không có quyền truy cập vào trang web này');
                    }
                } else {
                    Toast.Fail(body && body.message);
                }
                /*this.setState({phoneNumber: '', password:''});
                window.location = '#/active';*/
            } else {
                if(body&&body.errorCode === 401){
                    Toast.Fail('Tên đăng nhập và mật khẩu không tồn tại');
                } else {
                    Toast.Fail(body && body.message);
                }
                this.setState({isSubmitting: false});
                //this.setState({isSubmitting: false, phoneNumber: '', password:''});

            }
        }
    };
    render() {
        const {errorPhoneNumber, errorPassword, isSubmitting} = this.state;
        return (
            <div className='login-background'>
                <div className='login-content'>
                    <p className='header-text'>Đăng nhập</p>
                    <form onSubmit={this.handleSubmit}>
                        <div className='input-login-content'>
                        <Input
                            className='input-login'
                            placeholder='Tên đăng nhập'
                            value={this.state.phoneNumber}
                            onChange={this.handlePhoneNumber}
                        />
                        { errorPhoneNumber &&
                            <div className='error-text'>Hãy nhập số điện thoại</div>
                        }
                        </div>
                        <div className='input-login-content input-password'>
                        <Input
                            type='password'
                            className='input-login'
                            placeholder='Mật khẩu'
                            value={this.state.password}
                            onChange={this.handlePassword}
                        />
                            { errorPassword &&
                            <div className='error-text'>Hãy nhập password</div>
                            }
                        </div>
                        <a  className="forget-password" href="#/forget-password">Quên mật khẩu</a><br/>
                        <button className='button-submit' disabled={isSubmitting}>ĐĂNG NHẬP</button>
                    </form>
                </div>
                <div className="footer-ims">
               <div className="footer-content">
                    <a className="text" href="#/contacts">Contact Us</a>
                    <a className="text" href="#/helps">Helps</a>
                    <a className="text" href="#/termsofuse">Terms of Use</a>
                    <a className="text-policy" href="#/privacy-policy">Privacy policy </a>
                </div>
            </div>
            </div>
        )
    }

}
export default Login;