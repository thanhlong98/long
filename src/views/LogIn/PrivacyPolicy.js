import React, { Component } from 'react';

class PrivacyPolicy extends Component {
    render() {
        return (
            <div>
               <div className= "body-privacy">
                   <div className="privacy-title">Privacy Policy of the Zalo Academy App</div>
                        <div className="privacy-content">In order to receive information about your Personal Data, the purposes and the parties the Data is shared with, contact the Owner.</div>
                    <div className="privacy-title">Data Controller and Owner</div>
                        <div className="privacy-content">182 Le Dai Hanh , District 11, Ho Chi Minh City, Vietnam nhandv@vng.com.vn</div>
                    <div className="privacy-title">Types of Data collected</div>
                        <div className="privacy-content">The owner does not provide a list of Personal Data types collected.</div>
                        <div className="privacy-content">Other Personal Data collected may be described in other sections of this privacy policy or by dedicated explanation text contextually with the Data collection.</div>
                        <dib className="privacy-content">The Personal Data may be freely provided by the User, or collected automatically when using this Application.</dib>
                        <div className="privacy-content">Any use of Cookies – or of other tracking tools – by this Application or by the owners of third party services used by this Application, unless stated otherwise, serves to identify Users and remember their preferences, for the sole purpose of providing the service required by the User.</div>
                        <div className="privacy-content">Failure to provide certain Personal Data may make it impossible for this Application to provide its services.</div>
                        <div className="privacy-content">Users are responsible for any Personal Data of third parties obtained, published or shared through this Application and confirm that they have the third party’s consent to provide the Data to the Owner.</div>
                    <div className="privacy-title">Mode and place of processing the Data</div>
                        <div className="privacy-content">The Data Controller processes the Data of Users in a proper manner and shall take appropriate security measures to prevent unauthorized access, disclosure, modification, or unauthorized destruction of the Data.</div>
                        <div className="privacy-content">The Data processing is carried out using computers and/or IT enabled tools, following organizational procedures and modes strictly related to the purposes indicated. In addition to the Data Controller, in some cases, the Data may be accessible to certain types of persons in charge, involved with the operation of the site (administration, sales, marketing, legal, system administration) or external parties (such as third party technical service providers, mail carriers, hosting providers, IT companies, communications agencies) appointed, if necessary, as Data Processors by the Owner. The updated list of these parties may be requested from the Data Controller at any time.</div>
                    <div className="privacy-title">Place</div>
                        <div className="privacy-content">The Data is processed at the Data Controller’s operating offices and in any other places where the parties involved with the processing are located. For further information, please contact the Data Controller.</div>
                    <div className="privacy-title">Retention time</div>
                        <div className="privacy-content">The Data is kept for the time necessary to provide the service requested by the User, or stated by the purposes outlined in this document, and the User can always request that the Data Controller suspend or remove the data.</div>
                    <div className="privacy-title">The use of the collected Data</div>
                        <div className="privacy-content">The Personal Data used for each purpose is outlined in the specific sections of this document.</div>
                    <div className="privacy-title">Facebook permissions asked by this Application</div>
                        <div className="privacy-content">This Application may ask for some Facebook permissions allowing it to perform actions with the User’s Facebook account and to retrieve information, including Personal Data, from it.</div>
               </div>
            </div>
        );
    }
}

export default PrivacyPolicy;