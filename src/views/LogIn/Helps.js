import React, { Component } from 'react';

class Helps extends Component {
    render() {
        return (
            <div>
                <div className="helps-body">
                    <div className="helps-title">How to create a how-to article for a help website</div>
                        <div className="helps-content">
                        Before learning how to create a help website itself, you need to know how to write a how-to article. From my personal experience and users’ feedback, StepShot Guides is the best way to create it for a number of reasons. Here, I’d like to present a step-by-step instruction guide on how to create a how-to article using StepShot Guides. In case you use other tools, simply follow the general procedure, although there may be some additional steps needed – owing to AI, StepShot Guides can cope with this task up to 90% faster. Just imagine going through the procedure outlined below, create a long guide, and it has only been half an hour! Impressed? So am I.
                        </div>
                    <div className="helps-title">1. Capture</div>
                        <div className="helps-content">To use StepShot Guides, launch the program on your PC, click “Capture Process” -> “Start” and simply carry out the procedure you need to describe – the app will record all your mouse clicks and operational keyboard clicks. This will only take as long as your procedure requires to be completed – and you immediately receive a screenshot for each of your actions with already-marked (pointed, highlighted) buttons, fields, etc. Just try to do it with StepShot Guides once and you’ll never go back to other methods which require much more time and effort!</div>
                    <div className="helps-title">2. Edit & Improve</div>
                        <div className="helps-content">For the second step, you can edit and improve your guide if needed. You can add arrows, lines, shapes, sequence numbers, highlights, text, crop an image, and blur or recognize text on the image. In addition, it’s possible to edit or add step titles, step descriptions, headings, and any other steps if required. In fact, the built-in image editor in StepShot Guides is one of the best around if you compare it with other similar tools.</div>
                    <div className="helps-title">3. Export & Share</div>
                        <div className="helps-content">Once all the materials for your help support guide are ready, you can choose a format for export. StepShot Guides, even its free version, offers the following exporting and sharing options:</div>
                            <div className="helps-options">StepShot Cloud</div>
                            <div className="helps-options">Word document</div>
                            <div className="helps-options">PDF document</div>
                            <div className="helps-options">PowerPoint</div>
                            <div className="helps-options">Video</div>
                            <div className="helps-options">HTML export</div>
                        <div className="helps-content">That’s what we’re proud of – there are no similar apps offering such a diverse range of exporting and sharing alternatives. Some of these options offer various customizable templates you can use to create a guide in minutes. As for the video, it’s similarly simple to create, and you can also opt for multilingual auto narration in different languages and voices or record your own voice if needed. Also, you can publish your guides directly to the web to share them with others. In terms of help centers, the most outstanding option here is StepShot Cloud – you can use it as a basis for hosting your help pages be it a set of instructions, how-to article, feature descriptions, and so on. Therefore, it is incredibly easy to create and share guides in various formats with StepShot Guides with no need to use several tools to accomplish the task.</div>
               
                </div>
            </div>
        );
    }
}

export default Helps;