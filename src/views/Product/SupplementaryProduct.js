import React from 'react';
import {Button, Col, Input, Modal, ModalBody, ModalFooter, ModalHeader, Row} from "reactstrap";
import EditorText from "./EditorText";

class SupplementaryProduct extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            modal: false,
            productName:'',
            cost:'',
            description:'',
            editorValue:'',
            edit:null,
        }
    }

    deleteSupplementaryProduct = (index) => {
        this.props.onDelete(index)
    };

    duplicateSupplementaryProduct = (index) => {
        this.props.onDuplicate(index)
    };

   editSupplementaryProduct = (index) => {
        this.setState({
            edit: index,
            modal: true,
            productName:this.props.extraProduct[index].title,
            cost:this.props.extraProduct[index].cost,
            description:this.props.extraProduct[index].short_description,
            editorValue:this.props.extraProduct[index].full_description,
        });
    };

    renderSupplementaryProduct = () => {
        return(
            <div>
                {this.props.extraProduct && this.props.extraProduct.length !== 0 &&
                this.props.extraProduct.map((product, index)=>{
                    return (
                        <div>
                            <div className='supplementary-product-header'>
                                <div>{product.title}</div>
                                <div className='d-flex ml-auto'>
                                <i className='icon-supplementary-product icon-edit' onClick={()=>this.editSupplementaryProduct(index)}/>
                                <i className='icon-supplementary-product icon-duplicate' onClick={()=>this.duplicateSupplementaryProduct(index)}/>
                                <i className='icon-supplementary-product icon-delete' onClick={()=>this.deleteSupplementaryProduct(index)}/>
                                </div>
                            </div>
                            <div className='supplementary-product-content'>
                                <Row>
                                    <Col sm={3}>
                                        <div>Giá:</div>
                                        <div>Mô tả:</div>
                                    </Col>
                                    <Col sm={9}>
                                        <div>{product.cost} {product.unit}</div>
                                        <div>{product.short_description}</div>
                                    </Col>
                                </Row>
                                <a href='#' className='detail-link'>Xem chi tiết</a>
                            </div>
                        </div>
                    )
                })
                }
            </div>
        )
    };

    toggle = () => {
        this.setState({modal: !this.state.modal})
    };

    handleChangeModalInput = (event)=>{
        this.setState({[event.target.name]: event.target.value})
    };

    handleDoneButtonClick = ()=>{
        if(this.state.edit !== null){
            this.props.onEdit(this.state.edit,this.state.productName,this.state.cost,this.state.description, this.state.editorValue);
        } else {
            this.props.onDone(this.state.productName, this.state.cost, this.state.description, this.state.editorValue);
        }
        this.setState({
            modal: false,
            edit: null,
            productName: '',
            cost: '',
            description: '',
            editorValue: '',

        })
    };

    handleEditorChange = (event) => {
        this.setState({editorValue: event.target.getContent()})
    };

    render() {
        const {productName,cost,description}=this.state;
        return(
            <div>
                <div className='product-detail-title'>Sản phẩm bổ trợ</div>
                {this.renderSupplementaryProduct()}
                <i className='icon-add' onClick={this.toggle}/>
                <Modal isOpen={this.state.modal} toggle={this.toggle}l>
                    <ModalHeader
                        className='product-modal-header'
                        toggle={this.toggle}
                    >
                        Thêm sản phẩm bổ trợ
                    </ModalHeader>
                    <ModalBody className='supplementary-modal-content'>
                        <Row className='supplementary-row'>
                            <Col sm={4} className='product-detail-title'>Tên sản phẩm</Col>
                            <Col sm={8}>
                                <Input
                                    name='productName'
                                    placeholder = 'Thêm tên sản phẩm bổ trợ'
                                    className='supplementary-modal product-detail-input'
                                    onChange={this.handleChangeModalInput}
                                    value={productName}
                                />
                            </Col>
                        </Row>
                        <Row className='supplementary-row'>
                            <Col sm={4} className='product-detail-title'>Giá</Col>
                            <Col sm={8}>
                                <Input
                                    name='cost'
                                    placeholder='Thêm giá'
                                    className='supplementary-modal product-detail-input'
                                    onChange={this.handleChangeModalInput}
                                    value={cost}
                                />
                            </Col>
                        </Row>
                        <Row className='supplementary-row'>
                            <Col sm={4} className='product-detail-title'>Mô tả</Col>
                            <Col sm={8}>
                                <Input
                                    name='description'
                                    type="textarea"
                                    placeholder='Thêm mô tả'
                                    className='supplementary-modal product-detail-input'
                                    onChange={this.handleChangeModalInput}
                                    value={description}
                                />
                            </Col>
                        </Row>
                        <div className='product-detail-title'>Nội dung</div>
                        <EditorText
                            value={this.state.editorValue}
                            onChange={this.handleEditorChange}
                            height={197}
                        />
                    </ModalBody>
                    <ModalFooter>
                        <Button onClick={this.handleDoneButtonClick}>XONG</Button>
                    </ModalFooter>
                </Modal>
            </div>
        )
    }
}

export default SupplementaryProduct;