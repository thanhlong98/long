import React from 'react';
import {Button, Input} from "reactstrap";

class FeedbackHints extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            focused: false,
            hintRows: this.props.hintRows,
        }
    }
    addHints = () => {
        this.setState({
            hintRows: this.state.hintRows.concat([{ value: "", focus: false }])
        });
    };

    deleteHintsInput = (idx)=> {
        let newHintRows = this.state.hintRows.filter((element, index)=> idx !== index);
        this.setState({hintRows: newHintRows})
    };

    handleHintInputChange = (event, idx) => {
        let newHintRows = [...this.state.hintRows];
        newHintRows[idx].value = event.target.value;
        this.setState({hintRows: newHintRows})
    };
    handleInputFocus = (idx) => {
        let newHintRows = [...this.state.hintRows];
        for(let i = 0; i< newHintRows.length; i++) {
            if(i === idx) {
                newHintRows[i].focus = true;
            } else {
                newHintRows[i].focus = false;
            }
        }
        this.setState({hintRows: newHintRows})
    };

    deleteHintInputValue = (idx) => {
        let newHintRows = [...this.state.hintRows];
        newHintRows[idx].value = '';
        this.setState({hintRows: newHintRows})
    };

    render() {
        return(
            <div className='product-detail-hint'>
                {this.state.hintRows.map((hintRow, idx) => (
                    <div className='hints-row'>
                        <div className='hints-row-input'>
                            <Input
                                onFocus = {()=>this.handleInputFocus(idx)}
                                placeholder='Thêm gợi ý'
                                className='hints-input'
                                value={hintRow.value}
                                onChange = {(event)=>this.handleHintInputChange(event,idx)}
                            />
                            <i
                                className={hintRow.focus ? 'icon-delete-hints' : null}
                                onClick={()=>this.deleteHintInputValue(idx)}
                            />
                        </div>
                        <Button className='button-delete-hints' onClick={()=>this.deleteHintsInput(idx)}>Xóa</Button>
                    </div>
                ))}
                <i className='icon-add' onClick={this.addHints}/>
            </div>
        )
    }
}

export default FeedbackHints;