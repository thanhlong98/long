import React from "react";
import {Button, Col, Input, Row} from "reactstrap";
import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles'
import EditorText from "./EditorText";
import FormControl from "@material-ui/core/FormControl";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Radio from "@material-ui/core/Radio";
import ChooseImage from "./ChooseImage";
import {ProductServices} from "../../services/ProductServices";
import {Toast} from "../../utils/Toast.js";
import IMSDate from "../../utils/IMSDate";
import {common} from "../../utils/Common";
import AddImages from "./AddImages";

const theme = createMuiTheme({
    palette: {
        primary: {main: '#31BB71'},
    },
});

class MotorbikeInsuranceDetails extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            date: null,
            radioValue: null,
            productTitle: null,
            shortDescription:null,
            fullDescription:null,
            compulsoryInsuranceCost1: null,
            compulsoryInsuranceCost2:null,
            compulsoryInsuranceCost3:null,
            voluntaryInsuranceCost1:null,
            voluntaryInsuranceCost2:null,
            minMoneyAccidentInsurance:null,
            maxMoneyAccidentInsurance:null,
            rateAccidentInsurance:null,
            minMoneyFireInsurance:null,
            maxMoneyFireInsurance:null,
            rateFireInsurance:null,
            errorAccident:false,
            errorFire:false,
            data: null,
            images: null,
            photo: null,
        }
    }

    changeMotorbikeInsuranceInput = (event) => {
        this.setState({[event.target.name]: event.target.value})
    };

    changMotorbikeInsuranceEditor = (event) => {
        this.setState({fullDescription: event.target.getContent()})
    };

    handleChangeRadio = (event) => {
        this.setState({radioValue: event.target.value})
    };

    getCompulsoryInsurance = () => {
        let compulsoryInsurances = this.state.data.compulsory_insurance.map((element, index)=>{
            let compulsoryInsurance = {};
            for(let key in element) {
                if (key === 'cost') {
                    compulsoryInsurance.cost = this.state['compulsoryInsuranceCost'+(index+1)]
                }
                if (key === 'uid' ) {
                    compulsoryInsurance.uid = element.uid
                }
            }
            return compulsoryInsurance
        });
        return compulsoryInsurances;
    };

    getVoluntaryInsurance = () => {
        let voluntaryInsurances = this.state.data.voluntary_insurance.map((element, index)=>{
            let voluntaryInsurance = {}
            for(let key in element){
                if(key === 'cost'){
                    voluntaryInsurance.cost = this.state['voluntaryInsuranceCost'+(index+1)];
                }
                if(key === 'uid'){
                    voluntaryInsurance.uid = element.uid;
                }
            }
            return voluntaryInsurance;
        });
        return voluntaryInsurances
    };

    handleSubmit = async () => {
        let thumbnailInput = document.getElementById('choose-motorbike-thumbnail-image');
        let coverInput = document.getElementById('choose-motorbike-cover-image');
        let {date, radioValue, productTitle,shortDescription,fullDescription,compulsoryInsuranceCost1,compulsoryInsuranceCost2,
            compulsoryInsuranceCost3,voluntaryInsuranceCost1,voluntaryInsuranceCost2,minMoneyAccidentInsurance,
            maxMoneyAccidentInsurance,rateAccidentInsurance,minMoneyFireInsurance,maxMoneyFireInsurance,rateFireInsurance, errorAccident, errorFire}=this.state;
        if(date && radioValue && productTitle && shortDescription && fullDescription && compulsoryInsuranceCost1 && compulsoryInsuranceCost2
            && compulsoryInsuranceCost3 && voluntaryInsuranceCost1 && voluntaryInsuranceCost2 && minMoneyAccidentInsurance &&
            maxMoneyAccidentInsurance && rateAccidentInsurance && minMoneyFireInsurance && maxMoneyFireInsurance && rateFireInsurance
            && errorAccident===false && errorFire===false) {
            let compulsoryInsurance = this.getCompulsoryInsurance();
            let voluntaryInsurance = this.getVoluntaryInsurance();
            let accidentInsurance = {
                min_place_no: 1,
                max_place_no: 2,
                min_coverage_value: this.state.minMoneyAccidentInsurance,
                max_coverage_value: this.state.maxMoneyAccidentInsurance,
                rate: this.state.rateAccidentInsurance,
            };

            let fireInsurance = {
                min_place_no: 1,
                max_place_no: 2,
                min_coverage_value: this.state.minMoneyFireInsurance,
                max_coverage_value: this.state.maxMoneyFireInsurance,
                rate: this.state.rateFireInsurance,
            };

            let data = new FormData();
            data.append("provider", this.state.data && this.state.data.provider && this.state.data.provider.provider_uid);
            data.append('title', this.state.productTitle);
            data.append('short_description', this.state.shortDescription);
            data.append('full_description', this.state.fullDescription);
            data.append('active', 'false'); //this.state.radioValue
            data.append('published_date', this.state.date.format ? this.state.date.format('YYYY-MM-DD') : this.state.date);
            data.append('compulsory_insurance', JSON.stringify(compulsoryInsurance));
            data.append('voluntary_insurance', JSON.stringify(voluntaryInsurance));
            data.append('accident_insurance', JSON.stringify(accidentInsurance));
            data.append('fire_insurance', JSON.stringify(fireInsurance));
            data.append("thumbnail", thumbnailInput.files.length !== 0 ? thumbnailInput.files[0] : this.state.data.thumbnail);
            if(this.state.images&&this.state.images.length !== 0) {
                for (let file of this.state.images) {
                    data.append('photos', file);
                }
            } else {
                data.append('photos', this.state.data.photos);
            }
            let [success, body] = await ProductServices.updateMotorbikeInsurance(this.props.match.params.productId,data);
            if (success) {
                Toast.Success('Success');
            } else {
                if(body.errorCode === 401){
                    await common.handleExpiredToken();
                } else {
                    Toast.Fail(body && body.message);
                }
            }
        } else {
            if(errorAccident||errorFire){
                Toast.Fail('Tỷ lệ % lớn hơn 0 và nhỏ hơn 100')
            } else {
                Toast.Fail('Vui lòng nhập đầy đủ thông tin sản phẩm')
            }
        }
    };

    onDateChange = (date) => {
        this.setState({date: date});
    };

    handleRate = (event) => {
        if(event.target.name=== 'rateAccidentInsurance'){
            this.setState({rateAccidentInsurance: event.target.value},()=>{
                if(Number(this.state.rateAccidentInsurance)>100 || Number(this.state.rateAccidentInsurance)<0){
                    this.setState({errorAccident: true,rateAccidentInsurance: null})
                } else {
                    this.setState({errorAccident: false})
                }
            })} else {
            this.setState({rateFireInsurance: event.target.value},()=>{
                if(Number(this.state.rateFireInsurance)>100 || Number(this.state.rateAccidentInsurance)<0){
                    this.setState({errorFire: true,rateFireInsurance: null})
                } else {
                    this.setState({errorFire: false})
                }
            })
        }

    };

    componentDidMount() {
        this.getMotorbikeInsuranceDetail();
    };

    getMotorbikeInsuranceDetail = async () => {
        let [success, body] = await ProductServices.getMotorbikeInsuranceDetail(this.props.match.params.productId);
        if(success){
            this.setState({
                data:body.data,
                radioValue: JSON.stringify(body.data.active),
                date: body.data.published_date,
                minMoneyAccidentInsurance : Number(body.data.accident_insurance.min_coverage_value),
                maxMoneyAccidentInsurance : Number(body.data.accident_insurance.max_coverage_value),
                rateAccidentInsurance: Number(body.data.accident_insurance.rate),
                minMoneyFireInsurance :Number(body.data.fire_insurance.min_coverage_value),
                maxMoneyFireInsurance : Number(body.data.fire_insurance.max_coverage_value),
                rateFireInsurance: Number(body.data.fire_insurance.rate),
                productTitle : body.data.title,
                shortDescription : body.data.short_description,
                fullDescription: body.data.full_description,
                photo: body.data.photos,
            });
            body.data.compulsory_insurance.map((element, index)=>{
                this.setState({['compulsoryInsuranceCost'+(index+1)]: Number(element.cost)})
            });
            body.data.voluntary_insurance.map((element, index)=>{
                this.setState({['voluntaryInsuranceCost'+(index+1)]: Number(element.cost)})
            });
        } else {
            if(body.errorCode === 401){
                await common.handleExpiredToken();
            } else {
                Toast.Fail(body && body.message);
            }
        }
    };

    handleImage = (images) => {
        this.setState({images: images});
    };

    deleteImage = async (index) => {
        let fileNameIndex = this.state.photo[index].search('MotoInsurance/');
        let fileName = this.state.photo[index].slice(fileNameIndex+14);
        let containerType = 'MotoInsurance';
        let [success,body]=await ProductServices.deleteImage(fileName,containerType)
        if(success){
            let newPhoto = [...this.state.photo];
            let photo = newPhoto.filter((e, idx)=> idx!==index);
            this.setState({photo: photo},()=>{Toast.Success('Xoá thành công!')});
        } else {
            if(body.errorCode === 401){
                await common.handleExpiredToken();
            } else {
                Toast.Fail(body && body.message);
            }
        }
    };

    render() {
        const {data,radioValue} = this.state;
        return(
            <ThemeProvider theme={theme}>
                {data &&
                <div className='product-detail'>
                    <Row>
                        <Col lg={7} sm={12}>
                            <div className='product-detail-title'>Tên sản phẩm</div>
                            <Input
                                className='product-detail-input'
                                name='productTitle'
                                value={this.state.productTitle}
                                onChange={this.changeMotorbikeInsuranceInput}
                                placeholder='Thêm tên sản phẩm'
                            />
                            <div className='product-detail-title'>Mô tả</div>
                            <Input
                                type="textarea"
                                className='product-detail-input'
                                name='shortDescription'
                                value={this.state.shortDescription}
                                onChange={this.changeMotorbikeInsuranceInput}
                                placeholder='Thêm mô tả'
                            />
                            <div className='product-detail-title'>Nội dung</div>
                            <EditorText
                                name='fullDescription'
                                value={this.state.fullDescription}
                                onChange={this.changMotorbikeInsuranceEditor}
                                height={246}
                            />
                            <div className='motorbike-insurance-info'>Thông tin bảo hiểm</div>
                            <div className='hint-info'>Tất cả đều được tính toán theo giá trị 1 năm</div>
                            <div className='product-detail-title'>BH TNDS bắt buộc</div>
                            <div className='hint-info'>Hint Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                Curabitur vulputate.
                            </div>
                            <div>
                                {data.compulsory_insurance && data.compulsory_insurance.map((element, index)=>{
                                    return(
                                        <div>
                                            <div className='supplementary-product-header'>
                                                <div>{index+1}. {element.engine_type}</div>
                                            </div>
                                            <div className='mandatory-insurance'>
                                                <Row>
                                                    <Col sm={3}>
                                                        <div>Về người:</div>
                                                        <div>Về tài sản:</div>
                                                        <div className='motorbike-insurance-label'>Giá: (VAT)</div>
                                                    </Col>
                                                    <Col sm={9}>
                                                        <div>{element.person_coverage}</div>
                                                        <div>{element.asset_coverage}</div>
                                                        <Input
                                                            //defaultValue={Number(element.cost)}
                                                            className='product-detail-input motorbike-insurance-input'
                                                            placeholder='Thêm giá'
                                                            name={'compulsoryInsuranceCost'+(index+1)}
                                                            value={this.state['compulsoryInsuranceCost'+(index+1)]}
                                                            onChange={this.changeMotorbikeInsuranceInput}
                                                            type='number'
                                                        />
                                                    </Col>
                                                </Row>
                                            </div>
                                        </div>
                                    )
                                }) }
                            </div>
                            <div className='product-detail-title  mt-5'>BH TNDS tự nguyện</div>
                            <div className='hint-info'>Hint Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                Curabitur vulputate.
                            </div>
                            {data.voluntary_insurance && data.voluntary_insurance.map((element, index)=>{
                                return(
                                    <div>
                                        <div className='supplementary-product-header'>
                                            <div>{index+1}. {element.voluntary_type}</div>
                                        </div>
                                        <div className='mandatory-insurance'>
                                            <Row>
                                                <Col sm={3}>
                                                    <div>Về người:</div>
                                                    <div>Về tài sản:</div>
                                                    <div className='motorbike-insurance-label'>Giá: (VAT)</div>
                                                </Col>
                                                <Col sm={9}>
                                                    <div>{element.person_coverage}</div>
                                                    <div>{element.asset_coverage}</div>
                                                    <Input
                                                        //defaultValue={Number(element.cost)}
                                                        className='product-detail-input motorbike-insurance-input'
                                                        placeholder='Thêm giá'
                                                        name={'voluntaryInsuranceCost'+(index+1)}
                                                        value={this.state['voluntaryInsuranceCost'+(index+1)]}
                                                        onChange={this.changeMotorbikeInsuranceInput}
                                                        type='number'
                                                    />
                                                </Col>
                                            </Row>
                                        </div>
                                    </div>
                                )
                            })}
                            <div className='product-detail-title mt-5'>Bảo hiểm tai nạn người trên xe</div>
                            <Row>
                                <Col lg={4} sm={12} className='pr-0'>
                                    <div className='indemnify-input-label'>Số chỗ tối đa</div>
                                    <div className='indemnify-input-label'>Giá trị BH tối thiểu</div>
                                    <div className='indemnify-input-label'>Giá trị BH tối đa</div>
                                    <div className='indemnify-input-label'>Tỷ lệ % tính phí</div>
                                    {this.state.errorAccident ?
                                        <div className='error-label'/>
                                        :
                                        null
                                    }
                                    <div className='indemnify-input-label'>Ghi chú</div>
                                </Col>
                                <Col lg={8} sm={12} className='pl-0'>
                                    <div className='indemnify-input-label'>2</div>
                                    <Input
                                        className='indemnify-input product-detail-input'
                                        name='minMoneyAccidentInsurance'
                                        value={this.state.minMoneyAccidentInsurance}
                                        onChange={this.changeMotorbikeInsuranceInput}
                                        type='number'
                                    />
                                    <Input
                                        className='indemnify-input product-detail-input'
                                        name='maxMoneyAccidentInsurance'
                                        value={this.state.maxMoneyAccidentInsurance}
                                        onChange={this.changeMotorbikeInsuranceInput}
                                        type='number'
                                    />
                                    <Input
                                        className='indemnify-input product-detail-input'
                                        name='rateAccidentInsurance'
                                        value={this.state.rateAccidentInsurance}
                                        //onChange={this.changeMotorbikeInsuranceInput}
                                        onChange={this.handleRate}
                                        type='number'
                                    />
                                    {this.state.errorAccident ?
                                        <div className='error-message'>Tỷ lệ % lớn hơn 0 và nhỏ hơn 100</div>
                                        :
                                        null
                                    }
                                    <div className='indemnify-text'>Số chỗ tối thiểu * (số tiền nằm trong khoản số tiền
                                        tối thiểu và số tiền tối đa) * tỷ lệ % tính phí
                                    </div>

                                </Col>
                            </Row>
                            <div className='product-detail-title mt-5'>Bảo hiểm cháy nổ</div>
                            <Row>
                                <Col lg={4} sm={12} className='pr-0'>
                                    <div className='indemnify-input-label'>Số chỗ tối thiểu</div>
                                    <div className='indemnify-input-label'>Giá trị BH tối thiểu</div>
                                    <div className='indemnify-input-label'>Giá trị BH tối đa</div>
                                    <div className='indemnify-input-label'>Tỷ lệ % tính phí</div>
                                    {this.state.errorFire ?
                                        <div className='error-label'/>
                                        :
                                        null
                                    }
                                    <div className='indemnify-input-label'>Ghi chú</div>
                                </Col>
                                <Col lg={8} sm={12} className='pl-0'>
                                    <div className='indemnify-input-label'>2</div>
                                    <Input
                                        className='indemnify-input product-detail-input'
                                        name='minMoneyFireInsurance'
                                        value={this.state.minMoneyFireInsurance}
                                        onChange={this.changeMotorbikeInsuranceInput}
                                        type='number'
                                    />
                                    <Input
                                        className='indemnify-input product-detail-input'
                                        name='maxMoneyFireInsurance'
                                        value={this.state.maxMoneyFireInsurance}
                                        onChange={this.changeMotorbikeInsuranceInput}
                                        type='number'
                                    />
                                    <Input
                                        className='indemnify-input product-detail-input'
                                        name='rateFireInsurance'
                                        value={this.state.rateFireInsurance}
                                        onChange={this.handleRate}
                                        type='number'
                                    />
                                    {this.state.errorFire ?
                                        <div className='error-message'>Tỷ lệ % lớn hơn 0 và nhỏ hơn 100</div>
                                        :
                                        null
                                    }
                                    <div className='indemnify-text'>Số chỗ tối thiểu * (số tiền nằm trong khoản số tiền
                                        tối thiểu và số tiền tối đa) * tỷ lệ % tính phí
                                    </div>

                                </Col>
                            </Row>
                        </Col>
                        <Col lg={5} sm={12} className='left-content'>
                            <div className='product-detail-title'>Nhóm sản phẩm</div>
                            <div className='add-product-type'>Mô tô</div>
                            <div className='product-detail-title'>Ngày đăng</div>
                            <IMSDate
                                date={this.state.date}
                                onDateChange={this.onDateChange}
                            />
                            {/*<div className='show-checkbox'>
                                <div className='product-detail-title'>Hiển thị</div>
                                <FormControl>
                                    <RadioGroup
                                        className='mt-3'
                                        onChange={this.handleChangeRadio}
                                        value={radioValue}
                                    >
                                        <FormControlLabel value="false" control={<Radio color="primary"/>}
                                                          label="Ẩn"/>
                                        <FormControlLabel value="true" control={<Radio color="primary"/>}
                                                          label="Hiện"/>
                                    </RadioGroup>
                                </FormControl>
                            </div>*/}
                            {/*<ChooseImage
                                coverInputId='choose-motorbike-cover-image'
                                coverImgId='motorbike-cover-image'
                                thumbnailInputId='choose-motorbike-thumbnail-image'
                                thumbnailImgId='motorbike-thumbnail-image'
                                photo={this.state.photo}
                                thumbnail={data.thumbnail}
                                deleteImage={this.deleteImage}
                                renderImages={this.handleImage}
                            />*/}
                            <AddImages
                                coverInputId='choose-motorbike-cover-image'
                                coverImgId='motorbike-cover-image'
                                thumbnailInputId='choose-motorbike-thumbnail-image'
                                thumbnailImgId='motorbike-thumbnail-image'
                                photo={this.state.photo}
                                thumbnail={data.thumbnail}
                                deleteImage={this.deleteImage}
                                renderImages={this.handleImage}
                            />
                        </Col>
                    </Row>
                    <div className='product-bottom'>
                        <Button className='save-button' onClick={this.handleSubmit}>LƯU</Button>
                    </div>
                </div>
                }
            </ThemeProvider>
        )
    }
}
export default MotorbikeInsuranceDetails;