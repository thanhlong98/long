import React, { Component } from 'react';
import { ProductServices } from '../../services/ProductServices';
import _ from 'lodash';
const CommentItem = (props) => {
    const { data } = props;
    const renderRating = () => {
        let number = 5;
        return (
            <div className="reviewPagination-rating">
                {/* {data.rating} */}

                
                <i className='icons-rating rating-img' />
                <i className='icons-rating rating-img' />
                <i className='icons-rating rating-img' />
            </div>
        )
    }
    return (
        <div className="reviewPagination-product">
            <div className="reviewPagination-product-review">
                <div className="reviewPagination-image">
                    <img src={data.user_avatar} alt="" width='36px'
                        height='36px' />
                </div>
                <div className="reviewPagination-product-title">
                    <div>
                        <div className="reviewPagination-name">Hoang Nguyen</div>
                        <div className="reviewPagination-rating">
                            {renderRating()}
                        </div>
                        <div className="reviewPagination-commet-title">{data.comment}</div>
                    </div>
                </div>
            </div>
            <div className="reviewPagination-update">{data.created_at}</div>
        </div>

    )
}
const pageSize = 10;
class ReviewProductDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            currentPage: 1,
            totalRows: 0,
            data: [],
        }
    };
    componentDidMount() {
        this.getReviewProductPagination(this.state.currentPage);
    };

    getReviewProductPagination = async (currentPage) => {
        try {
            let response = await ProductServices.getReviewList(this.props.uid, currentPage, pageSize);
            if (response.errorCode === 0) {
                let data = _.union(this.state.data, response.data.content)
                this.setState({
                    data: data,
                    currentPage: currentPage,
                    totalRows: response.data.totalRows,
                })
            }
            else {
                
            }
        }
        catch (err) {

        };

    }

    loadMore = () => {
        this.getReviewProductPagination(this.state.currentPage + 1);
    }

    render() {
        const { data, totalRows } = this.state;

        return (
            <div>
                <div>
                    <div className="review-comment">Nhận xét và đánh giá ({totalRows})</div>
                    <div>
                        {data.map((row, index) => {
                            return (
                                <CommentItem
                                    key={row.user_name + index}
                                    data={row}
                                />
                            )
                        })
                        }
                        {
                            data.length < totalRows &&
                            <button onClick={this.loadMore}>Show more</button>
                        }
                    </div>
                </div>
            </div>
        );
    }
}

export default ReviewProductDetail;