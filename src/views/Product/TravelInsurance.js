import React from "react";
import {Button, Col, Input, Row} from "reactstrap";
import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles'
import EditorText from "./EditorText";
import {SingleDatePicker} from "react-dates";
import moment from "moment";
import Switch from "@material-ui/core/Switch";
import FormControl from "@material-ui/core/FormControl";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Radio from "@material-ui/core/Radio";
import ChooseImage from "./ChooseImage";

const theme = createMuiTheme({
    palette: {
        primary: {main: '#31BB71'},
    },
});

class TravelInsurance extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            date: null,
            focused: false,
            radioValue: null,
            showProduct: false,
            titleProduct: null,
            shortDescription:null,
            fullDescription:null,
            coverageDescription: null,
            protectTime: null,
        }
    }

    changeMotorbikeInsuranceInput = (event) => {
        this.setState({[event.target.name]: event.target.value})
    };

    changMotorbikeInsuranceEditor = (event) => {
        this.setState({[event.target.name]: event.target.getContent()})
    };

    showHideProduct = () => {
        this.setState({showProduct: !this.state.showProduct})
    };

    handleChangeRadio = (event) => {
        this.setState({radioValue: event.target.value})
    };

    handleCancel = () => {
        window.location = '#/products';
    };

    handleSubmit = () => {
        let data = new FormData();
        // goi api add product
    };

    render() {
        return(
            <ThemeProvider theme={theme}>
                <div className='product-detail'>
                    <Row>
                        <Col lg={7} sm={12}>
                            <div className='product-detail-title'>Tên sản phẩm</div>
                            <Input
                                className='product-detail-input'
                                name='titleProduct'
                                value={this.state.titleProduct}
                                onChange={this.changeMotorbikeInsuranceInput}
                                placeholder='Thêm tên sản phẩm'
                            />
                            <div className='product-detail-title'>Mô tả</div>
                            <Input
                                type="textarea"
                                className='product-detail-input'
                                name='shortDescription'
                                value={this.state.shortDescription}
                                onChange={this.changeMotorbikeInsuranceInput}
                                placeholder='Thêm mô tả'
                            />
                            <div className='product-detail-title'>Nội dung</div>
                            <EditorText
                                name='fullDescription'
                                value={this.state.fullDescription}
                                onChange={this.changMotorbikeInsuranceEditor}
                                height={246}
                            />
                        </Col>
                        <Col lg={5} sm={12} className='left-content'>
                            <div className='product-detail-title'>Nhóm sản phẩm</div>
                            <div className='add-product-type'>Du lịch</div>
                            <div className='product-detail-title'>Ngày đăng</div>
                            <div className='date-time'>
                                <SingleDatePicker
                                    date={this.state.date && moment(this.state.date)}
                                    onDateChange={date => this.setState({date})}
                                    focused={this.state.focused}
                                    onFocusChange={({focused}) => this.setState({focused})}
                                    id="date"
                                    numberOfMonths={1}
                                    placeholder="dd/mm/yyyy"
                                    showDefaultInputIcon={true}
                                    inputIconPosition='after'
                                    displayFormat="DD/MM/YYYY"
                                    isOutsideRange={() => false}
                                />
                            </div>
                            <div className='special-product'>
                                <div className='product-detail-title'>Sản phẩm nổi bật</div>
                                <Switch
                                    className='ml-auto'
                                    checked={this.state.showProduct}
                                    onChange={this.showHideProduct}
                                    color="primary"
                                />
                            </div>
                            <div className='show-checkbox'>
                                <div className='product-detail-title'>Hiển thị</div>
                                <FormControl>
                                    <RadioGroup
                                        className='mt-3'
                                        onChange={this.handleChangeRadio}
                                    >
                                        <FormControlLabel value="Ẩn" control={<Radio color="primary"/>}
                                                          label="Ẩn"/>
                                        <FormControlLabel value="Hiện" control={<Radio color="primary"/>}
                                                          label="Hiện"/>
                                    </RadioGroup>
                                </FormControl>
                            </div>
                            <ChooseImage
                                coverInputId='choose-motorbike-cover-image'
                                coverImgId='motorbike-cover-image'
                                thumbnailInputId = 'choose-motorbike-thumbnail-image'
                                thumbnailImgId = 'motorbike-thumbnail-image'
                            />
                        </Col>
                    </Row>
                    <div className='product-bottom'>
                        <Button className='cancel-button' onClick={this.handleCancel}>HỦY</Button>
                        <Button className='save-button' onClick={this.handleSubmit} >THÊM</Button>
                    </div>
                </div>
            </ThemeProvider>
        )
    }
}
export default TravelInsurance;