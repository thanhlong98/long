import React, { Component } from 'react';
import { UserServices } from '../../services/UserServices'
import IMSTable from "../../utils/IMSTable";
import {Dropdown, DropdownItem, DropdownMenu, DropdownToggle} from "reactstrap";
import {constants} from "../../utils/Constant";
import {Tab, Tabs} from "react-bootstrap";
import moment from "moment";
import {ProductServices} from "../../services/ProductServices";
import {Toast} from "../../utils/Toast.js";
import {common} from "../../utils/Common";

class Product extends Component {
    constructor(props) {
        super(props);
        this.state = {
            columns: [{
                dataField: 'title',
                text: 'Tiêu đề',
                formatter:this.titleFormatter,
                headerClasses:'header-table-title'
            },  {
                dataField: 'update',
                text: 'Cập nhập',
                headerClasses:'header-table-update'
            },],
            productList: null,
            totalRow: null,
            page: 1,
            sizePerPage: constants.PAGE_SIZE,
            productTypes: null,
            filterType: 'nhan_tho',
            isOpen: false,
            key: 'phi nhan tho',
            nIPage: 1,
            nISizePerPage:  constants.PAGE_SIZE,
            nITotalRow: null,
            nIProductList: null,
            nIColumns: [{
                dataField: 'title',
                text: 'Tiêu đề',
                formatter:this.titleFormatter,
                headerClasses:'header-table-title'
            },  {
                dataField: 'update',
                text: 'Cập nhập',
                headerClasses:'header-table-update'
            },],
        }
    }
    componentDidMount() {
        this.getProductListFollowPageNUmber(this.state.page, this.state.sizePerPage,this.state.filterType);
        this.getProductType();
        this.noneLifeInsuranceList(this.state.nIPage, this.state.nISizePerPage);
    }
    
    getProductListFollowPageNUmber = (page,sizePerPage,productType) => {
        UserServices.getProductList(page, sizePerPage, productType)
            .then(data => {
                let productList = data.data.content.map((row, index)=>{
                    return(
                        {
                            title:[<img src={row.cover} alt=""/>,row.short_description,row.title,row.uid],
                            update: moment(row.updated_at).format('DD/MM/YYYY, HH:mm:ss'),
                            type: row.product_type,
                        }
                    )
                });
                this.setState({
                    productList: productList,
                    totalRow: data.data.totalRows,
                    page: page,
                    sizePerPage: sizePerPage,
                })
            })
            .catch(err => {
                console.log(err);
            })
    };

    getProductType = () => {
        UserServices.getProducType()
            .then(data => {
                this.setState({
                    productTypes: data.data.content
                })
            })
            .catch(err => {
            })
    };

    noneLifeInsuranceList = async (page,sizePerPage) => {
        let [success, body] = await ProductServices.getNonLifeInsuranceList(page,sizePerPage);
        if(success){
            let nIProductList = body.data.content.map((row, index)=>{
                return(
                    {
                        title:[<img src={row.thumbnail} alt=""/>,row.short_description,row.title,row.uid],
                        update: moment(row.updated_at).format('DD/MM/YYYY, HH:mm:ss'),
                        type: row.product_type,
                    }
                )
            });
            this.setState({
                nIProductList: nIProductList,
                nITotalRow: body.data.totalRows,
                nIPage: page,
                nISizePerPage: sizePerPage,
            });
        } else {
            if(body && body.errorCode === 401){
                await common.handleExpiredToken();
            } else {
                Toast.Fail(body && body.message);
            }
        }
    };

    titleFormatter = (cell,row, rowIndex) => {
       let   subTitle= cell[0]?cell[0]: null;
       let  title = cell[1]? cell[1]: null;
       let  thumbnail  = cell[2]? cell[2] :null;
       let productId = cell[3]? cell[3] :null;
        return(
            <div>
                <div className="product-title">
                    <div className="sub-title">{subTitle}</div>
                    <div className="thumbnail-lable">
                        <a href={'#/products/'+row.type+'/'+productId} className="thumbnail-title">{thumbnail}</a>
                        <div className="title-lable">{title}</div>
                    </div>
                </div>
            </div>
        )
    };

    handleTableChange = (type, { page, sizePerPage }) => {
        this.getProductListFollowPageNUmber(page,sizePerPage, this.state.filterType);
    };
    handleNITableChange = (type, { page, sizePerPage }) => {
        this.noneLifeInsuranceList(page, sizePerPage);
    };

    goToAddProduct = (type) =>{
        let url = '/products/'+ type;
        this.props.history.push(url)
    };

    setKey = (k) => {
        this.setState({key: k})
    };

    toggle = () => {
        this.setState({isOpen: !this.state.isOpen})
    };

    renderAddProductButton = () => {
        if(this.state.key !== 'nhan tho') {
            return (
                <div className='ml-auto'>
                    <Dropdown isOpen={this.state.isOpen} toggle={this.toggle}>
                        <DropdownToggle className="product-add product-add-layout" >
                            Thêm mới
                            <i className='icon-sidebar icon-dropdown-close'/>
                        </DropdownToggle>
                        <DropdownMenu right className='type-menu'>
                            {
                                this.state.productTypes.filter((productType)=> productType.type !== 'nhan_tho').map((productType, index) => {
                                    return (
                                        <DropdownItem className='type-item' onClick={()=>this.goToAddProduct(productType.type)}>
                                            <div className='d-flex'>
                                                <i className={'icon-type'+' icon_'+productType.type}/>
                                                {productType.description}
                                            </div>
                                        </DropdownItem>
                                    )
                                })
                            }
                        </DropdownMenu>
                    </Dropdown>
                </div>
            )
        } else {
            return (
                <button className="product-add product-add-layout" onClick={()=>this.goToAddProduct(this.state.filterType)}>Thêm mới</button>
            )
        }
    };

    render() {
        const { productList,productTypes, nIProductList} = this.state;
        return (
            <div>
                {/*{productList && productTypes && nIProductList &&*/}
                <div>
                    <div className='product-list-tab'>
                        <div className='add-product-button'>
                            {productTypes&&this.renderAddProductButton()}
                        </div>
                        <Tabs id="controlled-tab-example" activeKey={this.state.key} onSelect={k => this.setKey(k)}>
                            <Tab eventKey="nhan tho" title="Nhân thọ">
                                <div className='product-list-page'>
                                    {productList &&
                                    <IMSTable
                                        data={this.state.productList}
                                        columns={this.state.columns}
                                        totalRow={this.state.totalRow}
                                        page={this.state.page}
                                        sizePerPage={this.state.sizePerPage}
                                        onTableChange={this.handleTableChange}
                                    />
                                    }
                                </div>
                            </Tab>
                            <Tab eventKey="phi nhan tho" title="Phi nhân thọ">
                                <div className='product-list-page'>
                                    {nIProductList &&
                                    <IMSTable
                                        data={this.state.nIProductList}
                                        columns={this.state.nIColumns}
                                        totalRow={this.state.nITotalRow}
                                        page={this.state.nIPage}
                                        sizePerPage={this.state.nISizePerPage}
                                        onTableChange={this.handleNITableChange}
                                    />
                                    }
                                </div>
                            </Tab>
                        </Tabs>
                    </div>
                </div>
               {/* }*/}
            </div>
        );
    }
}

export default Product;