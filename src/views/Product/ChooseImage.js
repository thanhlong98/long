import React from "react";

class ChooseImage extends React.Component{
    constructor(props){
        super(props);
        this.state={
            defaultImage:false,
            images: null,
        }
    }
     handleChange = ()=> {
        this.setState({defaultImage: true});
        let filesInput = document.getElementById(this.props.coverInputId);
        let files = filesInput.files;
        let newImage = this.state.images ? [...this.state.images] : [];
        for (let i = 0; i < files.length; i++) {
            newImage.push(files[i]);
        }
        this.setState({images: newImage},()=>{
             for (let i = 0; i < this.state.images.length; i++) {
                 let file = this.state.images[i];
                 let picReader = new FileReader();
                 picReader.addEventListener("load", function (event) {
                     let picFile = event.target;
                     document.getElementById('image' + (i + 1)).src=picFile.result
                 });
                 picReader.readAsDataURL(file);
             }
            this.props.renderImages(this.state.images)
         });

    };

    handleDeleteImage = (index)=> {
        let newImages = [...this.state.images];
        let files = newImages.filter((element, idx)=> idx!== index);
        this.setState({images: files},()=>{
            for (let i = 0; i < this.state.images.length; i++) {
                let file = this.state.images[i];
                let picReader = new FileReader();
                picReader.addEventListener("load", function (event) {
                    let picFile = event.target;
                    document.getElementById('image' + (i + 1)).src=picFile.result
                });
                picReader.readAsDataURL(file);
            }
            this.props.renderImages(this.state.images)
        })
    };

    renderImage = (inputId, imgId) => {
        let preview = document.getElementById(imgId);
        let file    = document.getElementById(inputId).files[0];
        let reader  = new FileReader();
        reader.addEventListener("load", function () {
            preview.src = reader.result;
        }, false);
        if(file) {
            reader.readAsDataURL(file);
        }
    };

    renderCoverImages = () => {
        if (this.state.images) {
            let images = [];
            for(let i = 0; i < this.state.images.length; i < i++) {
                    images.push (
                            <div>
                                <img id={'image'+(i+1)} className='cover-image' src='' width='100%' height='96px'/>
                                <span className='delete-image'><i className='icon-delete-image' onClick={()=>this.handleDeleteImage(i)}/></span>
                            </div>
                    );
            }
            return images;
        }
    };

    handleImageFromProductDetail = () => {
        let photo = this.props.photo.map((photo, index)=>{
            return <div><img className='cover-image' src={photo} width='100%' height='96px'/>
                <span className='delete-image'><i className='icon-delete-image' onClick={()=>this.props.deleteImage(index)}/></span>
            </div>
        });
        return <div className='out-put-image'>{photo}</div>;
    };

    renderDefaultImage = () =>{
        if(!this.state.defaultImage && this.props.photo === undefined) {
            return <img
                className='default-image ml-3'
                src={require("../../assets/images/image-product.svg")} width='33%'
                height='96px'
                onClick={()=>document.getElementById('cover-image').click()}
            />
        }
    };

    render() {
        return(
            <div>
                <div>
                    <div className='d-flex'>
                        <div className='product-detail-title mr-auto'>Ảnh bìa</div>
                        <label id='cover-image' htmlFor={this.props.coverInputId} className='image-label'>Thêm ảnh</label>
                    </div>
                    <input
                        id={this.props.coverInputId}
                        type="file"
                        multiple
                        onChange={this.handleChange}
                        style={{display: 'none'}}
                    />
                    <br/>
                    <span className='image-content'>
                        {this.props.photo ? this.handleImageFromProductDetail():null}
                        <div className='out-put-image'>
                        {this.renderCoverImages()}
                        </div>
                        {this.renderDefaultImage()}
                    </span>
                </div>
                <div className='mt-4'>
                    <div className='d-flex'>
                        <div className='product-detail-title mr-auto'>Ảnh thumbnail</div>
                        <label id='thumbnail-image' htmlFor={this.props.thumbnailInputId} className='image-label'>Đổi ảnh</label>
                    </div>
                    <input
                        id={this.props.thumbnailInputId}
                        type='file'
                        onChange={() => this.renderImage(this.props.thumbnailInputId, this.props.thumbnailImgId)}
                        style={{display: 'none'}}
                    />
                    <br/>
                    <img
                        className='ml-3'
                        src={this.props.thumbnail ?this.props.thumbnail : require("../../assets/images/image-product.svg")} width='50px'
                        height='50px'
                        id={this.props.thumbnailImgId}
                        onClick={()=>document.getElementById('thumbnail-image').click()}
                    />
                </div>
            </div>
        )
    }
}

export default ChooseImage;