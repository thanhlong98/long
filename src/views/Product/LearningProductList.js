import React, { Component } from 'react';
import { UserServices } from '../../services/UserServices'
import IMSTable from "../../utils/IMSTable";
import {Dropdown, DropdownItem, DropdownMenu, DropdownToggle} from "reactstrap";
import {constants} from "../../utils/Constant";
import moment from 'moment';

class Product extends Component {
    constructor(props) {
        super(props);
        this.state = {
            columns: [{
                dataField: 'title',
                text: 'Tiêu đề',
                formatter:this.titleFormatter,
                headerClasses:'header-table-title'
            },  {
                dataField: 'update',
                text: 'Cập nhập',
                headerClasses:'header-table-update'
            },],
            productList: null,
            totalRow: null,
            page: 1,
            sizePerPage: constants.PAGE_SIZE,
            productTypes: null,
            filterType: 'all-type',
            isOpen: false,
           
        }
    }
    componentDidMount() {
        this.getProductListFollowPageNUmber(this.state.page, this.state.sizePerPage,this.state.filterType);
        this.getProductType();
    }
    
    getProductListFollowPageNUmber = (page,sizePerPage,productType) => {
        UserServices.getProductList(page, sizePerPage, productType)
            .then(data => {
                let productList = data.data.content.map((row, index)=>{
                    return(
                        {
                            title:[<img src={row.cover} alt=""/>,row.short_description,row.title,row.uid],
                            update: moment(row.updated_at).format('DD/MM/YY ,HH:mm:ss') ,
                            type: row.product_type,
                        }
                    )
                });
                this.setState({
                    productList: productList,
                    totalRow: data.data.totalRows,
                    page: page,
                    sizePerPage: sizePerPage,
                })
            })
            .catch(err => {
                console.log(err);
            })
    };

    getProductType = () => {
        UserServices.getProducType()
            .then(data => {
                this.setState({
                    productTypes: data.data.content
                })
            })
            .catch(err => {
            })
    };
    titleFormatter = (cell,row, rowIndex) => {
       let   subTitle= cell[0]?cell[0]: null;
       let  title = cell[1]? cell[1]: null;
       let  thumbnail  = cell[2]? cell[2] :null;
       let productId = cell[3]? cell[3] :null;
        return(
            <div>
                <div className="product-title">
                    <div className="sub-title">{subTitle}</div>
                    <div className="thumbnail-lable">
                        <a href={'#/products/'+productId} className="thumbnail-title">{thumbnail}</a>
                        <div className="title-lable">{title}</div>
                    </div>
                </div>
            </div>
        )
    };

    handleTableChange = (type, { page, sizePerPage }) => {
        this.getProductListFollowPageNUmber(page,sizePerPage, this.state.filterType);
    };

    goToAddProduct = (type) =>{
        let url = '/products/'+ type;
        this.props.history.push(url)
    };

    productFilter = (productType) => {
        this.getProductListFollowPageNUmber(1,constants.PAGE_SIZE,productType);
        this.setState({filterType: productType})
    };

    toggle = () => {
        this.setState({isOpen: !this.state.isOpen})
    };

    renderAddProductButton = () => {
        if(this.state.filterType === 'all-type') {
            return (
                <div className='ml-auto'>
                    {/*<button className="product-add" onClick={this.goToAddProduct}>Thêm mới <i/></button>*/}
                    <Dropdown isOpen={this.state.isOpen} toggle={this.toggle}>
                        <DropdownToggle className="product-add" >
                            Thêm mới
                            <i className='icon-sidebar icon-dropdown-close'/>
                        </DropdownToggle>
                        <DropdownMenu right className='type-menu'>
                            {
                                this.state.productTypes.map((productType, index) => {
                                    return (
                                        <DropdownItem className='type-item' onClick={()=>this.goToAddProduct(productType.type)}>
                                            <div className='d-flex'>
                                                <i className={'icon-type'+' icon_'+productType.type}/>
                                                {productType.description}
                                            </div>
                                        </DropdownItem>
                                    )
                                })
                            }
                        </DropdownMenu>
                    </Dropdown>
                </div>
            )
        } else {
            return (
                <button className="product-add" onClick={()=>this.goToAddProduct(this.state.filterType)}>Thêm mới</button>
            )
        }
    };
    render() {
        const { productList,productTypes } = this.state;
        return (
            <div>
                {productList && productTypes &&
                <div>
                    <div className="product-list-filter">
                        <div className={this.state.filterType === 'all-type' ? "product-list-travel active" : "product-list-travel"} onClick={()=>this.productFilter('all-type')}>
                            Tất cả
                        </div>
                        {
                            productTypes.map((productType, index) => {
                                return (
                                    <div className={this.state.filterType ===productType.type ? "product-list-travel active" : "product-list-travel"} onClick={()=>this.productFilter(productType.type)}>
                                        {productType.description}
                                    </div>
                                )
                            })
                        }
                    </div>
                    <div className='product-list-page'>
                        <div className="product-content">
                            <div className="product-all"> Tất cả sản phẩm</div>
                            { this.renderAddProductButton() }
                        </div>
                        <IMSTable
                            data={this.state.productList}
                            columns={this.state.columns}
                            totalRow={this.state.totalRow}
                            page={this.state.page}
                            sizePerPage={this.state.sizePerPage}
                            onTableChange={this.handleTableChange}
                        />
                    </div>
                </div>
                }
            </div>
        );
    }
}

export default Product;