import React from 'react';
import Select from "react-select";
import {UserServices} from "../../services/UserServices";

class SelectProductType extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            optionType: null
        }
    }

    componentDidMount() {
        this.getProductType();
    }

    getProductType = () => {
        UserServices.getProductGroups()
            .then(data => {
                this.setState({
                    optionType: data.data.content
                })
            })
            .catch(err => {
            })
    };

    getSelectValue = (event) => {
        this.props.onChange(event.value)
    };

    getSelectDefaultValue = () => {
        if(this.props.defaultValue){
            let value = this.props.defaultValue;
            let label = this.state.optionType.filter((option)=>(
                option.type === this.props.defaultValue))[0].description;
            return {value: value, label: label}
        } else {
            return null;
        }

    };

    render() {
        const {optionType}=this.state;
        return(
            <div>
                {optionType &&
                <div className='product-detail-period'>
                    <div className='product-detail-title'>Loại sản phẩm</div>
                    <Select
                        defaultValue={this.getSelectDefaultValue()}
                        options={optionType && optionType.map((optionType, idx) => {
                            return (
                                {value: optionType.type, label: optionType.description}
                            )
                        })}
                        className="multi-select-period"
                        classNamePrefix="select-period"
                        onChange={this.getSelectValue}
                    />
                </div>
                }
            </div>
        )
    }
}
export default SelectProductType;