import React from "react";
import {Button, Col, Input, Row} from "reactstrap";
import EditorText from "./EditorText";
import FormControl from "@material-ui/core/FormControl";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Radio from "@material-ui/core/Radio";
import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles'
import Select from "react-select";
import {SingleDatePicker} from "react-dates";
import moment from "moment";
import Switch from "@material-ui/core/Switch";
import FormGroup from "@material-ui/core/FormGroup";
import {Checkbox} from "@material-ui/core";
import ChooseImage from "./ChooseImage";
import {ProductServices} from "../../services/ProductServices";
import {Toast} from '../../utils/Toast';
import IMSDate from "../../utils/IMSDate";
import {common} from "../../utils/Common";
import AddImages from "./AddImages";

const theme = createMuiTheme({
    palette: {
        primary: {main: '#31BB71'},
    },
});

class HouseInsurance extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            date: null,
            focused: false,
            radioValue: 'true',
            question1: false,
            question2: false,
            indemnify: true,
            productTitle: null,
            shortDescription:null,
            fullDescription:null,
            coverageDescription: null,
            protectTime: null,
            indemnifyMoney: null,
            minMoney: null,
            maxMoney: null,
            houseRate: null,
            houseHint: '',
            innerRate: null,
            innerHint: '',
            data: null,
            errorInnerHouse: false,
            errorHouse: false,
            images: null,
            photo: null,
        }
    }

    handleChangeRadio = (event) => {
        this.setState({radioValue: event.target.value})
    };

    handleCheckBoxChange = (event, name) => {
        this.setState({[name]: event.target.checked})
    };

    handleSelectOptions = () => {
        let options = [];
        for(let i= 1; i<=20; i++){
            options.push({value: i, label: i});
        }
        return options
    };

    indemnifyAssetInsideHouse = () => {
        this.setState({indemnify: !this.state.indemnify})
    };

    changeHouseInsuranceInput = (event) => {
        this.setState({[event.target.name]: event.target.value})
    };

    changeFullDescriptionEditor = (event) => {
        this.setState({fullDescription: event.target.getContent()})
    };

    changeCoverageDescriptionEditor = (event) => {
        this.setState({coverageDescription: event.target.getContent()})
    };

    getProtectTimeValue = (event) => {
        this.setState({protectTime: event.value})
    };

    getIndemnifyMoney = (event) => {
        let indemnifyMoney = event && event.map((element)=>{
            return Number(element.value);
        });
        this.setState({indemnifyMoney: indemnifyMoney})
    };

    handleSubmit = async () => {
        let thumbnailInput = document.getElementById('choose-house-thumbnail-image');
        let coverInput = document.getElementById('choose-house-cover-image');
        let {errorHouse, errorInnerHouse, date, radioValue, productTitle, shortDescription,
            fullDescription, coverageDescription, protectTime ,
            indemnifyMoney, minMoney, maxMoney, houseRate,
            houseHint, innerRate, innerHint}=this.state;
        if(date && radioValue && productTitle && shortDescription && fullDescription && protectTime &&
            minMoney && maxMoney && houseRate && errorHouse===false && errorInnerHouse===false) {
            let data = new FormData();
            data.append('provider', this.state.data && this.state.data.provider && this.state.data.provider.provider_uid);
            data.append("title", this.state.productTitle);
            data.append('short_description', this.state.shortDescription);
            data.append('full_description', this.state.fullDescription);
            data.append('coverage_description', this.state.coverageDescription);
            data.append("active", 'false'); //this.state.radioValue
            data.append("published_date", this.state.date.format ? this.state.date.format('YYYY-MM-DD') : this.state.date);
            data.append("thumbnail", thumbnailInput.files.length !== 0 ? thumbnailInput.files[0] : this.state.data.thumbnail);
            data.append('usage_living', this.state.question1);
            data.append('stable_architecture', this.state.question2);
            data.append('min_year', '1');
            data.append('max_year', this.state.protectTime);
            data.append('min_house_money', this.state.minMoney);
            data.append('max_house_money', this.state.maxMoney);
            data.append('house_rate', this.state.houseRate);
            data.append('house_hint', this.state.houseHint);
            data.append('inner_assets_money', JSON.stringify(this.state.indemnifyMoney));
            data.append('inner_rate', this.state.innerRate);
            data.append('inner_hint', this.state.innerHint);
            data.append('highlight', this.state.indemnify);
            data.append('coverage_description', this.state.coverageDescription);

            if(this.state.images&&this.state.images.length !== 0) {
                for (let file of this.state.images) {
                    data.append('photos', file);
                }
            } else {
                data.append('photos', this.state.photo);
            }

            let [success, body] = await ProductServices.updateHouseInsurance(this.props.match.params.productId, data);
            if (success) {
                Toast.Success('Success');
            } else {
                if(body.errorCode === 401){
                    await common.handleExpiredToken();
                } else {
                    Toast.Fail(body && body.message);
                }
            }
        } else {
            if(errorHouse || errorInnerHouse){
                Toast.Fail('Tỷ lệ % lớn hơn 0 và nhỏ hơn 100')
            } else {
                Toast.Fail('Vui lòng nhập đầy đủ thông tin sản phẩm')
            }
        }
    };
    componentDidMount() {
        this.getHouseInsuranceDetail();
    };

    getHouseInsuranceDetail = async () => {
        let [success, body] = await ProductServices.getHouseInsuranceDetail(this.props.match.params.productId);
        if(success){
            this.setState({
                data: body.data,
                radioValue: JSON.stringify(body.data.active),
                question1: body.data.usage_living,
                question2: body.data.stable_architecture,
                indemnify: body.data.highlight,
                date: body.data.published_date,
                productTitle: body.data.title,
                shortDescription: body.data.short_description,
                fullDescription:body.data.full_description,
                coverageDescription: body.data.coverage_description,
                protectTime: body.data.max_year,
                minMoney : Number(body.data.min_house_money),
                maxMoney : Number(body.data.max_house_money),
                houseRate : Number(body.data.house_rate),
                houseHint : body.data.house_hint,
                innerRate : body.data.highlight ? Number(body.data.inner_rate):'',
                innerHint : body.data.highlight ? body.data.inner_hint : '' ,
                indemnifyMoney : body.data.inner_assets_money,
                photo: body.data.photos,
            })
        } else {
            if(body.errorCode === 401){
                await common.handleExpiredToken();
            } else {
                Toast.Fail(body && body.message);
            }
        }
    };

    getInnerAssets = () => {
        let innerAssets = this.state.indemnifyMoney && this.state.indemnifyMoney.length && this.state.indemnifyMoney.map((element)=>{
            return {value:element, label: element.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.") + ' đ'}
        });
        return innerAssets;
    };

    onDateChange = (date) => {
        this.setState({date: date});
    };

    handleRate = (event) => {
        if(event.target.name=== 'houseRate'){
            this.setState({houseRate: event.target.value},()=>{
                if(Number(this.state.houseRate)>100 || Number(this.state.houseRate)<0){
                    this.setState({errorHouse: true,houseRate: null})
                } else {
                    this.setState({errorHouse: false})
                }
            })} else {
            this.setState({innerRate: event.target.value},()=>{
                if(Number(this.state.innerRate)>100 || Number(this.state.houseRate)<0){
                    this.setState({errorInnerHouse: true,innerRate: null})
                } else {
                    this.setState({errorInnerHouse: false})
                }
            })
        }

    };
    handleImage = (images) => {
        this.setState({images: images});
    };

    deleteImage = async (index) => {
        let fileNameIndex = this.state.photo[index].search('HouseInsurance/');
        let fileName = this.state.photo[index].slice(fileNameIndex+15);
        let containerType = 'HouseInsurance';
        let [success,body]=await ProductServices.deleteImage(fileName,containerType);
        if(success){
            let newPhoto = [...this.state.photo];
            let photo = newPhoto.filter((e, idx)=> idx!==index);
            this.setState({photo: photo},()=>{Toast.Success('Xoá thành công!')});
        } else {
            if(body.errorCode === 401){
                await common.handleExpiredToken();
            } else {
                Toast.Fail(body && body.message);
            }
        }
    };

    render() {
        const {data, radioValue} = this.state;
        return(
            <ThemeProvider theme={theme}>
                {data &&
                <div className='product-detail'>
                    <Row>
                        <Col lg={7} sm={12}>
                            <div className='product-detail-title'>Tên sản phẩm</div>
                            <Input
                                className='product-detail-input'
                                name='productTitle'
                                value={this.state.productTitle}
                                onChange={this.changeHouseInsuranceInput}
                                placeholder='Thêm tên sản phẩm'
                            />
                            <div className='product-detail-title'>Mô tả</div>
                            <Input
                                type="textarea"
                                className='product-detail-input'
                                name='shortDescription'
                                value={this.state.shortDescription}
                                onChange={this.changeHouseInsuranceInput}
                                placeholder='Thêm mô tả'
                            />
                            <div className='product-detail-title'>Nội dung</div>
                            <EditorText
                                value={this.state.fullDescription}
                                onChange={this.changeFullDescriptionEditor}
                                height={246}
                            />
                            <FormControl>
                                <FormGroup>
                                    <FormControlLabel
                                        control={
                                            <Checkbox
                                                checked={this.state.question1}
                                                onChange={(event) => this.handleCheckBoxChange(event, 'question1')}
                                                color="primary"
                                                value={this.state.question1}
                                            />
                                        }
                                        label='Căn nhà được bảo hiểm được sử dụng cho mục đích sinh sống hay không?'
                                    />
                                    <FormControlLabel
                                        control={
                                            <Checkbox
                                                checked={this.state.question2}
                                                onChange={(event) => this.handleCheckBoxChange(event, 'question2')}
                                                color="primary"
                                                value={this.state.question2}
                                            />
                                        }
                                        label='Ngôi nhà được bảo hiểm được xây dựng bằng gạch, đá, xi măng?'
                                    />
                                </FormGroup>
                            </FormControl>
                            <div className='cost-info'>Bảo hiểm và chi phí thông tin bảo hiểm</div>
                            <div className='product-detail-title'>Mô tả trách nhiệm bảo vệ</div>
                            <EditorText
                                value={this.state.coverageDescription}
                                onChange={this.changeCoverageDescriptionEditor}
                                height={246}
                            />
                            <div className='product-detail-period'>
                                <div className='product-detail-title'>Thời gian bảo vệ</div>
                                <Select
                                    defaultValue={{value: data.max_year, label: data.max_year}}
                                    options={this.handleSelectOptions()}
                                    className="multi-select-period"
                                    classNamePrefix="select-period"
                                    onChange={this.getProtectTimeValue}
                                />
                            </div>
                            <div className='product-detail-title'>Giới hạn bồi thường phần ngôi nhà</div>
                            <Row>
                                <Col lg={3} sm={12} className='pr-0'>
                                    <div className='indemnify-input-label'>Tối thiểu</div>
                                    <div className='indemnify-input-label'>Tối đa</div>
                                    <div className='indemnify-input-label'>Chỉ số phần trăm (Dùng để tính phí)</div>
                                    <div className='indemnify-input-label'>Phí đóng bảo hiểm</div>
                                    <div className='indemnify-input-label'>Chú thích thêm</div>
                                </Col>
                                <Col lg={9} sm={12} className='pl-0'>
                                    <Input
                                        className='indemnify-input product-detail-input'
                                        name='minMoney'
                                        value={this.state.minMoney}
                                        onChange={this.changeHouseInsuranceInput}
                                        type='number'
                                    />
                                    <Input
                                        className='indemnify-input product-detail-input'
                                        name='maxMoney'
                                        value={this.state.maxMoney}
                                        onChange={this.changeHouseInsuranceInput}
                                        type='number'
                                    />
                                    <Input
                                        className='indemnify-input product-detail-input'
                                        name='houseRate'
                                        value={this.state.houseRate}
                                        onChange={this.handleRate}
                                        type='number'
                                    />
                                    {this.state.errorHouse ? <div className='error-message mb-2'>Tỷ lệ % lớn hơn 0 và nhỏ hơn 100</div> : null}
                                    <div className='indemnify-text'>Phí = (Chỉ số tính phí) * (số tiền nằm trong khoản
                                        số tiền tối thiểu và số tiền tối đa)
                                    </div>
                                    <Input
                                        className='indemnify-input product-detail-input'
                                        name='houseHint'
                                        value={this.state.houseHint}
                                        onChange={this.changeHouseInsuranceInput}
                                    />
                                </Col>
                            </Row>
                            <div className='d-flex mt-3 align-items-center'>
                                <div className='product-detail-title'>Giới hạn bồi thường phần tài sản bên trong ngôi
                                    nhà
                                </div>
                                <Switch
                                    className='ml-auto'
                                    checked={this.state.indemnify}
                                    onChange={this.indemnifyAssetInsideHouse}
                                    color="primary"
                                />
                            </div>
                            <Row>
                                <Col lg={3} sm={12} className='pr-0'>
                                    <div className='indemnify-input-label'>Giá trị</div>
                                    <div className='indemnify-input-label'>Chỉ số phần trăm</div>
                                    <div className='indemnify-input-label'>Phí bồi thường</div>
                                    <div className='indemnify-input-label'>Chú thích thêm</div>
                                </Col>
                                <Col lg={9} sm={12} className='pl-0'>
                                    <div className='product-detail-period'>
                                        <Select
                                            isMulti
                                            defaultValue={this.getInnerAssets()}
                                            isDisabled={!this.state.indemnify}
                                            options={[
                                                {value: 30000000, label: '30.000.000 đ'},
                                                {value: 50000000, label: '50.000.000 đ'},
                                                {value: 100000000, label: '100.000.000 đ'}
                                            ]}
                                            className="multi-select-period house-insurance"
                                            classNamePrefix="select-period"
                                            onChange={this.getIndemnifyMoney}
                                        />
                                    </div>
                                    <Input
                                        disabled={!this.state.indemnify}
                                        className='indemnify-input product-detail-input'
                                        name='innerRate'
                                        value={this.state.innerRate}
                                        onChange={this.handleRate}
                                        type='number'
                                    />
                                    {this.state.errorInnerHouse ? <div className='error-message mb-2'>Tỷ lệ % lớn hơn 0 và nhỏ hơn 100</div> : null}
                                    <div className='indemnify-text'>Phí = (Chỉ số tính phí) * (số tiền nằm trong khoản
                                        số tiền tối thiểu và số tiền tối đa)
                                    </div>
                                    <Input
                                        disabled={!this.state.indemnify}
                                        className='indemnify-input product-detail-input'
                                        name='innerHint'
                                        value={this.state.innerHint}
                                        onChange={this.changeHouseInsuranceInput}

                                    />
                                </Col>
                            </Row>
                        </Col>
                        <Col lg={5} sm={12} className='left-content'>
                            <div className='product-detail-title'>Nhóm sản phẩm</div>
                            <div className='add-product-type'>Nhà</div>
                            <div className='product-detail-title'>Ngày đăng</div>
                            <IMSDate
                                date = {this.state.date}
                                onDateChange = {this.onDateChange}
                            />
                            {/*<div className='show-checkbox'>
                                <div className='product-detail-title'>Hiển thị</div>
                                <FormControl>
                                    <RadioGroup
                                        className='mt-3'
                                        onChange={this.handleChangeRadio}
                                        value={radioValue}
                                    >
                                        <FormControlLabel value="false" control={<Radio color="primary"/>}
                                                          label="Ẩn"/>
                                        <FormControlLabel value="true" control={<Radio color="primary"/>}
                                                          label="Hiện"/>
                                    </RadioGroup>
                                </FormControl>
                            </div>*/}
                            {/*<ChooseImage
                                coverInputId='choose-house-cover-image'
                                coverImgId='house-cover-image'
                                thumbnailInputId='choose-house-thumbnail-image'
                                thumbnailImgId='house-thumbnail-image'
                                photo={this.state.photo}
                                thumbnail={data.thumbnail}
                                deleteImage={this.deleteImage}
                                renderImages={this.handleImage}
                            />*/}
                            <AddImages
                                coverInputId='choose-house-cover-image'
                                coverImgId='house-cover-image'
                                thumbnailInputId='choose-house-thumbnail-image'
                                thumbnailImgId='house-thumbnail-image'
                                photo={this.state.photo}
                                thumbnail={data.thumbnail}
                                deleteImage={this.deleteImage}
                                renderImages={this.handleImage}
                            />
                        </Col>
                    </Row>
                    <div className='product-bottom'>
                        <Button className='cancel-button' onClick={this.handleSubmit}>LƯU</Button>
                    </div>
                </div>
                }
            </ThemeProvider>
        )
    }
}
export default HouseInsurance;