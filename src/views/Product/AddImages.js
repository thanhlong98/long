import React from "react";
import {Col, Row} from "reactstrap";

class AddImages extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            images: null,
            thumbnailSource: this.props.thumbnail,
        }
    }

    handleChange = () =>{
        let filesInput = document.getElementById(this.props.coverInputId);
        let files = filesInput.files;
        let newImage = this.state.images ? [...this.state.images] : [];
        for (let i = 0; i < files.length; i++) {
            let picReader = new FileReader();
            picReader.addEventListener("load", (event)=> {
                files[i].src = event.target.result;
                this.setState({images: newImage},()=>{this.props.renderImages(newImage)});
            });
            picReader.readAsDataURL(files[i]);
            newImage.push(files[i]);
        }
    };

    handleDeleteImage = (index) => {
        let newImages = [...this.state.images];
        let files = newImages.filter((element, idx)=> idx!== index);
        this.setState({images: files});
        this.props.renderImages(this.state.images)
    };

    renderImage = (inputId, imgId) => {
        let preview = document.getElementById(imgId);
        let file    = document.getElementById(inputId).files[0];
        let reader  = new FileReader();
        reader.addEventListener("load",  () => {
            //preview.src = reader.result;
            this.setState({thumbnailSource: reader.result})
        }, false);
        if(file) {
            reader.readAsDataURL(file);
        }
    };

    render() {
        return (
            <div>
                <div>
                <div className='d-flex'>
                    <div className='product-detail-title mr-auto'>Ảnh bìa</div>
                    <label id='cover-image' htmlFor={this.props.coverInputId} className='image-label'>Thêm ảnh</label>
                </div>
                <input
                    id={this.props.coverInputId}
                    type="file"
                    multiple
                    onChange={this.handleChange}
                    style={{display: 'none'}}
                />
                <br/>
                <Row>
                    {
                        this.props.photo && this.props.photo.map((photo,index)=>{
                            return(
                                <Col sm={4} className='images-container'>
                                    <img className='cover-image mb-3' src={photo} width='100%' height='96px'/>
                                    <span className='delete-image'><i className='icon-delete-image' onClick={()=>this.props.deleteImage(index)}/></span>
                                </Col>
                            )
                        })

                    }
                    {
                        this.state.images ?
                            this.state.images.map((element, index)=>{
                            return(
                                <Col sm={4} className='images-container'>
                                    <img src={element.src} className='cover-image mb-3' width='100%' height='96px'/>
                                    <span className='delete-image'><i className='icon-delete-image' onClick={()=>this.handleDeleteImage(index)}/></span>
                                </Col>
                                )
                            })
                            : (this.props.photo ? null :
                            <Col sm={4}>
                                <img
                                    className='default-image ml-3'
                                    src={require("../../assets/images/image-product.svg")} width='100%'
                                    height='96px'
                                    onClick={()=>document.getElementById('cover-image').click()}
                                />
                            </Col>
                            )
                    }
                </Row>
            </div>
                <div className='mt-4'>
                    <div className='d-flex'>
                        <div className='product-detail-title mr-auto'>Ảnh thumbnail</div>
                        <label id='thumbnail-image' htmlFor={this.props.thumbnailInputId} className='image-label'>Đổi ảnh</label>
                    </div>
                    <input
                        id={this.props.thumbnailInputId}
                        type='file'
                        onChange={() => this.renderImage(this.props.thumbnailInputId, this.props.thumbnailImgId)}
                        style={{display: 'none'}}
                    />
                    <br/>
                    <img
                        className='ml-3'
                        src={this.state.thumbnailSource ? this.state.thumbnailSource : require("../../assets/images/image-product.svg")} width='50px'
                        height='50px'
                        id={this.props.thumbnailImgId}
                        onClick={()=>document.getElementById('thumbnail-image').click()}
                    />
                </div>
            </div>
        );
    }
}
export default AddImages;