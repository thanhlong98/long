import React from 'react';
import {Button, Col, Input, Row} from "reactstrap";
import 'react-dates/initialize';
import 'react-dates/lib/css/_datepicker.css';
import {SingleDatePicker} from "react-dates";
import FormControl from "@material-ui/core/FormControl";
import Switch from "@material-ui/core/Switch";
import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles'
import FormControlLabel from "@material-ui/core/FormControlLabel";
import RadioGroup from "@material-ui/core/RadioGroup";
import Radio from "@material-ui/core/Radio";
import Select from 'react-select';
import FeedbackHints from "./FeedbackHints";
import {ProductServices} from "../../services/ProductServices";
import moment from "moment";
import {UserServices} from "../../services/UserServices";
import SupplementaryProduct from "./SupplementaryProduct";
import EditorText from "./EditorText";
import CostPackage from "./CostPackage";
import SelectProductType from "./SelectProductType";
import ReviewProductDetail from './ReviewProductDetail';
import {common} from "../../utils/Common";
import {Toast} from "../../utils/Toast";


const theme = createMuiTheme({
    palette: {
        primary: {main: '#31BB71'},
    },
});

class ProductDetails extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            date: null,
            focused: false,
            radioValue: null,
            switched: false,
            hintRows: [],
            productDetailsData: null,
            extraProduct: [],
            productPrice: [],

            titleProduct: null,
            shortDescription: null,
            fullDescription: null,
            productType: null,
        }
    }

    handleChangeSwitch = () => {
        this.setState({switched: !this.state.switched})
    };

    handleChangeRadio = (event) => {
        this.setState({radioValue: event.target.value})
    };

    componentDidMount() {
        this.getProductDetails();
    }

    getProductDetails = async () => {
        let productId=this.props.match.params.productId;
        let [success, body] = await ProductServices.getProductDetail(productId);
        if(success){
            let extraProductDetail = body.data.extra_product;
            let extraProduct = extraProductDetail.map((product)=>{
                let extraProduct = {};
                for(let key in product){
                    if(key === 'title'){
                        extraProduct.title = product.title;
                    }
                    if(key === 'cost'){
                        extraProduct.cost = product.cost;
                    }
                    if(key === 'short_description'){
                        extraProduct.short_description = product.short_description;
                    }
                    if(key === 'full_description'){
                        extraProduct.full_description = product.full_description;
                    }
                    if(key === 'unit'){
                        extraProduct.unit = product.unit;
                    }
                    if(key === 'uid'){
                        extraProduct.uid = product.uid;
                    }
                }
                return extraProduct
            });
            this.setState({
                productDetailsData: body.data,
                switched: body.data.highlight,
                date: body.data.created_at,
                extraProduct: extraProduct,
                productPrice: body.data.product_price,
                fullDescription: body.data.full_description,

            })
        } else {
            if(body.errorCode === 401){
                await common.handleExpiredToken();
            } else {
                Toast.Fail(body && body.message);
            }
        }
    };

    addSupplementaryProduct = (productName,cost,description,editorValue) => {
        this.setState({
            extraProduct: this.state.extraProduct.concat([
                {
                    title: productName,
                    cost: cost,
                    short_description: description,
                    full_description:editorValue,
                    unit: "VND",
                }
            ])
        })
    };

    addCostPackage = (cost, protectedTime, paidTime, period) => {
        this.setState({
            productPrice: this.state.productPrice.concat([
                {
                    "unit": "VND",
                    "cost_per_package": cost,
                    "paid_time": paidTime,
                    "protected_time": protectedTime,
                    "product_price_term": period,
                }
            ])
        })
    };

    handleChangePDInput = (event) => {
        this.setState({[event.target.name]: event.target.value})
    };

    handleEditorChange = (event) => {
        this.setState({fullDescription: event.target.getContent()})
    };
    editCostPackage = (idx,cost, protectedTime, paidTime, period) => {
        let newProductPrice = [...this.state.productPrice];
        newProductPrice[idx]={
            "unit": "VND",
            "cost_per_package": cost,
            "paid_time": paidTime,
            "protected_time": protectedTime,
            "product_price_term": period,
        };
        this.setState({productPrice : newProductPrice});
    };
    deleteCostPackage = (idx) => {
        let newProductPrice =this.state.productPrice.filter((product, index)=> idx !== index);
        this.setState({productPrice : newProductPrice})
    };
    duplicateCostPackage = (idx) => {
        let newProductPrice = [...this.state.productPrice];
        newProductPrice.push(newProductPrice[idx]);
        this.setState({productPrice : newProductPrice})
    };
    handleSelectChange = (value) => {
        this.setState({productType: value})
    };

    extraProductUid=[];
    deleteSupplementaryProduct = (idx) => {
        let uid = this.state.extraProduct[idx].uid;
        this.extraProductUid.push(uid);
        let newExtraProduct =this.state.extraProduct.filter((product, index)=> idx !== index);
        this.setState({extraProduct : newExtraProduct})
    };

    duplicateSupplementaryProduct = (idx) => {
        let newExtraProduct = [...this.state.extraProduct];
        let newProduct = {...newExtraProduct[idx]};
        delete newProduct["uid"];
        newExtraProduct.push(newProduct);
        this.setState({extraProduct : newExtraProduct})
    };

    editSupplementaryProduct = (idx,productName,cost,description,editorValue) => {
        let newExtraProduct = [...this.state.extraProduct];
        newExtraProduct[idx]={
            title: productName,
            cost: cost,
            short_description: description,
            full_description: editorValue,
            uid: this.state.extraProduct[idx].uid
        };
        this.setState({extraProduct : newExtraProduct});
    };

    updateProduct = async (productId) => {
        let input = document.getElementById('product-detail-image');
        let data = new FormData();
        data.append("provider", this.state.productDetailsData.provider_uid);
        data.append("photos", this.state.productDetailsData.photos);
        data.append("title", this.state.titleProduct ? this.state.titleProduct : this.state.productDetailsData.title);
        data.append("highlight", this.state.switched);
        data.append("product_type", 'nhan_tho');
        data.append("short_description", this.state.shortDescription ? this.state.shortDescription : this.state.productDetailsData.short_description);
        data.append("full_description", this.state.fullDescription ? this.state.fullDescription : this.state.productDetailsData.full_description);
        data.append("thumbnail", this.state.productDetailsData.thumbnail);
        data.append("cover", input.files.length !== 0 ? input.files[0] : this.state.productDetailsData.cover);
        data.append("product_price", JSON.stringify(this.state.productPrice));
        data.append('extra_product',JSON.stringify(this.state.extraProduct));
        let[success, body] = await ProductServices.updateProduct(productId, data);
        if(success){
            Toast.Success('Update thanh cong');
        } else {
            if(body.errorCode === 401){
                await common.handleExpiredToken();
            } else {
                Toast.Fail(body && body.message);
            }
        }
    };

    handleUpdateProduct = ()=>{
        let productId = this.props.match.params.productId;
        /*let data = {
            "title": this.state.extraProduct.title,
            "cost": this.state.extraProduct.cost,
            "unit": "VND",
            "product": this.props.match.params.productId,
        };
        let [success,body] = ProductServices.postExtraProduct(data);
        if(success){
            alert('success')
        }*/
        this.updateProduct(productId);
        window.location = '#/products';

    };
    previewFile = () => {
       let preview = document.getElementById('add-image');
        let file    = document.getElementById('product-detail-image').files[0];
        let reader  = new FileReader();
        /*reader.onload = function(e) {
            preview.src = reader.result;
        };*/
        reader.addEventListener("load", function () {
      
      
            preview.src = reader.result;
        }, false);
        if(file) {
            reader.readAsDataURL(file);
        }
    };

    render() {
        const {productDetailsData, extraProduct, fullDescription} = this.state;
        return (
            <div>
                {productDetailsData &&
                <ThemeProvider theme={theme}>
                    <div className='product-detail'>
                        <Row>
                            <Col lg={7} sm={12}>
                                <div className='product-detail-title'>Tên sản phẩm</div>
                                <Input
                                    defaultValue={productDetailsData.title}
                                    className='product-detail-input'
                                    name='titleProduct'
                                    value={this.state.titleProduct}
                                    onChange={this.handleChangePDInput}
                                />
                                <div className='product-detail-title'>Mô tả</div>
                                <Input
                                    type="textarea"
                                    defaultValue={productDetailsData.short_description}
                                    className='product-detail-input'
                                    name='shortDescription'
                                    value={this.state.shortDescription}
                                    onChange={this.handleChangePDInput}
                                />
                                <div className='product-detail-title'>Nội dung</div>
                                <EditorText
                                    //initialValue={productDetailsData.full_description}
                                    value={this.state.fullDescription}
                                    onChange={this.handleEditorChange}
                                    height={246}
                                />
                                <div>
                                    <SupplementaryProduct
                                        extraProduct = {extraProduct}
                                        onDone={this.addSupplementaryProduct}
                                        onDelete={this.deleteSupplementaryProduct}
                                        onDuplicate={this.duplicateSupplementaryProduct}
                                        onEdit={this.editSupplementaryProduct}
                                    />
                                </div>
                                <div>
                                    <CostPackage
                                        productPrice = {this.state.productPrice}
                                        onSubmit = {this.addCostPackage}
                                        onDelete={this.deleteCostPackage}
                                        onDuplicate={this.duplicateCostPackage}
                                        onEdit={this.editCostPackage}
                                    />
                                </div>
                                <div className='product-detail-title'>Gợi ý nhận xét</div>
                                <FeedbackHints
                                    hintRows={this.state.hintRows}
                                />
                                <ReviewProductDetail 
                                    uid ={this.props.match.params.productId}
                                />
                            </Col>
                            <Col lg={5} sm={12} className='left-content'>
                                {/*<SelectProductType
                                    onChange={this.handleSelectChange}
                                    defaultValue={productDetailsData.product_type}
                                />*/}
                                <div className='product-detail-title'>Nhóm sản phẩm</div>
                                <div className='add-product-type'>Nhân thọ</div>
                                <div className='product-detail-title'>Ngày đăng</div>
                                <div className='date-time'>
                                    <SingleDatePicker
                                        date={this.state.date && moment(this.state.date)}
                                        onDateChange={date => this.setState({date})}
                                        focused={this.state.focused}
                                        onFocusChange={({focused}) => this.setState({focused})}
                                        id="date"
                                        numberOfMonths={1}
                                        placeholder="dd/mm/yyyy"
                                        showDefaultInputIcon={true}
                                        inputIconPosition='after'
                                        displayFormat="DD/MM/YYYY"
                                        isOutsideRange={() => false}
                                    />
                                </div>
                                <div className='special-product'>
                                    <div className='product-detail-title'>Sản phẩm nổi bật</div>
                                    <Switch
                                        className='ml-auto'
                                        checked={this.state.switched}
                                        value="checkedA"
                                        onChange={this.handleChangeSwitch}
                                        inputProps={{'aria-label': 'secondary checkbox'}}
                                        color="primary"
                                    />
                                </div>
                                <div className='show-checkbox'>
                                    <div className='product-detail-title'>Hiển thị</div>
                                    <FormControl>
                                        <RadioGroup
                                            className='mt-3'
                                            onChange={this.handleChangeRadio}
                                        >
                                            <FormControlLabel value="Ẩn" control={<Radio color="primary"/>}
                                                              label="Ẩn"/>
                                            <FormControlLabel value="Hiện" control={<Radio color="primary"/>}
                                                              label="Hiện"/>
                                        </RadioGroup>
                                    </FormControl>
                                </div>
                                <div>
                                    <div className='product-detail-title'>Ảnh bìa</div>
                                    <input
                                        id='product-detail-image'
                                        type='file'
                                        onChange={this.previewFile}
                                    />
                                    <img
                                        className='mt-3'
                                        src={productDetailsData.cover} width='100%'
                                        height='100%'
                                        id='add-image'
                                    />
                                </div>
                            </Col>
                        </Row>
                        <div className='product-bottom'>
                            <Button className='save-button' onClick={this.handleUpdateProduct}>LƯU</Button>
                        </div>
                    </div>
                </ThemeProvider>
                }

            </div>
        )
    }

}

export default ProductDetails;