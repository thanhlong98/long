import React from "react";
import {Button, Col, Input, Row} from "reactstrap";
import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles'
import EditorText from "./EditorText";
import {SingleDatePicker} from "react-dates";
import moment from "moment";
import Switch from "@material-ui/core/Switch";
import FormControl from "@material-ui/core/FormControl";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Radio from "@material-ui/core/Radio";
import ChooseImage from "./ChooseImage";
import {ProductServices} from "../../services/ProductServices";
import {Toast} from "../../utils/Toast.js";
import IMSDate from "../../utils/IMSDate";
import {common} from "../../utils/Common";
import AddImages from "./AddImages";

const theme = createMuiTheme({
    palette: {
        primary: {main: '#31BB71'},
    },
});

class MotorbikeInsurance extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            date: moment(new Date()),
            radioValue: null,
            productTitle: null,
            shortDescription:null,
            fullDescription:null,
            compulsoryInsuranceCost1: null,
            compulsoryInsuranceCost2:null,
            compulsoryInsuranceCost3:null,
            voluntaryInsuranceCost1:null,
            voluntaryInsuranceCost2:null,
            minMoneyAccidentInsurance:null,
            maxMoneyAccidentInsurance:null,
            rateAccidentInsurance:null,
            minMoneyFireInsurance:null,
            maxMoneyFireInsurance:null,
            rateFireInsurance:null,
            errorAccident:false,
            errorFire:false,
            images: null,
        }
    }

    changeMotorbikeInsuranceInput = (event) => {
        this.setState({[event.target.name]: event.target.value})
    };

    changMotorbikeInsuranceEditor = (event) => {
        this.setState({fullDescription: event.target.getContent()})
    };

    handleChangeRadio = (event) => {
        this.setState({radioValue: event.target.value})
    };

    handleCancel = () => {
        window.location = '#/products';
    };

    handleSubmit = async () => {
        let thumbnailInput = document.getElementById('choose-motorbike-thumbnail-image');
        let {date, radioValue, productTitle,shortDescription,fullDescription,compulsoryInsuranceCost1,compulsoryInsuranceCost2,
            compulsoryInsuranceCost3,voluntaryInsuranceCost1,voluntaryInsuranceCost2,minMoneyAccidentInsurance,
            maxMoneyAccidentInsurance,rateAccidentInsurance,minMoneyFireInsurance,maxMoneyFireInsurance,rateFireInsurance, errorAccident, errorFire}=this.state;
        if(date && productTitle && shortDescription && fullDescription && compulsoryInsuranceCost1 && compulsoryInsuranceCost2
            && compulsoryInsuranceCost3 && voluntaryInsuranceCost1 && voluntaryInsuranceCost2 && minMoneyAccidentInsurance &&
            maxMoneyAccidentInsurance && rateAccidentInsurance && minMoneyFireInsurance && maxMoneyFireInsurance && rateFireInsurance
            && thumbnailInput.files[0] !== undefined && errorAccident===false && errorFire===false && this.state.images.length !== 0 ) {
            let compulsoryInsurance = [{
                engine_type: 'Mô tô 2 bánh từ 50 cc trở xuống',
                person_coverage: '100 triệu/người/vụ',
                asset_coverage: '50 triệu/vụ',
                cost: this.state.compulsoryInsuranceCost1
            }, {
                engine_type: 'Mô tô 2 bánh trên 50 cc',
                person_coverage: '100 triệu/người/vụ',
                asset_coverage: '50 triệu/vụ',
                cost: this.state.compulsoryInsuranceCost2
            }, {
                engine_type: 'Xe mô tô 3 bánh, xe gắn máy và các loại xe cơ giới tương tự',
                person_coverage: '100 triệu/người/vụ',
                asset_coverage: '50 triệu/vụ',
                cost: this.state.compulsoryInsuranceCost3
            }];

            let voluntaryInsurance = [{
                voluntary_type: 'Loại A',
                person_coverage: '50 triệu/người/vụ',
                asset_coverage: '50 triệu/vụ',
                cost: this.state.voluntaryInsuranceCost1
            }, {
                voluntary_type: 'Loại B',
                person_coverage: '100 triệu/người/vụ',
                asset_coverage: '50 triệu/vụ',
                cost: this.state.voluntaryInsuranceCost2
            }];

            let accidentInsurance = {
                min_place_no: 1,
                max_place_no: 2,
                min_coverage_value: this.state.minMoneyAccidentInsurance,
                max_coverage_value: this.state.maxMoneyAccidentInsurance,
                rate: this.state.rateAccidentInsurance,
            };

            let fireInsurance = {
                min_place_no: 1,
                max_place_no: 2,
                min_coverage_value: this.state.minMoneyFireInsurance,
                max_coverage_value: this.state.maxMoneyFireInsurance,
                rate: this.state.rateFireInsurance,
            };

            let data = new FormData();
            data.append("provider", this.props.providerUid);
            data.append('title', this.state.productTitle);
            data.append('short_description', this.state.shortDescription);
            data.append('full_description', this.state.fullDescription);
            data.append('active', 'false'); //this.state.radioValue
            data.append('published_date', this.state.date && this.state.date.format && this.state.date.format('YYYY-MM-DD'));
            data.append('compulsory_insurance', JSON.stringify(compulsoryInsurance));
            data.append('voluntary_insurance', JSON.stringify(voluntaryInsurance));
            data.append('accident_insurance', JSON.stringify(accidentInsurance));
            data.append('fire_insurance', JSON.stringify(fireInsurance));
            data.append("thumbnail", thumbnailInput.files[0]);
            for (let file of this.state.images) {
                data.append('photos', file);
            };
            let [success, body] = await ProductServices.addMotorbikeInsurance(data);
            if (success) {
                Toast.Success('Success');
                window.location = '#products'
            } else {
                if(body.errorCode === 401){
                    await common.handleExpiredToken();
                } else {
                    Toast.Fail(body && body.message);
                }
            }
        } else {
            if(errorAccident||errorFire){
                Toast.Fail('Tỷ lệ % lớn hơn 0 và nhỏ hơn 100')
            } else {
                Toast.Fail('Vui lòng nhập đầy đủ thông tin sản phẩm')
            }
        }
    };

    onDateChange = (date) => {
        this.setState({date: date});
    };

    handleRate = (event) => {
        if(event.target.name=== 'rateAccidentInsurance'){
        this.setState({rateAccidentInsurance: event.target.value},()=>{
            if(Number(this.state.rateAccidentInsurance)>100 || Number(this.state.rateAccidentInsurance)<0){
                this.setState({errorAccident: true,rateAccidentInsurance: null})
            } else {
                this.setState({errorAccident: false})
            }
        })} else {
            this.setState({rateFireInsurance: event.target.value},()=>{
                if(Number(this.state.rateFireInsurance)>100 || Number(this.state.rateAccidentInsurance)<0){
                    this.setState({errorFire: true,rateFireInsurance: null})
                } else {
                    this.setState({errorFire: false})
                }
            })
        }

    };

    handleImage = (images) => {
        this.setState({images: images});
    };

    render() {
        return(
            <ThemeProvider theme={theme}>
                <div className='product-detail'>
                    <Row>
                        <Col lg={7} sm={12}>
                            <div className='product-detail-title'>Tên sản phẩm</div>
                            <Input
                                className='product-detail-input'
                                name='productTitle'
                                value={this.state.productTitle}
                                onChange={this.changeMotorbikeInsuranceInput}
                                placeholder='Thêm tên sản phẩm'
                            />
                            <div className='product-detail-title'>Mô tả</div>
                            <Input
                                type="textarea"
                                className='product-detail-input'
                                name='shortDescription'
                                value={this.state.shortDescription}
                                onChange={this.changeMotorbikeInsuranceInput}
                                placeholder='Thêm mô tả'
                            />
                            <div className='product-detail-title'>Nội dung</div>
                            <EditorText
                                name='fullDescription'
                                value={this.state.fullDescription}
                                onChange={this.changMotorbikeInsuranceEditor}
                                height={246}
                            />
                            <div className='motorbike-insurance-info'>Thông tin bảo hiểm</div>
                            <div className='hint-info'>Tất cả đều được tính toán theo giá trị 1 năm</div>
                            <div className='product-detail-title'>BH TNDS bắt buộc</div>
                            <div className='hint-info'>Hint Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur vulputate.</div>
                            <div>
                                <div className='supplementary-product-header'>
                                    <div>1. Mô tô 2 bánh từ 50 cc trở xuống</div>
                                </div>
                                <div className='mandatory-insurance'>
                                    <Row>
                                        <Col sm={3}>
                                            <div>Về người:</div>
                                            <div>Về tài sản:</div>
                                            <div className='motorbike-insurance-label'>Giá: (VAT)</div>
                                        </Col>
                                        <Col sm={9}>
                                            <div>100 triệu/người/vụ</div>
                                            <div>50 triệu/vụ</div>
                                            <Input
                                                className='product-detail-input motorbike-insurance-input'
                                                placeholder='Thêm giá'
                                                name='compulsoryInsuranceCost1'
                                                value={this.state.compulsoryInsuranceCost1}
                                                onChange={this.changeMotorbikeInsuranceInput}
                                                type='number'
                                            />
                                        </Col>
                                    </Row>
                                </div>
                                <div className='supplementary-product-header'>
                                    <div>2. Mô tô 2 bánh trên 50 cc</div>
                                </div>
                                <div className='mandatory-insurance'>
                                    <Row>
                                        <Col sm={3}>
                                            <div>Về người:</div>
                                            <div>Về tài sản:</div>
                                            <div className='motorbike-insurance-label'>Giá: (VAT)</div>
                                        </Col>
                                        <Col sm={9}>
                                            <div>100 triệu/người/vụ</div>
                                            <div>50 triệu/vụ</div>
                                            <Input
                                                className='product-detail-input motorbike-insurance-input'
                                                placeholder='Thêm giá'
                                                name='compulsoryInsuranceCost2'
                                                value={this.state.compulsoryInsuranceCost2}
                                                onChange={this.changeMotorbikeInsuranceInput}
                                                type='number'
                                            />
                                        </Col>
                                    </Row>
                                </div>
                                <div className='supplementary-product-header'>
                                    <div>3. Xe mô tô 3 bánh, xe gắn máy và các loại xe cơ giới tương tự</div>
                                </div>
                                <div className='mandatory-insurance'>
                                    <Row>
                                        <Col sm={3}>
                                            <div>Về người:</div>
                                            <div>Về tài sản:</div>
                                            <div className='motorbike-insurance-label'>Giá: (VAT)</div>
                                        </Col>
                                        <Col sm={9}>
                                            <div>100 triệu/người/vụ</div>
                                            <div>50 triệu/vụ</div>
                                            <Input
                                                className='product-detail-input motorbike-insurance-input'
                                                placeholder='Thêm giá'
                                                name='compulsoryInsuranceCost3'
                                                value={this.state.compulsoryInsuranceCost3}
                                                onChange={this.changeMotorbikeInsuranceInput}
                                                type='number'
                                            />
                                        </Col>
                                    </Row>
                                </div>
                            </div>
                            <div className='product-detail-title  mt-5'>BH TNDS tự nguyện</div>
                            <div className='hint-info'>Hint Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur vulputate.</div>
                            <div className='supplementary-product-header'>
                                <div>1. Loại A</div>
                            </div>
                            <div className='mandatory-insurance'>
                                <Row>
                                    <Col sm={3}>
                                        <div>Về người:</div>
                                        <div>Về tài sản:</div>
                                        <div className='motorbike-insurance-label'>Giá: (VAT)</div>
                                    </Col>
                                    <Col sm={9}>
                                        <div>50 triệu/người/vụ</div>
                                        <div>50 triệu/vụ</div>
                                        <Input
                                            className='product-detail-input motorbike-insurance-input'
                                            placeholder='Thêm giá'
                                            name='voluntaryInsuranceCost1'
                                            value={this.state.voluntaryInsuranceCost1}
                                            onChange={this.changeMotorbikeInsuranceInput}
                                            type='number'
                                        />
                                    </Col>
                                </Row>
                            </div>
                            <div className='supplementary-product-header'>
                                <div>2. Loại B</div>
                            </div>
                            <div className='mandatory-insurance'>
                                <Row>
                                    <Col sm={3}>
                                        <div>Về người:</div>
                                        <div>Về tài sản:</div>
                                        <div className='motorbike-insurance-label'>Giá: (VAT)</div>
                                    </Col>
                                    <Col sm={9}>
                                        <div>100 triệu/người/vụ</div>
                                        <div>50 triệu/vụ</div>
                                        <Input
                                            className='product-detail-input motorbike-insurance-input'
                                            placeholder='Thêm giá'
                                            name='voluntaryInsuranceCost2'
                                            value={this.state.voluntaryInsuranceCost2}
                                            onChange={this.changeMotorbikeInsuranceInput}
                                            type='number'
                                        />
                                    </Col>
                                </Row>
                            </div>
                            <div className='product-detail-title mt-5'>Bảo hiểm tai nạn người trên xe</div>
                            <Row>
                                <Col lg={4} sm={12} className='pr-0'>
                                    <div className='indemnify-input-label'>Số chỗ tối đa</div>
                                    <div className='indemnify-input-label'>Giá trị BH tối thiểu</div>
                                    <div className='indemnify-input-label'>Giá trị BH tối đa</div>
                                    <div className='indemnify-input-label'>Tỷ lệ % tính phí</div>
                                    {this.state.errorAccident ?
                                        <div className='error-label'/>
                                        :
                                        null
                                    }
                                    <div className='indemnify-input-label'>Ghi chú</div>
                                </Col>
                                <Col lg={8} sm={12} className='pl-0'>
                                    <div className='indemnify-input-label'>2</div>
                                    <Input
                                        className='indemnify-input product-detail-input'
                                        name='minMoneyAccidentInsurance'
                                        value={this.state.minMoneyAccidentInsurance}
                                        onChange={this.changeMotorbikeInsuranceInput}
                                        type='number'
                                    />
                                    <Input
                                        className='indemnify-input product-detail-input'
                                        name='maxMoneyAccidentInsurance'
                                        value={this.state.maxMoneyAccidentInsurance}
                                        onChange={this.changeMotorbikeInsuranceInput}
                                        type='number'
                                    />
                                    <Input
                                        className='indemnify-input product-detail-input'
                                        name='rateAccidentInsurance'
                                        value={this.state.rateAccidentInsurance}
                                        //onChange={this.changeMotorbikeInsuranceInput}
                                        onChange={this.handleRate}
                                        type='number'
                                    />
                                    {this.state.errorAccident ?
                                        <div className='error-message'>Tỷ lệ % lớn hơn 0 và nhỏ hơn 100</div>
                                        :
                                        null
                                    }
                                    <div className='indemnify-text'>Số chỗ tối thiểu * (số tiền nằm trong khoản số tiền tối thiểu và số tiền tối đa) * tỷ lệ % tính phí</div>

                                </Col>
                            </Row>
                            <div className='product-detail-title mt-5'>Bảo hiểm cháy nổ</div>
                            <Row>
                                <Col lg={4} sm={12} className='pr-0'>
                                    <div className='indemnify-input-label'>Số chỗ tối thiểu</div>
                                    <div className='indemnify-input-label'>Giá trị BH tối thiểu</div>
                                    <div className='indemnify-input-label'>Giá trị BH tối đa</div>
                                    <div className='indemnify-input-label'>Tỷ lệ % tính phí</div>
                                    {this.state.errorFire ?
                                        <div className='error-label'/>
                                        :
                                        null
                                    }
                                    <div className='indemnify-input-label'>Ghi chú</div>
                                </Col>
                                <Col lg={8} sm={12} className='pl-0'>
                                    <div className='indemnify-input-label'>2</div>
                                    <Input
                                        className='indemnify-input product-detail-input'
                                        name='minMoneyFireInsurance'
                                        value={this.state.minMoneyFireInsurance}
                                        onChange={this.changeMotorbikeInsuranceInput}
                                        type='number'
                                    />
                                    <Input
                                        className='indemnify-input product-detail-input'
                                        name='maxMoneyFireInsurance'
                                        value={this.state.maxMoneyFireInsurance}
                                        onChange={this.changeMotorbikeInsuranceInput}
                                        type='number'
                                    />
                                    <Input
                                        className='indemnify-input product-detail-input'
                                        name='rateFireInsurance'
                                        value={this.state.rateFireInsurance}
                                        onChange={this.handleRate}
                                        type='number'
                                    />
                                    {this.state.errorFire ?
                                        <div className='error-message'>Tỷ lệ % lớn hơn 0 và nhỏ hơn 100</div>
                                        :
                                        null
                                    }
                                    <div className='indemnify-text'>Số chỗ tối thiểu * (số tiền nằm trong khoản số tiền tối thiểu và số tiền tối đa) * tỷ lệ % tính phí</div>

                                </Col>
                            </Row>
                        </Col>
                        <Col lg={5} sm={12} className='left-content'>
                            <div className='product-detail-title'>Nhóm sản phẩm</div>
                            <div className='add-product-type'>Mô tô</div>
                            <div className='product-detail-title'>Ngày đăng</div>
                            <IMSDate
                                date = {this.state.date}
                                onDateChange = {this.onDateChange}
                            />
                            {/*<div className='show-checkbox'>
                                <div className='product-detail-title'>Hiển thị</div>
                                <FormControl>
                                    <RadioGroup
                                        className='mt-3'
                                        onChange={this.handleChangeRadio}
                                    >
                                        <FormControlLabel value="false" control={<Radio color="primary"/>}
                                                          label="Ẩn"/>
                                        <FormControlLabel value="true" control={<Radio color="primary"/>}
                                                          label="Hiện"/>
                                    </RadioGroup>
                                </FormControl>
                            </div>*/}
                            {/*<ChooseImage
                                renderImages={this.handleImage}
                                coverInputId='choose-motorbike-cover-image'
                                coverImgId='motorbike-cover-image'
                                thumbnailInputId = 'choose-motorbike-thumbnail-image'
                                thumbnailImgId = 'motorbike-thumbnail-image'
                            />*/}
                            <AddImages
                                renderImages={this.handleImage}
                                coverInputId='choose-motorbike-cover-image'
                                coverImgId='motorbike-cover-image'
                                thumbnailInputId = 'choose-motorbike-thumbnail-image'
                                thumbnailImgId = 'motorbike-thumbnail-image'
                            />
                        </Col>
                    </Row>
                    <div className='product-bottom'>
                        <Button className='cancel-button' onClick={this.handleCancel}>HỦY</Button>
                        <Button className='save-button' onClick={this.handleSubmit} >THÊM</Button>
                    </div>
                </div>
            </ThemeProvider>
        )
    }
}
export default MotorbikeInsurance;