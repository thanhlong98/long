import React from 'react';
import {Button, Col, Input, Modal, ModalBody, ModalFooter, ModalHeader, Row} from "reactstrap";
import Select from "react-select";
import Checkbox from "@material-ui/core/Checkbox";
import FormControl from "@material-ui/core/FormControl";
import FormGroup from "@material-ui/core/FormGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles'

const theme = createMuiTheme({
    palette: {
        primary: {main: '#31BB71'},
    },
});
class CostPackage extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            modal: false,
            month: false,
            quarterly: false,
            halfYear: false,
            yearly: false,
            cost: null,
            protectedTime: null,
            paidTime:null,
            monthPrice: '',
            quarterlyPrice: '',
            halfYearPrice: '',
            yearlyPrice: '',
            edit: null,
        }
    }

    toggle = () => {
        this.setState({modal: !this.state.modal})
    };

    editCostPackage = (index) => {
        this.setState({
            edit: index,
            modal: true,
            "unit": "VND",
            cost: this.props.productPrice[index].cost_per_package,
            /*paidTime: this.props.productPrice[index].paid_time,
            protectedTime: this.props.productPrice[index].protected_time,
            period: this.props.productPrice[index].product_price_term,*/
        })
    };

    renderCostPackage = () =>{
        return(
            <div>
                {
                    this.props.productPrice && this.props.productPrice.length !== 0 &&
                    this.props.productPrice.map((costPackage, index)=>{
                        return(
                            <div>
                                <div className='supplementary-product-header'>
                                    <div> Gói {index+1}</div>
                                    <div className='d-flex ml-auto'>
                                        <i className='icon-supplementary-product icon-edit' onClick={()=>this.editCostPackage(index)}/>
                                        <i className='icon-supplementary-product icon-duplicate' onClick={()=>this.props.onDuplicate(index)}/>
                                        <i className='icon-supplementary-product icon-delete' onClick={()=>this.props.onDelete(index)}/>
                                    </div>
                                </div>
                                <div className='supplementary-product-content'>
                                    <Row>
                                        <Col sm={4}>
                                            <div>Giá:</div>
                                            <div>Thời gian bảo vệ:</div>
                                            <div>Thời gian đóng phí:</div>
                                            <div>Kỳ hạn:</div>
                                        </Col>
                                        <Col sm={8}>
                                            <div>{costPackage.cost_per_package} {costPackage.unit}</div>
                                            <div>{costPackage.protected_time}</div>
                                            <div>{costPackage.paid_time}</div>
                                        </Col>
                                    </Row>
                                    <Row>
                                        {costPackage.product_price_term && costPackage.product_price_term.length !== 0 &&
                                        costPackage.product_price_term.map((element, idx)=>{
                                            return(
                                                <Col sm={6}>
                                                    <div className='package-period'>{element.term}: {element.cost_per_term}đ</div>
                                                </Col>
                                            )
                                        })}
                                    </Row>
                                </div>
                            </div>
                        )
                    })
                }
            </div>
        )
    };

    handleSelectOptions = () => {
        let options = [];
        for(let i= 1; i<=40; i++){
            options.push({value: i, label: i});
        }
        return options
    };

    handleCheckboxChange = (event,name) => {
        this.setState({[name]: event.target.checked})
    };

    handleCostChange = (event) => {
        this.setState({cost: event.target.value})
    };

    handleChangeProtectedTime = (event) => {
        this.setState({protectedTime: event.value})
    };
    handleChangePaidTime = (event) => {
        this.setState({paidTime: event.value})
    };
    periodPrice = (event) => {
        this.setState({[event.target.name]: event.target.value})
    };

    handleClickDone = () => {
        let period = [];
        if(this.state.month){
            period.push({ "cost_per_term": this.state.monthPrice,"term": 'monthly'})
        }
        if(this.state.quarterly){
            period.push({ "cost_per_term": this.state.quarterlyPrice,"term": 'quarterly'})
        }
        if(this.state.halfYear){
            period.push({ "cost_per_term": this.state.halfYearPrice,"term": 'half_of_year'})
        }
        if(this.state.yearly){
            period.push({ "cost_per_term": this.state.yearlyPrice,"term": 'yearly'})
        }
        if(this.state.edit !== null){
            this.props.onEdit(this.state.edit,this.state.cost, this.state.protectedTime, this.state.paidTime, period)
        } else {
            this.props.onSubmit(this.state.cost, this.state.protectedTime, this.state.paidTime, period);
        }
        this.setState({
            cost: null,
            modal: false,
            month: false,
            quarterly: false,
            halfYear: false,
            yearly: false,
            monthPrice: '',
            quarterlyPrice: '',
            halfYearPrice: '',
            yearlyPrice: '',
            edit: null,
        })
    };

    render() {
        return(
            <div>
                <ThemeProvider theme={theme}>
                <div className='product-detail-title'>Gói giá</div>
                {this.renderCostPackage()}
                <i className='icon-add' onClick={this.toggle}/>
                <Modal isOpen={this.state.modal} toggle={this.toggle}>
                    <ModalHeader
                        className='product-modal-header'
                        toggle={this.toggle}
                    >
                        Thêm sản phẩm bổ trợ
                    </ModalHeader>
                    <ModalBody className='supplementary-modal-content product-detail-period'>
                        <Row className='supplementary-row'>
                            <Col sm={4} className='product-detail-title'>Giá</Col>
                            <Col sm={8}>
                                <Input
                                    placeholder='Thêm giá'
                                    className='supplementary-modal product-detail-input'
                                    value={this.state.cost}
                                    onChange={this.handleCostChange}
                                />
                            </Col>
                        </Row>
                        <Row className='supplementary-row'>
                            <Col sm={4} className='product-detail-title'>Thời gian bảo vệ</Col>
                            <Col sm={8}>
                                <Select
                                    options={this.handleSelectOptions()}
                                    className="package-modal multi-select-period"
                                    classNamePrefix="select-period"
                                    onChange={this.handleChangeProtectedTime}
                                />
                            </Col>
                        </Row>
                        <Row className='supplementary-row'>
                            <Col sm={4} className='product-detail-title'>Thời gian đóng phí</Col>
                            <Col sm={8}>
                                <Select
                                    options={this.handleSelectOptions()}
                                    className="package-modal multi-select-period"
                                    classNamePrefix="select-period"
                                    onChange={this.handleChangePaidTime}
                                />
                            </Col>
                        </Row>
                        <Row className='supplementary-row'>
                            <Col sm={4} className='product-detail-title'>Chọn kỳ hạn</Col>
                            <Col sm={8}>
                                <FormControl className='content-period-checkbox'>
                                    <FormGroup >
                                        <div className='d-flex justify-content-between'>
                                        <FormControlLabel control={
                                            <Checkbox
                                                checked={this.state.month}
                                                onChange={(event)=>this.handleCheckboxChange(event,'month')}
                                                value={this.state.month}
                                                color="primary"
                                            />}
                                             label="Tháng"
                                        />
                                            {this.state.month &&
                                            <Input
                                                className='price-item'
                                                name='monthPrice'
                                                value={this.state.monthPrice}
                                                onChange={this.periodPrice}
                                            />
                                            }
                                        </div>
                                        <div className='d-flex justify-content-between'>
                                        <FormControlLabel control={
                                            <Checkbox
                                                checked={this.state.quarterly}
                                                onChange={(event)=>this.handleCheckboxChange(event,'quarterly')}
                                                value={this.state.quarterly}
                                                color="primary"
                                            />}
                                            label="Quý"
                                        />
                                            {this.state.quarterly &&
                                            <Input
                                                className='price-item'
                                                name='quarterlyPrice'
                                                value={this.state.quarterlyPrice}
                                                onChange={this.periodPrice}
                                            />
                                            }
                                        </div>
                                        <div className='d-flex justify-content-between'>
                                        <FormControlLabel control={
                                            <Checkbox
                                                checked={this.state.halfYear}
                                                onChange={(event)=>this.handleCheckboxChange(event,'halfYear')}
                                                value={this.state.halfYear}
                                                color="primary"
                                            />}
                                            label="Nửa năm"
                                        />
                                            {this.state.halfYear &&
                                            <Input
                                                className='price-item'
                                                name='halfYearPrice'
                                                value={this.state.halfYearPrice}
                                                onChange={this.periodPrice}
                                            />
                                            }
                                        </div>
                                        <div className='d-flex justify-content-between'>
                                        <FormControlLabel control={
                                            <Checkbox
                                                checked={this.state.yearly}
                                                onChange={(event)=>this.handleCheckboxChange(event,'yearly')}
                                                value={this.state.yearly}
                                                color="primary"
                                            />}
                                            label="Năm"
                                        />
                                            {this.state.yearly &&
                                            <Input
                                                className='price-item'
                                                name='yearlyPrice'
                                                value={this.state.yearlyPrice}
                                                onChange={this.periodPrice}
                                            />
                                            }
                                        </div>
                                    </FormGroup>
                                </FormControl>
                            </Col>
                        </Row>
                    </ModalBody>
                    <ModalFooter>
                        <Button onClick={this.handleClickDone}>XONG</Button>
                    </ModalFooter>
                </Modal>
                </ThemeProvider>
            </div>
        )
    }
}
export default CostPackage;