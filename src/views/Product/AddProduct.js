import React from 'react';
import {Button, Col, Input, Row} from "reactstrap";
import Select from "react-select";
import {SingleDatePicker} from "react-dates";
import moment from "moment";
import Switch from "@material-ui/core/Switch";
import FormControl from "@material-ui/core/FormControl";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Radio from "@material-ui/core/Radio";
import {createMuiTheme} from "@material-ui/core";
import {ThemeProvider} from "@material-ui/styles";
import EditorText from "./EditorText";
import SupplementaryProduct from "./SupplementaryProduct";
import CostPackage from "./CostPackage";
import {UserServices} from "../../services/UserServices";
import {ProductServices} from "../../services/ProductServices";
import SelectProductType from "./SelectProductType";
import {common} from "../../utils/Common";
import {Toast} from "../../utils/Toast";

const theme = createMuiTheme({
    palette: {
        primary: {main: '#31BB71'},
    },
});

class AddProduct extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            titleProduct: null,
            shortDescription: null,
            date: null,
            focused: false,
            switched: false,
            radioValue: null,
            fullDescription: null,
            extraProduct: [],
            productPrice: [],
            productType: null,
        }
    }

    handleChangeInput = (event) => {
        this.setState({[event.target.name]: event.target.value})
    };

    handleChangeSwitch = () => {
        this.setState({switched: !this.state.switched})
    };

    handleChangeRadio = (event) => {
        this.setState({radioValue: event.target.value})
    };

    handleEditorChange = (event) => {
        this.setState({fullDescription: event.target.getContent()})
    };

    addSupplementaryProduct = (productName,cost,description,editorValue) => {
        this.setState({
            extraProduct: this.state.extraProduct.concat([
                {
                    title: productName,
                    cost: cost,
                    short_description: description,
                    full_description:editorValue
                }
            ])
        })
    };

    deleteSupplementaryProduct = (idx) => {
        let newExtraProduct =this.state.extraProduct.filter((product, index)=> idx !== index);
        this.setState({extraProduct : newExtraProduct})
    };

    duplicateSupplementaryProduct = (idx) => {
        let newExtraProduct = [...this.state.extraProduct];
        newExtraProduct.push(newExtraProduct[idx]);
        this.setState({extraProduct : newExtraProduct})
    };

    editSupplementaryProduct = (idx,productName,cost,description,editorValue) => {
        let newExtraProduct = [...this.state.extraProduct];
        newExtraProduct[idx]={
            title: productName,
            cost: cost,
            short_description: description,
            full_description: editorValue
        };
        this.setState({extraProduct : newExtraProduct});
    };

    handleCancel = () => {
      window.location = '#/products';
    };

    handleAddProduct = async () => {
        let input = document.getElementById('add-product-image');
        let data = new FormData();
        data.append('provider', this.props.providerUid); // hard code
        data.append('cover', input.files[0]);
        data.append('title', this.state.titleProduct);
        data.append('highlight', this.state.switched);
        data.append('product_type', 'nhan_tho');
        data.append('short_description', this.state.shortDescription);
        data.append('full_description', this.state.fullDescription);
        data.append('product_price', JSON.stringify(this.state.productPrice));
        data.append('extra_product', JSON.stringify(this.state.extraProduct));
        let [success, body] = await ProductServices.addProduct(data);
        if(success){
            Toast.Success('Add product thanh cong');
            window.location = '#/products';
        } else {
            if(body&&body.errorCode === 401){
                await common.handleExpiredToken();
            } else {
                Toast.Fail(body && body.message);
            }
        }
    };

    handleSelectChange = (value) => {
       this.setState({productType: value})
    };

    addCostPackage = (cost, protectedTime, paidTime, period) => {
        this.setState({
            productPrice: this.state.productPrice.concat([
                {
                    "unit": "VND",
                    "cost_per_package": cost,
                    "paid_time": paidTime,
                    "protected_time": protectedTime,
                    "product_price_term": period,
                }
            ])
        })
    };

    deleteCostPackage = (idx) => {
        let newProductPrice =this.state.productPrice.filter((product, index)=> idx !== index);
        this.setState({productPrice : newProductPrice})
    };
    duplicateCostPackage = (idx) => {
        let newProductPrice = [...this.state.productPrice];
        newProductPrice.push(newProductPrice[idx]);
        this.setState({productPrice : newProductPrice})
    };
    editCostPackage = (idx,cost, protectedTime, paidTime, period) => {
        let newProductPrice = [...this.state.productPrice];
        newProductPrice[idx]={
            "unit": "VND",
            "cost_per_package": cost,
            "paid_time": paidTime,
            "protected_time": protectedTime,
            "product_price_term": period,
        };
        this.setState({productPrice : newProductPrice});
    };

    previewFile = () => {
        let preview = document.getElementById('add-image');
        let file    = document.getElementById('add-product-image').files[0];
        let reader  = new FileReader();
        /*reader.onload = function(e) {
            preview.src = reader.result;
        };*/
        reader.addEventListener("load", function () {
            preview.src = reader.result;
        }, false);
        if(file) {
            reader.readAsDataURL(file);
        }
    };

    render() {
        return(
            <ThemeProvider theme={theme}>
                <div className='product-detail'>
                    <Row>
                        <Col lg={7} sm={12}>
                            <div className='product-detail-title'>Tên sản phẩm</div>
                            <Input
                                className='product-detail-input'
                                name='titleProduct'
                                value={this.state.titleProduct}
                                onChange={this.handleChangeInput}
                                placeholder='Thêm tên sản phẩm'
                            />
                            <div className='product-detail-title'>Mô tả</div>
                            <Input
                                type="textarea"
                                className='product-detail-input'
                                name='shortDescription'
                                value={this.state.shortDescription}
                                onChange={this.handleChangeInput}
                                placeholder='Thêm mô tả'
                            />
                            <div className='product-detail-title'>Nội dung</div>
                            <EditorText
                                value={this.state.fullDescription}
                                onChange={this.handleEditorChange}
                                height={246}
                            />
                            <div>
                                <SupplementaryProduct
                                    extraProduct={this.state.extraProduct}
                                    onDone={this.addSupplementaryProduct}
                                    onDelete={this.deleteSupplementaryProduct}
                                    onDuplicate={this.duplicateSupplementaryProduct}
                                    onEdit={this.editSupplementaryProduct}
                                />
                            </div>
                            <div>
                                <CostPackage
                                    productPrice={this.state.productPrice}
                                    onSubmit = {this.addCostPackage}
                                    onDelete={this.deleteCostPackage}
                                    onDuplicate={this.duplicateCostPackage}
                                    onEdit={this.editCostPackage}

                                />
                            </div>
                        </Col>
                        <Col lg={5} sm={12} className='left-content'>
                            {/*<SelectProductType
                                onChange={this.handleSelectChange}
                            />*/}
                            <div className='product-detail-title'>Nhóm sản phẩm</div>
                            <div className='add-product-type'>Nhân thọ</div>
                            <div className='product-detail-title'>Ngày đăng</div>
                            <div className='date-time'>
                                <SingleDatePicker
                                    date={this.state.date}
                                    onDateChange={date => this.setState({date})}
                                    focused={this.state.focused}
                                    onFocusChange={({focused}) => this.setState({focused})}
                                    id="date"
                                    numberOfMonths={1}
                                    placeholder="dd/mm/yyyy"
                                    showDefaultInputIcon={true}
                                    inputIconPosition='after'
                                    displayFormat="DD/MM/YYYY"
                                    isOutsideRange={() => false}
                                />
                            </div>
                            <div className='special-product'>
                                <div className='product-detail-title'>Sản phẩm nổi bật</div>
                                <Switch
                                    className='ml-auto'
                                    checked={this.state.switched}
                                    value="checkedA"
                                    onChange={this.handleChangeSwitch}
                                    inputProps={{'aria-label': 'secondary checkbox'}}
                                    color="primary"
                                />
                            </div>
                            <div className='show-checkbox'>
                                <div className='product-detail-title'>Hiển thị</div>
                                <FormControl>
                                    <RadioGroup
                                        className='mt-3'
                                        onChange={this.handleChangeRadio}
                                    >
                                        <FormControlLabel value="Ẩn" control={<Radio color="primary"/>}
                                                          label="Ẩn"/>
                                        <FormControlLabel value="Hiện" control={<Radio color="primary"/>}
                                                          label="Hiện"/>
                                    </RadioGroup>
                                </FormControl>
                            </div>
                            <div>
                                <div className='product-detail-title'>Ảnh bìa</div>
                                <input
                                    className='mt-4'
                                    id='add-product-image'
                                    type='file'
                                    onChange={this.previewFile}
                                />
                                <img
                                    className='mt-4'
                                    src={require("../../assets/images/image-product.svg")} width='100%'
                                    height='100%'
                                    id='add-image'
                                />
                            </div>
                        </Col>
                    </Row>
                    <div className='product-bottom'>
                        <Button className='cancel-button' onClick={this.handleCancel}>HỦY</Button>
                        <Button className='save-button' onClick={this.handleAddProduct}>THÊM</Button>
                    </div>
                </div>
            </ThemeProvider>

        )
    }
}
export default AddProduct;