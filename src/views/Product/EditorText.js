import React from 'react';
import { Editor } from '@tinymce/tinymce-react';

class EditorText extends React.Component{
    render() {
        return(
            <div className='text-editor'>
                <Editor
                    initialValue={this.props.value}
                    init={{
                        height: this.props.height,
                        //images_upload_url: 'postAcceptor.php',
                        //images_upload_base_path: '/some/basepath',
                        //images_upload_credentials: true,
                        plugins: [
                            'advlist autolink lists link charmap print preview anchor',
                            'searchreplace visualblocks code fullscreen',
                            'insertdatetime table paste code help wordcount',
                        ],
                        toolbar:
                            'undo redo | formatselect | bold italic backcolor | \
                            alignleft aligncenter alignright alignjustify | \
                            bullist numlist outdent indent | removeformat | help'
                    }}
                    //value={this.props.value}
                    onChange={this.props.onChange}
                />
            </div>
        )
    }
}
export default EditorText;