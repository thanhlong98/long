import React, { Component } from 'react';
import IMSTable from "../../utils/IMSTable";
import { transactionServices } from '../../services/TransactionServices';
import moment from "moment";

class ETransactionList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            columns: [{
                dataField: 'code',
                text: 'Mã giao dịch',
                formatter:this.codeFormatter,
                headerClasses:'header-table-code'
            }, {
                dataField: 'title',
                text: 'Sản phẩm',
                formatter:this.titleFormatter,
                headerClasses:'header-table-transaction'
            }, {
                dataField: 'update',
                text: 'Ngày tạo',
                headerClasses:'header-table-transaction-update'
            }, {
                dataField: 'customer',
                text: 'Khách hàng',
                headerClasses:'header-table-customer'
            }, {
                dataField: 'status',
                text: 'Trạng thái',
                formatter:this.statusFormatter,
                headerClasses:'header-table-status'
            }, {
                dataField: 'value',
                text: 'Giá trị hợp đồng',
                headerClasses:'header-table-value'
            }, {
                dataField: 'type',
                text: 'Loại',
                headerClasses:'header-table-value'
            }],
            transactionList: null,
            totalRow: null,
            page: 1,
            sizePerPage: 10,

        }
    }
    componentDidMount() {
        this.getETransactionList();
    }
    getETransactionList = () => {
        this.getETransactionListFollowPageNUmber(this.state.page, this.state.sizePerPage);
    };

    getETransactionListFollowPageNUmber = (page,sizePerPage) => {
        transactionServices.getETransactionList(page, sizePerPage)
            .then(data => {
                let transactionList = data.data.content.map((row, index)=>{
                    return(
                        {
                            title: row.title,
                            code:[row.transaction_code,row.uid, row.product_type],
                            update:moment(row.created_at).format('HH:mm:ss  DD/MM/YYYY'),
                            customer:row.user,
                            status:row.status,
                            value:row.total_cost.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.") + ' đ',
                            type: row.type_description,
                        }
                    )
                });
                this.setState({
                    transactionList: transactionList,
                    totalRow: data.data.totalRows,
                    page: page,
                    sizePerPage: sizePerPage,
                })
            })
            .catch(err => {
                console.log(err);
            })
    };
    titleFormatter = (cell,row, rowIndex) => {
       let title = cell? cell :null;
        return(
            <div>
                    <div className="title-transaction">{title}</div>
            </div>
        )
    };
    codeFormatter = (cell,row, rowIndex) => {
        let code = cell[0]? cell[0] :null;
        let uid = cell[1] ? cell[1] : null;
        let type = cell[2] ? cell[2] : null;
        return(
            <div>
                <a href={'#/transactions/'+type+'/'+uid+'/'} className="code-transaction">{code}</a>
            </div>
        )
    };

    statusFormatter = (cell,row, rowIndex) => {
        let status = cell? cell :null;
        return <div className={ status+'-status'+' product-status transaction-detail-status'}>{status}</div>
    };

    handleTableChange = (type, { page, sizePerPage }) => {
        this.getETransactionListFollowPageNUmber(page,sizePerPage);
    };
    render() {
        const { transactionList } = this.state;
        return (
            <div>
                {transactionList &&
                    <div className='product-page'>
                        <div className="product-content">
                            <div className="product-all"> Giao dịch </div>
                        </div>
                        <IMSTable
                            data={this.state.transactionList}
                            columns={this.state.columns}
                            totalRow={this.state.totalRow}
                            page={this.state.page}
                            sizePerPage={this.state.sizePerPage}
                            onTableChange={this.handleTableChange}
                        />
                    </div>
                }
            </div>
        );
    }
}

export default ETransactionList;