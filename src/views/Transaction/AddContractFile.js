import React from'react';
import {Col} from "reactstrap";

class AddContractFile extends React.Component{
    constructor(props){
        super(props);
        this.state={
            contractFiles: null,
        }
    }

    handleChange=()=>{
        let filesInput = document.getElementById('add-contract');
        let files = filesInput.files;
        let newImage = this.state.contractFiles ? [...this.state.contractFiles] : [];
        for (let i = 0; i < files.length; i++) {
            let picReader = new FileReader();
            picReader.addEventListener("load", (event)=> {
                files[i].src = event.target.result;
                this.setState({contractFiles: newImage},()=>{
                    this.props.onChange(newImage)});
            });
            picReader.readAsDataURL(files[i]);
            newImage.push(files[i]);
        }
        this.setState({contractFiles: newImage});
    };

    deleteFile = (index) => {
        let newFile = [...this.state.contractFiles].filter((e,idx)=> idx!==index);
        this.setState({contractFiles: newFile})
    };

    render() {
        return(
            <div>
                <div className='d-flex'>
                    <div className='product-detail-title mr-auto'>File(s)</div>
                    {this.props.status === 'signed' ? null : <label htmlFor='add-contract' className='image-label'>Thêm file</label>}
                </div>
                <input
                    multiple
                    id='add-contract'
                    type='file'
                    onChange={this.handleChange}
                    style={{display: 'none'}}
                />
                {this.props.status === 'signed' ? <div className='mt-3'/> : <div className='contact-file-info'>Tải lên file chứng nhận hoặc hợp đồng</div>}
                {this.props.files && this.props.files.map((element, index)=>{
                    let fileName;
                    if(element.includes('UserMotoInsurance/')) {
                        let fileNameIndex = this.props.files[index].search('UserMotoInsurance/');
                        fileName = this.props.files[index].slice(fileNameIndex + 18);
                    } else if(element.includes('UserHouseInsurance/')) {
                        let fileNameIndex = this.props.files[index].search('UserHouseInsurance/');
                        fileName = this.props.files[index].slice(fileNameIndex + 19);
                    }
                    return(
                        <div className='file-content d-flex'>
                            <div className='file-icon-container'>
                            <i className='icon-file'><span className='type-file'>{element.split('.').pop()}</span></i>
                            </div>
                            <a className='file-link' download href={element} target='_blank' data-toggle="tooltip" data-placement="bottom">
                                {fileName}
                            </a>
                        </div>
                    )
                })}
                {
                    this.state.contractFiles && this.state.contractFiles.map((element, index)=>{
                        return(
                            <div className='file-content d-flex'>
                                <div className='file-icon-container'>
                                <i className='icon-file'><span className='type-file'>{element.name.split('.').pop()}</span></i>
                                </div>
                                <a className='file-link' download href={element.src} title={element.name} data-toggle="tooltip" data-placement="bottom">
                                {element.name}
                                </a>
                                <i className='icon-delete-file' onClick={()=>this.deleteFile(index)}/>
                            </div>
                        )
                    })
                }

            </div>
        )
    }
}
export default AddContractFile;