import React from 'react';
import {Button, Col, Input, Row} from "reactstrap";
import {transactionServices} from "../../services/TransactionServices";
import {common} from "../../utils/Common";
import moment from "moment";
import {Toast} from "../../utils/Toast";

class ETransactionDetails extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            data: null
        }
    }

    componentDidMount() {
        this.getTransactionDetails();
    }

    getTransactionDetails = async () => {
        let uid=this.props.match.params.uid;
        let [success, body] = await transactionServices.getTransactionDetails(uid);
        if(success){
            this.setState({data: body.data})
        } else {
            if(body.errorCode === 401){
                await common.handleExpiredToken();
            } else {
                Toast.Fail(body && body.message);
            }
        }
    };

    conTract = () =>{
        let uid=this.props.match.params.uid;
        window.location='#/contract-detail/'+uid;
    }
    renderPeriod = () => {
        if(this.state.data.term === 1) {
            return <div className='transaction-details-period'>Tháng</div>
        }
        if(this.state.data.term === 4) {
            return <div className='transaction-details-period'>Quý</div>
        }
        if(this.state.data.term === 6) {
            return <div className='transaction-details-period'>Nửa năm</div>
        }
        if(this.state.data.term === 12) {
            return <div className='transaction-details-period'>Năm</div>
        }
    };

    updateStatus = async (type) => {
        let uid=this.props.match.params.uid;
        let [success, body] = await transactionServices.updateStatusETransaction(uid,type);
        if(success){
            window.location = '#/transactions';
        } else {
            if(body.errorCode === 401){
                await common.handleExpiredToken();
            } else {
                Toast.Fail(body && body.message);
            }
        }
    };

    checkStatus = () => {
        if(this.state.data.status === 'requested'){
            return  (
                <div>
                    <Button className='contact-accept-button' onClick={()=>this.updateStatus('accepted')}>CHẤP NHẬN</Button>
                    <Button className='contact-cancel-button' onClick={()=>this.updateStatus('canceled')}>HỦY GIAO DỊCH</Button>
                </div>
            )
        }
        if(this.state.data.status === 'paid'){
            return <Button className='contact-accept-button' onClick={this.conTract}>SOẠN HỢP ĐỒNG</Button>
        }
        if(this.state.data.status === 'accepted'){
            return <Button className='contact-cancel-button' onClick={()=>this.updateStatus('canceled')}>HỦY GIAO DỊCH</Button>
        }
    };

    renderStatus = () => {
        return <div className={this.state.data.status +'-status'+' product-status'}>{(this.state.data.status)}</div>
    };

    render() {
        // Hiện tại đang hard code ngày taọ transaction dòng 75
        const {data} = this.state;
        return(
            <div>
                {data &&
                <div className='e-transaction-details'>
                    <Row>
                        <Col sm={7}>
                            <div className='e-transaction-title'>Thông tin sản phẩm</div>
                            <div className='product-info'>
                                <div className='d-flex align-items-center'>
                                    <div className='code-title'>{data.provider_title}</div>
                                    <div className='code-text'>{data.transaction_code}</div>
                                </div>
                                <div className='product-name'>{data.product}</div>
                                <div className='product-cost'>{data.cost_per_package}</div>
                                <div className='d-flex align-items-center'>
                                    {this.renderStatus()}
                                    <div className='transaction-date-time'>16:56:10 05/12/2018</div>
                                </div>
                            </div>
                            <div className='e-transaction-title'>Gói giá đã chọn</div>
                            <div>
                                <div className='supplementary-product-header'>
                                    <div> Gói 1</div>
                                </div>
                                <div className='supplementary-product-content'>
                                    <Row>
                                        <Col sm={4}>
                                            <div>Giá:</div>
                                            <div>Thời gian bảo vệ:</div>
                                            <div>Thời gian đóng phí:</div>
                                        </Col>
                                        <Col sm={8}>
                                            <div>{data.cost_per_package} {data.unit}</div>
                                            <div>{data.protected_time}</div>
                                            <div>{data.paid_time}</div>
                                        </Col>
                                    </Row>
                                </div>
                            </div>
                            {data.extra_items && data.extra_items.length !== 0 && data.extra_items.map((item)=>{
                                return(
                                    <div>
                                        <div className='e-transaction-title'>Sản phẩm bổ trợ</div>
                                        <div>
                                            <div className='supplementary-product-header'>
                                                <div>{item.title}</div>
                                            </div>
                                            <div className='supplementary-product-content'>
                                                <Row>
                                                    <Col sm={4}>
                                                        <div>Giá:</div>
                                                        <div>Mô tả:</div>
                                                    </Col>
                                                    <Col sm={8}>
                                                        <div>{item.cost} {item.unit}</div>
                                                        <div>{item.short_description}</div>
                                                    </Col>
                                                </Row>
                                                <a href='#' className='detail-link'>Xem chi tiết</a>
                                            </div>
                                        </div>
                                    </div>
                                )
                            })}
                            {data.owner &&
                            <div>
                                <div className='e-transaction-title'>Chủ sở hữu</div>
                                <div className='product-owner'>
                                    <Row>
                                        <Col sm={4}>
                                            {data.owner.full_name && <div>Tên đầy đủ</div>}
                                            {data.owner.person_id && <div>CMND/CCCD</div>}
                                            {data.owner.gender && <div>Giới tính</div>}
                                            {data.owner.full_address && <div>Địa chỉ</div>}
                                            {data.owner.date_of_birth && <div>Ngày sinh</div>}
                                            {data.owner.phone_number && <div>Số điện thoại</div>}
                                            {data.owner.email && <div>Email</div>}
                                            {data.owner.occupation && <div>Nghề nghiệp</div>}
                                        </Col>
                                        <Col sm={8} className='product-owner-detail'>
                                            <div>{data.owner.full_name}</div>
                                            <div>{data.owner.person_id}</div>
                                            <div>{data.owner.gender}</div>
                                            <div>{data.owner.full_address}</div>
                                            <div>{moment(data.owner.date_of_birth).format('DD/MM/YYYY')}</div>
                                            <div>{data.owner.phone_number}</div>
                                            <div>{data.owner.email}</div>
                                            <div>{data.owner.occupation}</div>
                                        </Col>
                                    </Row>
                                </div>
                            </div>
                            }
                            {data.beneficiary &&
                                <div>
                                    <div className='e-transaction-title'>Người thụ hưởng</div>
                                    <div className='product-owner'>
                                        <Row>
                                            <Col sm={4}>
                                                {data.beneficiary.full_name && <div>Tên đầy đủ</div>}
                                                {data.beneficiary.person_id && <div>CMND/CCCD</div>}
                                                {data.beneficiary.gender && <div>Giới tính</div>}
                                                {data.beneficiary.full_address && <div>Địa chỉ</div>}
                                                {data.beneficiary.date_of_birth && <div>Ngày sinh</div>}
                                                {data.beneficiary.phone_number && <div>Số điện thoại</div>}
                                                {data.beneficiary.email && <div>Email</div>}
                                                {data.beneficiary.occupation && <div>Nghề nghiệp</div>}
                                            </Col>
                                            <Col sm={8} className='product-owner-detail'>
                                                <div>{data.beneficiary.full_name}</div>
                                                <div>{data.beneficiary.person_id}</div>
                                                <div>{data.beneficiary.gender}</div>
                                                <div>{data.beneficiary.full_address}</div>
                                                <div>{data.beneficiary.date_of_birth}</div>
                                                <div>{data.beneficiary.phone_number}</div>
                                                <div>{data.beneficiary.email}</div>
                                                <div>{data.beneficiary.occupation}</div>
                                            </Col>
                                        </Row>
                                    </div>
                                </div>
                            }
                        </Col>
                        <Col sm={5} className='left-content'>
                            <div className='e-transaction-title'>Kỳ hạn</div>
                            {this.renderPeriod()}
                            <div className='e-transaction-title'>Giá mỗi kỳ</div>
                            <div className='transaction-details-period'>{data.cost_per_term}</div>
                            {this.checkStatus()}

                        </Col>
                    </Row>
                </div>
                }
            </div>
        )
    }
}
export default ETransactionDetails;