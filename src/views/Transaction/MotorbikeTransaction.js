import React from 'react';
import {Button, Col, Row} from "reactstrap";
import AddContractFile from "./AddContractFile";
import {transactionServices} from "../../services/TransactionServices";
import moment from "moment";
import {Toast} from '../../utils/Toast.js';
import {common} from "../../utils/Common";

class MotorbikeTransaction extends React.Component{
    constructor(props){
        super(props);
        this.state={
            data: null,
            files: null,
            contractFiles: null,
        }
    }

    renderStatus = () => {
        return <div className={this.state.data.status +'-status'+' product-status'}>{(this.state.data.status)}</div>
    };

    componentDidMount() {
        this.getMotorbikeTransactionDetails();
    };

    getMotorbikeTransactionDetails = async () => {
        let [success, body] = await transactionServices.getMotorbikeTransactionDetails(this.props.match.params.uid);
        if(success){
            this.setState({data: body.data, files: body.data.contract_files})
        } else {
            if(body.errorCode === 401){
                await common.handleExpiredToken();
            } else {
                Toast.Fail(body && body.message);
            }
        }
    };

    handleSubmit = async () => {
        let data = new FormData;
        data.append('status', 'signed');
        if(this.state.contractFiles&&this.state.contractFiles.length !== 0) {
            for (let file of this.state.contractFiles) {
                data.append('contract_files', file);
            }
        }
        if(this.state.contractFiles && this.state.contractFiles.length !== 0) {
            let [success, body] = await transactionServices.updateMotorbikeInsuranceStatus(this.props.match.params.uid, data);
            if (success) {
                //Toast.Success('Tải file thành công');
                window.location='#/transactions';
            } else {
                if (body.errorCode === 401) {
                    await common.handleExpiredToken();
                } else {
                    Toast.Fail(body && body.message);
                }
            }
        } else {
            Toast.Fail('Vui lòng tải file hợp đồng');
        }
    };

    getContractFile = (contractFile) => {
     this.setState({contractFiles: contractFile})
    };

    renderSaveButton = () => {
        if(this.state.data.status === 'paid') {
            return (
                <div className='product-bottom ml-auto'>
                    <Button className='contact-accept-button' onClick={this.handleSubmit}>XÁC NHẬN</Button>
                </div>
            )
        }
    };

    formatMoney = (money) => {
        if(money){
            return money.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.");
        } else {
            return 0;
        }
    };

    render() {
        const {data} = this.state;
        return(
            <div className='e-transaction-details'>
                {data &&
                    <Row>
                    <Col sm={7}>
                        <div className='e-transaction-title'>Thông tin sản phẩm</div>
                        <div className='product-info'>
                            <div className='d-flex align-items-center'>
                                <div className='code-title'>{data.provider.provider_title}</div>
                                <div className='code-text'>{data.transaction_code}</div>
                            </div>
                            <div className='product-name'>{data.title}</div>
                            <div className='product-cost'>{data.engine_type}</div>
                            <div className='d-flex align-items-center'>
                                {this.renderStatus()}
                                <div className='transaction-date-time'>{moment(data.updated_at).format(' HH:mm:ss, DD/MM/YYYY')}</div>
                            </div>
                        </div>
                        <div>
                            <div className='e-transaction-title'>Tính phí</div>
                            <div className='product-owner'>
                                <div className='coverage-content'>
                                <div className='d-flex insurance-info-content'>
                                    <div className='mr-auto'>Phí bảo hiểm trách nhiệm dân sự bắt buộc:</div>
                                    <div>{this.formatMoney(data.compulsory_cost)} đ</div>
                                </div>
                                <div className='insurance-info-content'>
                                    <div className='d-flex'>
                                        <div className='mr-auto'>Phí bảo hiểm trách nhiệm dân sự tự nguyện:</div>
                                        <div>{this.formatMoney(data.voluntary_cost)} đ</div>
                                    </div>
                                    <div className='coverage-info'>{data.voluntary_person_coverage}, {data.voluntary_asset_coverage}</div>
                                </div>
                                <div className='insurance-info-content'>
                                    <div className='d-flex'>
                                        <div className='mr-auto'>Phí bảo hiểm tai nạn người ngồi trên xe:</div>
                                        <div>{this.formatMoney(data.accident_cost)} đ</div>
                                    </div>
                                    <div className='coverage-info'>Số người tham gia: 2</div>
                                    <div className='coverage-info'>Số tiền bảo hiểm bảo vệ tối đa: {data.accident_coverage} đ</div>
                                </div>
                                <div className='insurance-info-content'>
                                    <div className='d-flex'>
                                        <div className='mr-auto'>Phí Bảo hiểm cháy nổ:</div>
                                        <div>{this.formatMoney(data.fire_cost)} đ</div>
                                    </div>
                                    <div className='coverage-info'>Số người tham gia: 2</div>
                                    <div className='coverage-info'>Số tiền bảo hiểm bảo vệ tối đa: {data.fire_coverage} đ</div>
                                </div>
                                </div>
                                <div className='d-flex mt-3'>
                                    <div className='mr-auto'>Tổng phí bảo hiểm:</div>
                                    <div>{this.formatMoney(data.total_cost)} đ</div>
                                </div>
                            </div>
                            <div className='e-transaction-title'>Chủ sở hữu</div>
                            <div className='product-owner'>
                                <Row>
                                    <Col sm={4}>
                                        {data.owner.full_name && <div>Tên đầy đủ</div>}
                                        {data.owner.person_id && <div>CMND/CCCD</div>}
                                        {data.owner.gender && <div>Giới tính</div>}
                                        {data.owner.full_address && <div>Địa chỉ</div>}
                                        {data.owner.date_of_birth && <div>Ngày sinh</div>}
                                        {data.owner.phone_number && <div>Số điện thoại</div>}
                                        {data.owner.email && <div>Email</div>}
                                        {data.owner.occupation && <div>Nghề nghiệp</div>}
                                    </Col>
                                    <Col sm={8} className='product-owner-detail'>
                                        <div>{data.owner.full_name}</div>
                                        <div>{data.owner.person_id}</div>
                                        <div>{data.owner.gender}</div>
                                        <div>{data.owner.full_address}</div>
                                        <div>{moment(data.owner.date_of_birth).format('DD/MM/YYYY')}</div>
                                        <div>{data.owner.phone_number}</div>
                                        <div>{data.owner.email}</div>
                                        <div>{data.owner.occupation}</div>
                                    </Col>
                                </Row>
                            </div>
                            <div className='e-transaction-title'>Thông tin xe</div>
                            <div className='product-owner'>
                                <Row>
                                    <Col sm={4}>
                                        <div>Biển số</div>
                                        <div>Số khung</div>
                                        <div>Số máy</div>
                                    </Col>
                                    <Col sm={8} className='product-owner-detail'>
                                        <div>{data.number_plate}</div>
                                        <div>{data.frame_number}</div>
                                        <div>{data.machine_number}</div>
                                    </Col>
                                </Row>
                            </div>
                        </div>
                    </Col>
                    <Col sm={5} className='left-content'>
                        <div className='e-transaction-title'>Ngày bắt đầu hiệu lực</div>
                        <div className='add-product-type'>{moment(data.start_date).format('DD/MM/YYYY')}</div>
                        <div className='e-transaction-title'>Ngày hết hiệu lực</div>
                        <div className='add-product-type'>{moment(data.end_date).format('DD/MM/YYYY')}</div>
                        <AddContractFile
                            status = {this.state.data.status}
                            files = {this.state.files}
                            onChange={this.getContractFile}
                        />
                        {this.renderSaveButton()}
                    </Col>
                </Row>
                }
            </div>
        )
    }
}
export default MotorbikeTransaction;