import React from 'react';
import {transactionServices} from "../../services/TransactionServices";
import {Button, Col, Row} from "reactstrap";
import moment from "moment";
import AddContractFile from "./AddContractFile";
import {common} from "../../utils/Common";
import {Toast} from "../../utils/Toast";

class HouseTransaction extends React.Component {
    constructor(props){
        super(props);
        this.state={
            data: null,
            contractFiles: null,
            files: null,
        }
    }

    renderStatus = () => {
        return <div className={this.state.data.status +'-status'+' product-status'}>{(this.state.data.status)}</div>
    };

    componentDidMount() {
        this.getHouseTransactionDetails();
    };

    getHouseTransactionDetails = async () => {
        let [success, body] = await transactionServices.getHouseTransactionDetails(this.props.match.params.uid);
        if(success){
            this.setState({data: body.data,files: body.data.contract_files})
        } else {
            if(body.errorCode === 401){
                await common.handleExpiredToken();
            } else {
                Toast.Fail(body && body.message);
            }
        }
    };

    handleSubmit = async () => {
        let data = new FormData;
        data.append('status', 'signed');
        if(this.state.contractFiles&&this.state.contractFiles.length !== 0) {
            for (let file of this.state.contractFiles) {
                data.append('contract_files', file);
            }
        }
        if(this.state.contractFiles && this.state.contractFiles.length !== 0) {
            let [success, body] = await transactionServices.updateHouseInsuranceStatus(this.props.match.params.uid, data);
            if (success) {
                //Toast.Success('Tải file thành công');
                window.location='#/transactions';
            } else {
                if (body.errorCode === 401) {
                    await common.handleExpiredToken();
                } else {
                    Toast.Fail(body && body.message);
                }
            }
        } else {
            Toast.Fail('Vui lòng tải file hợp đồng');
        }
    };

    renderSaveButton = () => {
        if(this.state.data.status === 'paid') {
            return (
                <div className='product-bottom ml-auto'>
                    <Button className='contact-accept-button' onClick={this.handleSubmit}>XÁC NHẬN</Button>
                </div>
            )
        }
    };

    getContractFile = (contractFile) => {
        this.setState({contractFiles: contractFile})
    };

    formatMoney = (money) => {
        if(money){
            return money.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.");
        } else {
            return 0;
        }
    };

    render() {
        const {data} = this.state;
        return(
            <div className='e-transaction-details'>
                {data &&
                <Row>
                    <Col sm={7}>
                        <div className='e-transaction-title'>Thông tin sản phẩm</div>
                        <div className='product-info'>
                            <div className='d-flex align-items-center'>
                                <div className='code-title'>{data.provider.provider_title}</div>
                                <div className='code-text'>{data.transaction_code}</div>
                            </div>
                            <div className='product-name'>{data.title}</div>
                            <div className='product-cost'>{data.engine_type}</div>
                            <div className='d-flex align-items-center'>
                                {this.renderStatus()}
                                <div className='transaction-date-time'>{moment(data.updated_at).format(' HH:mm:ss, DD/MM/YYYY')}</div>
                            </div>
                        </div>
                        <div className='e-transaction-title'>Tính phí</div>
                        <div className='product-owner'>
                            <div className='coverage-content'>
                                <div className='d-flex insurance-info-content'>
                                    <div className='mr-auto'>Thời gian bảo vệ::</div>
                                    <div>{data.year ? data.year+' năm' : ''} </div>
                                </div>
                                <div className='insurance-info-content'>
                                    <div className='d-flex'>
                                        <div className='mr-auto'>Phần ngôi nhà::</div>
                                        <div>{this.formatMoney(data.house_cost)} đ</div>
                                    </div>
                                </div>
                                <div className='insurance-info-content'>
                                    <div className='d-flex'>
                                        <div className='mr-auto'>Tài sản bên trong:</div>
                                        <div>{this.formatMoney(data.inner_assets_cost)} đ</div>
                                    </div>
                                </div>
                            </div>
                            <div className='d-flex mt-3'>
                                <div className='mr-auto'>Tổng phí bảo hiểm:</div>
                                <div>{this.formatMoney(data.total_cost)} đ</div>
                            </div>
                        </div>
                        <div className='e-transaction-title'>Chủ sở hữu</div>
                        <div className='product-owner'>
                            <Row>
                                <Col sm={4}>
                                    {data.owner.full_name && <div>Tên đầy đủ</div>}
                                    {data.owner.person_id && <div>CMND/CCCD</div>}
                                    {data.owner.gender && <div>Giới tính</div>}
                                    {data.owner.full_address && <div>Địa chỉ</div>}
                                    {data.owner.date_of_birth && <div>Ngày sinh</div>}
                                    {data.owner.phone_number && <div>Số điện thoại</div>}
                                    {data.owner.email && <div>Email</div>}
                                    {data.owner.occupation && <div>Nghề nghiệp</div>}
                                </Col>
                                <Col sm={8} className='product-owner-detail'>
                                    <div>{data.owner.full_name}</div>
                                    <div>{data.owner.person_id}</div>
                                    <div>{data.owner.gender}</div>
                                    <div>{data.owner.full_address}</div>
                                    <div>{moment(data.owner.date_of_birth).format('DD/MM/YYYY')}</div>
                                    <div>{data.owner.phone_number}</div>
                                    <div>{data.owner.email}</div>
                                    <div>{data.owner.occupation}</div>
                                </Col>
                            </Row>
                        </div>
                        <div className='e-transaction-title'>Chủ sở hữu</div>
                        <div className='product-owner'>
                            <Row className='house-info'>
                                <Col sm={4}>
                                    <div>Địa chỉ</div>
                                    <div>Quyền sở hữu</div>
                                    <div>Diện tích sử dụng</div>
                                    <div>Loại hình ngôi nhà</div>
                                </Col>
                                <Col sm={8} className='product-owner-detail'>
                                    <div>{data.house_address}</div>
                                    <div>{data.user_status}</div>
                                    <div>{data.usage_square}</div>
                                    <div>{data.house_type}</div>
                                </Col>
                            </Row>
                            {data.usage_living && <div> Căn nhà được bảo hiểm được sử dụng cho mục đích sinh sống</div>}
                            {data.stable_architecture && <div className='mt-2'>Ngôi nhà được bảo hiểm được xây dựng bằng gạch, đá, xi măng</div>}
                        </div>
                    </Col>
                    <Col sm={5} className='left-content'>
                        <div className='e-transaction-title'>Ngày bắt đầu hiệu lực</div>
                        <div className='add-product-type'>{moment(data.start_date).format('DD/MM/YYYY')}</div>
                        <div className='e-transaction-title'>Ngày hết hiệu lực</div>
                        <div className='add-product-type'>{moment(data.end_date).format('DD/MM/YYYY')}</div>
                        <AddContractFile
                            status = {this.state.data.status}
                            files = {this.state.files}
                            onChange={this.getContractFile}
                        />
                        {this.renderSaveButton()}
                    </Col>
                </Row>
                }
            </div>
        )
    }
}
export default HouseTransaction;