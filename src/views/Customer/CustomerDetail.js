import React from "react";
import {Col, Row} from "reactstrap";
import {ContractServices} from "../../services/ContractServices";
import moment from "moment";
import {common} from "../../utils/Common";
import {Toast} from "../../utils/Toast";

class CustomerDetail extends React.Component{
    constructor(props){
        super(props);
        this.state={
            data: null,
        }
    }

    componentDidMount = async ()=> {
        let customerId=this.props.match.params.uid;
        let [success, body] = await ContractServices.getCustomerDetail(customerId)
        if(success){
            this.setState({data: body.data})
        } else {
            if(body.errorCode === 401){
                await common.handleExpiredToken();
            } else {
                Toast.Fail(body && body.message);
            }
        }
    };

    render() {
        const{data} = this.state;
        return(
            <div>
                {data &&
                <div>
                    <Row className='user-info-content m-0'>
                        <Col lg={2} sm={12} className='pl-0'>
                            <img
                                className='user-avatar'
                                src={data.user_profile && data.user_profile.avatar  ? data.user_profile.avatar : require("../../assets/images/avatar.svg")}
                                width='206px' height='206px'
                            />
                        </Col>
                        <Col lg={10} sm={12} className='pl-5'>
                            { data.user_profile && data.user_profile.full_name && <div className='user-name'>{data.user_profile.full_name}</div>}
                            <div className='update-customer'>Ngày cập nhật: 20/12/2019</div>
                            { data.user_profile && data.user_profile.gender && <div className='customer-gender'>{data.user_profile.gender}</div>}
                            <Row className='user-info'>
                                <Col lg={2} sm={12} className='left-info-content'>
                                    {data.user_profile && data.user_profile.email && <div>Email:</div>}
                                    {data.user_profile && data.user_profile.dob && <div>Ngày sinh:</div>}
                                    {data.phone_number && data.phone_number && <div>Số điện thoại:</div>}
                                    {data.user_profile &&  data.user_profile.address &&<div>Địa chỉ:</div>}
                                </Col>
                                <Col lg={10} sm={12} className='right-info-content'>
                                    {data.user_profile && data.user_profile.email && <div>{data.user_profile.email}</div>}
                                    {data.user_profile && data.user_profile.dob &&<div>{data.user_profile.dob}</div>}
                                    {data.phone_number && data.phone_number &&  <div>{data.phone_number}</div>}
                                    {data.user_profile && data.user_profile.address &&  <div>{data.user_profile.address}</div>}
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                    <div className='user-name contract-transaction'>Hợp đồng</div>
                    <table className='customer-table'>
                        <tr className='customer-table-header'>
                            <th className='first-column'>Mã hợp đồng</th>
                            <th>Tên hợp đồng</th>
                            <th>Nhóm</th>
                            <th>Trạng thái</th>
                            <th>Ngày cập nhật</th>
                        </tr>
                        {data.user_product_contracts.length !== 0 && data.user_product_contracts.map((row) => {
                            return (
                                <tr className='customer-table-row'>
                                    <td className='first-column'>{row.contract_code}</td>
                                    <td className='column-color'>{row.title}</td>
                                    <td className='column-color'>{row.product_type_id}</td>
                                    <td className={row.status+'-status '+'customer-status'}>{row.status}</td>
                                    <td className='column-color'>{moment(row.updated_at).format('DD/MM/YYYY, HH:mm:ss')}</td>
                                </tr>
                            )
                        })}
                    </table>
                    <div className='user-name contract-transaction'>Giao dịch</div>
                    <table className='customer-table customer-detail-footer'>
                        <tr className='customer-table-header'>
                            <th className='first-column'>Mã giao dịch</th>
                            <th>Sản phẩm</th>
                            <th>Ngày tạo</th>
                            <th>Trạng thái</th>
                            <th>Giá trị hợp đồng</th>
                        </tr>
                        {data.user_e_transactions.length !== 0 && data.user_e_transactions.map((row)=>{
                            return(
                                <tr className='customer-table-row'>
                                    <td className='code-column'>{row.transaction_code}</td>
                                    <td className='column-color'>{row.title}</td>
                                    <td className='column-color'>{moment(row.created_at).format('DD/MM/YYYY, HH:mm:ss')}</td>
                                    <td className={row.status+'-status '+'customer-status'}>{row.status}</td>
                                    <td className='column-color'>{row.cost_per_package}</td>
                                </tr>
                            )
                        })}
                    </table>
                </div>
                }
            </div>
        )
    }
}
export default CustomerDetail;