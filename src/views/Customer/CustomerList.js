import React, { Component } from 'react';
import IMSTable from "../../utils/IMSTable";
import { CustomerServices } from '../../services/CustomerServices';

class CustomerList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            columns: [{
                dataField: 'name',
                text: 'Tên khách hàng',
                headerClasses: "table-name",
                formatter: this.codeName,

            }, {
                dataField: 'email',
                text: 'Email',
                headerClasses: "table-email",
                formatter: this.codeEmail,

            }, {
                dataField: 'number',
                text: 'Số điện thoại',
                headerClasses: "table-number",
                formatter: this.codeNumber,

            }, {
                dataField: 'date',
                text: 'Ngày cập nhật',
                headerClasses: "table-date",
                formatter: this.codeDate,

            },],
            customerList: null,
            totalRow: null,
            page: 1,
            sizePerPage: 10,

        }
    }
    componentDidMount() {
        this.customerListGet(this.state.page, this.state.sizePerPage)
    }

    customerListGet = (page, sizePerPage) => {
        CustomerServices.getCustomer(page, sizePerPage)
            .then(data => {
                let customerList = data.data.content.map((row, index) => {
                    return (
                        {
                            name: [[row.user_profile && row.user_profile.avatar ? [<img src={row.user_profile.avatar} alt="" />] : [<img src={require("../../assets/images/avatar.svg")}/>]], [row.user_profile ? row.user_profile.full_name :null], row.uid],
                            email: [row.user_profile ? [row.user_profile.email] : ("chua co")],
                            number: row.phone_number,
                            data: "22/04/2020",
                        }
                    )
                });
                this.setState({
                    customerList: customerList,
                    totalRow: data.data.totalRows,
                    page: page,
                    sizePerPage: sizePerPage,
                })
            })
            .catch(err => {
                console.log(err);
            })
    };
    handleTableChange = (type, { page, sizePerPage }) => {
        this.customerListGet(page, sizePerPage);
    };
    codeName = (cell, row, rowIndex) => {
        let thumbnail = cell[0] ? cell[0] : null;
        let name = cell[1] ? cell[1] : null;
        let customerId = cell[2] ? cell[2] : null;
        return (
            <div>
                <div className="customer-list">
                    < div className="thubnail-title-img">
                        <a href={'#/customer/' + customerId}  className="thumbnail-title">{thumbnail}</a>
                    </div>
                    <a href={'#/customer/' + customerId} className="customer-name">{name}</a>
                </div>
            </div>
        )

    }
    codeEmail = (cell, row, rowIndex) => {
        let email = cell ? cell : null
        return (
            <div>
                <div className="customer-email">{email}</div>
            </div>
        )
    }
    codeNumber = (cell, row, rowIndex) => {
        let number = cell ? cell : null
        return (
            <div>
                <div className="customer-number">{number}</div>
            </div>
        )
    }
    codeDate = (cell, row, rowIndex) => {
        let date = cell ? cell : null
        return (
            <div>
                <div className="customer-date">{date}</div>
            </div>
        )
    }

    render() {
        const { customerList } = this.state;
        return (
            <div>
                {customerList &&
                    <div className='product-page'>
                        <div className="product-content">
                            <div className="product-all"> Khách hàng</div>
                        </div>
                        <div>

                        </div>
                        <IMSTable
                            data={this.state.customerList}
                            columns={this.state.columns}
                            totalRow={this.state.totalRow}
                            page={this.state.page}
                            sizePerPage={this.state.sizePerPage}
                            onTableChange={this.handleTableChange}
                        />
                    </div>
                }
            </div>
        );
    }
}

export default CustomerList;