import React from 'react';
import {UserServices} from "../../services/UserServices";
import {Col, Input, Row} from "reactstrap";

class UserProfile extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            data:null,
        }
    }
    componentDidMount() {
        UserServices.gettLoadAvatars()
            .then(data => {
                this.setState({
                    data: data.data
                });
            })
            .catch(err => {
                console.log(err);
            })
    }

    render() {
        const {data} = this.state;
        console.log(this.state.data);
        return(
            <div className='user-profile-container'>
                <p className='user-profile-header'>User profile</p>
                {data &&
                    <Row>
                    <Col sm={4}>
                        <div className='indemnify-input-label'>Tên đầy đủ:</div>
                        <div className='indemnify-input-label'>Ngày sinh:</div>
                        <div className='indemnify-input-label'>Giới tính:</div>
                        <div className='indemnify-input-label'>Email:</div>
                        <div className='indemnify-input-label'>Số điện thoại:</div>
                        <div className='indemnify-input-label'>Địa chỉ:</div>
                    </Col>
                    <Col sm={8}>
                        <Input
                            className='indemnify-input product-detail-input'
                            value={data.profile && data.profile.full_name}
                            name='minMoneyFireInsurance'
                        />
                        <Input
                            className='indemnify-input product-detail-input'
                            value={data.profile && data.profile.dob}
                            name='minMoneyFireInsurance'
                        />
                        <Input
                            value={data.profile && data.profile.gender}
                            className='indemnify-input product-detail-input'
                            name='minMoneyFireInsurance'
                        />
                        <Input
                            value={data.profile && data.profile.email}
                            className='indemnify-input product-detail-input'
                            name='minMoneyFireInsurance'
                        />
                        <Input
                            className='indemnify-input product-detail-input'
                            value={data.phone_number}
                            name='minMoneyFireInsurance'
                        />
                        <Input
                            value={data.profile && data.profile.address}
                            className='indemnify-input product-detail-input'
                            name='minMoneyFireInsurance'
                        />
                    </Col>
                </Row>
                }
            </div>
        )
    }
}
export default UserProfile;