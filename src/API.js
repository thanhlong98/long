//const API_URL = 'https://ims.horus.vn/';
const API_URL = 'http://115.79.29.63:8282/';
//const API_URL = 'http://192.168.1.62:8000/';


const LOGIN_USER = API_URL + 'api/token/';

const LOAD_AVATAR = API_URL + 'auth/get_profile/';

const GET_PRODUCT_LIST = (pageNumber, pageSize, productType)=>{
    if(productType === 'all-type'){
        return API_URL + `insurance/product/list/?currentPage=${pageNumber}&pageSize=${pageSize}`;
    } else {
        return API_URL + `insurance/product/list/?currentPage=${pageNumber}&pageSize=${pageSize}&product_type=${productType}`;
    }
};

const GET_PRODUCT_GROUPS = (currentPage, pageSize) =>{
    if(currentPage && pageSize ){
        return API_URL + `insurance/product/producttype/list/?currentPage=${currentPage}&pageSize=${pageSize}`;
    }
    else if (currentPage){
        return API_URL + `insurance/product/producttype/list/?currentPage=${currentPage}`;
    }
    else if(pageSize){
        return API_URL + `insurance/product/producttype/list/?pageSize=${pageSize}`;
    }
    return API_URL + 'insurance/product/producttype/list/';

};

const GET_PRODUCT_TYPE = (currentPage, pageSize) =>{
    if(currentPage && pageSize ){
        return API_URL + `insurance/product/producttype/list/?currentPage=${currentPage}&pageSize=${pageSize}`;
    }
    else if (currentPage){
        return API_URL + `insurance/product/producttype/list/?currentPage=${currentPage}`;
    }
    else if(pageSize){
        return API_URL + `insurance/product/producttype/list/?pageSize=${pageSize}`;
    }
    return API_URL + 'insurance/product/producttype/list/';

};

const GET_PRODUCT_DETAILS = (productId) =>{
    return API_URL + 'insurance/product/detail/'+ productId +'/'
};

const GET_E_TRANSACTION_LIST = (currentPage,pageSize) =>{
    return API_URL + `people/nonlife_transaction_list/?currentPage=${currentPage}&pageSize=${pageSize}`
}
const POST_EXTRA_PRODUCT = API_URL + 'insurance/product/extra/create/';

const UPDATE_PRODUCT =  (productId) => {
    return API_URL + 'insurance/product/update/' + productId + '/'
};

const VERIFY_TOKEN =  API_URL + 'api/token/verify/';

const ADD_PRODUCT = API_URL + 'insurance/product/create/';

const REFRESH_TOKEN = API_URL +'api/token/refresh/';

const GET_E_TRANSACTION_DETAILS = (uid) => {
    return API_URL + 'people/user_e_transaction/detail/' + uid + '/'
};

const UPDATE_STATUS_E_TRANSACTION = (uid) => {
    return API_URL + 'people/user_e_transaction/update/' + uid + '/'
};

const POST_CONTRACT_DETAIL = API_URL + 'people/user_product_contract/create/';

const GET_CONTRACT_LIST = (currentPage, pageSize)=>{
    return API_URL + `people/user_product_contract/list/?currentPage=${currentPage}&pageSize=${pageSize}`;
};

const GET_REVIEW_PRODUCT = (product_uid, currentPage, pageSize)=>{
    return API_URL + `people/user_transaction_review/list/?product_uid=${product_uid}&currentPage=${currentPage}&pageSize=${pageSize}`;
};

const POST_TOKEN = API_URL + 'auth/user_device_token/create/';

const GET_NOTIFICATION_LIST = (pageNumber, pageSize)=>{
    return API_URL + `notification/user_list/?currentPage=${pageNumber}&pageSize=${pageSize}`;
};

const GET_CUSTOMER = (currentPage,pageSize)=>{
    return API_URL + `people/customer/list/?currentPage=${currentPage}&pageSize=${pageSize}`;
};

const GET_CUSTOMER_LIST = (customerId)=>{
    return API_URL + 'people/customer/detail/'+ customerId + '/'
};

const GET_PAY_MENT_LIST = (currentPage,pageSize) => {
    // if(currentPage && pageSize ){
    //     return API_URL + `people/user_payment/list/?currentPage=${currentPage}&pageSize=${pageSize}`;
    // }
    // else if (currentPage){
    //     return API_URL + `people/user_payment/list/?currentPage=${currentPage}`;
    // }
    // else if(pageSize){
    //     return API_URL + `people/user_payment/list/?pageSize=${pageSize}`;
    // }
    // return API_URL + 'people/user_payment/list/';

    return API_URL + `people/user_payment/list/?currentPage=${currentPage}&pageSize=${pageSize}`;


}
const READ_NOTIFICATION = API_URL + 'notification/user_mark_all_read/';

const ADD_HOUSE_INSURANCE = API_URL + 'nonlife_insurance/house/create/';

const GET_NON_LIFE_INSURANCE = (currentPage,pageSize)=>{
    return API_URL + `nonlife_insurance/list/?currentPage=${currentPage}&pageSize=${pageSize}/`;
};

const GET_HOUSE_INSURANCE_DETAIL = (productId)=>{
    return API_URL + 'nonlife_insurance/house/detail/' + productId + '/';
};

const UPDATE_HOUSE_INSURANCE = (productId)=>{
    return API_URL + 'nonlife_insurance/house/update/' + productId + '/';
};

const ADD_MOTORBIKE_INSURANCE = API_URL + 'nonlife_insurance/moto/moto_insurance/create/';

const GET_MOTORBIKE_INSURANCE_DETAIL = (productId)=>{
    return API_URL + 'nonlife_insurance/moto/moto_insurance/detail/' + productId+'/';
};

const UPDATE_MOTORBIKE_INSURANCE =(productId)=>{
    return API_URL + 'nonlife_insurance/moto/moto_insurance/update/' + productId+'/';
};

const DELETE_IMAGE = (fileName,containerType) => {
    return API_URL + 'people/delete/'+fileName+'/?container_type='+containerType
};

const GET_MOTORBIKE_TRANSACTION_DETAILS = (uid)=>{
    return API_URL + 'people/user_moto_transaction/detail/'+uid+'/';
};
const GET_HOUSE_TRANSACTION_DETAILS = (uid)=>{
    return API_URL + 'people/user_house_transaction/detail/'+uid+'/';
};
const UPDATE_HOUSE_INSURANCE_STATUS = (uid)=>{
    return API_URL + 'people/user_house_transaction/update/'+uid+'/';
};

const UPDATE_MOTORBIKE_INSURANCE_STATUS = (uid)=>{
    return API_URL + 'people/user_moto_transaction/update/'+uid+'/';
};

const GET_UID_PROVIDER = API_URL + 'auth/get_profile/';

const GET_PROVIDER_DETAIL = (uid)=> {
    return API_URL + 'insurance/provider/detail/' + uid + '/';
};

export const API_CONST = {
    API_URL,
    LOGIN_USER,
    LOAD_AVATAR,
    GET_PRODUCT_LIST,
    GET_PRODUCT_GROUPS,
    GET_PRODUCT_DETAILS,
    GET_E_TRANSACTION_LIST,
    POST_EXTRA_PRODUCT,
    UPDATE_PRODUCT,
    ADD_PRODUCT,
    VERIFY_TOKEN,
    REFRESH_TOKEN,
    GET_E_TRANSACTION_DETAILS,
    UPDATE_STATUS_E_TRANSACTION,
    POST_CONTRACT_DETAIL,
    GET_CONTRACT_LIST ,
    GET_PRODUCT_TYPE,
    GET_REVIEW_PRODUCT ,
    POST_TOKEN,
    GET_NOTIFICATION_LIST,
    READ_NOTIFICATION,
    GET_CUSTOMER,
    GET_CUSTOMER_LIST,
    ADD_HOUSE_INSURANCE,
    GET_NON_LIFE_INSURANCE,
    GET_PAY_MENT_LIST,
    GET_HOUSE_INSURANCE_DETAIL,
    UPDATE_HOUSE_INSURANCE,
    ADD_MOTORBIKE_INSURANCE,
    GET_MOTORBIKE_INSURANCE_DETAIL,
    UPDATE_MOTORBIKE_INSURANCE,
    DELETE_IMAGE,
    GET_MOTORBIKE_TRANSACTION_DETAILS,
    GET_HOUSE_TRANSACTION_DETAILS,
    UPDATE_HOUSE_INSURANCE_STATUS,
    UPDATE_MOTORBIKE_INSURANCE_STATUS,
    GET_UID_PROVIDER,
    GET_PROVIDER_DETAIL,
};