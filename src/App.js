import React from 'react';
import './App.css';
import { HashRouter, Route, Switch } from 'react-router-dom';
import ForgetPassword from './views/ForgetPassword/ForgetPassword';
// import LogIn from './views/LogIn/LogIn'
import DefaultLayout from './containers/DefaultLayout/DefaultLayout';
import {ToastContainer} from "react-toastify";
import TermsofUse from './views/LogIn/TermsofUse';
import Helps from './views/LogIn/Helps';
import PrivacyPolicy from './views/LogIn/PrivacyPolicy';
import * as firebase from "firebase/app";
import 'firebase/messaging';
import {Toast} from "./utils/Toast";
import {NotificationServices} from "./services/NotificationServices";
import {UserServices} from "./services/UserServices";
import ContactUs from './views/LogIn/ContactUs';
import {common} from "./utils/Common";
import ProductGroups from './views/ProductGroups/ProductGroups';

const firebaseConfig = {
    apiKey: "AIzaSyBPERpEbsqj3RkmS5BVc6Ayn0kAiKfzl5w",
    authDomain: "einsurance-81201.firebaseapp.com",
    databaseURL: "https://einsurance-81201.firebaseio.com",
    projectId: "einsurance-81201",
    storageBucket: "einsurance-81201.appspot.com",
    messagingSenderId: "88179175334",
    appId: "1:88179175334:web:c7cf12ffd2685ba69d6967",
    measurementId: "G-NBGBQH1S5Y"
};
firebase.initializeApp(firebaseConfig);

let messaging;
if(!window.safari) {
    messaging = firebase.messaging();
    messaging.usePublicVapidKey("BG0_WAzr_jq2yMuCwwZylU5B64ErP3mfYUxrAdVnrrTLzlgY2lWoZw4cFNWbdCXKsR2b5tUCD0OlYvP22L4kKeY")

    Notification.requestPermission().then(() => {
        console.log('permission');
        return messaging.getToken();
    })
        .then(async (token) => {
            console.log(token);
            if (token) {
                /*let [success, body] = UserServices.createDeviceToken(token);
                if (success) {
                } else {
                    if(body.errorCode === 401){
                        await common.handleExpiredToken();
                    } else {
                        Toast.Fail(body && body.message);
                    }
                }*/
                localStorage.setItem('deviceToken', token);
            }
        })
        .catch((err) => {
            console.log(err)
        });
}

class App extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            unread: null,
        }
    }
    componentDidMount =  ()=> {
        if(messaging) {
            messaging.onMessage((payload) => {
                //alert(payload.notification.title)
                Toast.Success(payload.notification.title);
                if (window.location.hash !== '#/notifications') {
                    this.getRealTimeNotificationCount();
                } else {
                    this.readNoti();
                }
            });
            let hidden = "hidden";
            if (hidden in document) {
                document.addEventListener("visibilitychange", this.visibilityChange);
            }
        }
        /*else if ((hidden = "mozHidden") in document) {
            document.addEventListener("mozvisibilitychange", this.visibilityChange);
        }
        else if ((hidden = "webkitHidden") in document) {
            document.addEventListener("webkitvisibilitychange", this.visibilityChange);
        }
        else if ((hidden = "msHidden") in document) {
            document.addEventListener("msvisibilitychange", this.visibilityChange);
        }
        else if ("onfocusin" in document) {
            document.onfocusin = document.onfocusout = this.visitblityChange;
        }
        else {
            window.onpageshow = window.onpagehide
                = window.onfocus = window.onblur = this.visibilityChange;
        }*/
        if(localStorage.getItem('token')) {
            this.getRealTimeNotificationCount();
        }

    };

    getRealTimeNotificationCount = async () => {
        let [success, body] = await NotificationServices.getNotificationList(1, 10);
        if (success) {
            if(window.location.hash !== '#/notifications') {
                let unread = body.data.unread;
                this.setState({unread: unread});
            } else {
                this.readNoti();
            }
        } else {
                Toast.Fail(body && body.message);
            }
    };

    readNoti = async () => {
        let [success, body] = await NotificationServices.readNotification();
        if(success){
            this.setState({unread: 0});
        } else {
            if(body.errorCode === 401){
                await common.handleExpiredToken();
            } else {
                Toast.Fail(body && body.message);
            }
        }
    };

    visibilityChange = (event) => {
        if(document.visibilityState === 'visible') {
            if(localStorage.getItem('token')) {
                this.getRealTimeNotificationCount();
            }
        }
    };

    render() {
    return (
        <HashRouter>
            <Switch>
                <Route path="/contact" name="Contact" component={ContactUs}/>
                <Route path="/aaa" name="Login" component={ProductGroups}/>
                <Route path="/termsofuse" name="Teems Of Use" component={TermsofUse}/>
                <Route path="/privacy-policy" name="Privacy Policy" component={PrivacyPolicy}/>
                <Route path="/helps" name="Helps" component={Helps}/>
                <Route path="/forget-password" name="Forget Password" component={ForgetPassword}/>
                <Route path="/" name="Home"
                       render={()=>{
                           return(
                               <DefaultLayout
                                   unread = {this.state.unread}
                                   onRead = {this.readNoti}
                               />
                           )
                       }}/>
            </Switch>
            <ToastContainer/>
        </HashRouter>
    );
}
}
export default App;


