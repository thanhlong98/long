import {API_CONST} from "../API";
import { common } from "../utils/Common";
import {Toast} from "../utils/Toast";

const login = async (phoneNumber, password)=>{
    let response;
    let options = {
        method: 'POST',
        body: JSON.stringify({
            phone_number: phoneNumber,
            password: password,
            device_token: localStorage.getItem('deviceToken') ? localStorage.getItem('deviceToken') : '',
        }),
        headers: { 'Content-Type': 'application/json' },
    };
    let url = API_CONST.LOGIN_USER;
    try {
        response = await fetch(url, options);
        let body = await response.json();
        return [body.errorCode === 0, body];
    }
    catch (error) {
        if (response && response.statusText) {
            Toast.Fail(response.statusText)
        } else {
            Toast.Fail(error.message)
        }
        return [false, null];
    }
};

const verifyToken = async (refreshToken) => {
    let response;
    let options = {
        method: 'POST',
        body: JSON.stringify({token: refreshToken}),
        headers: { 'Content-Type': 'application/json' },
    };
    let url = API_CONST.VERIFY_TOKEN;
    try {
        response = await fetch(url, options);
        let body = await response.json();
        return [body.errorCode , body]
    }
    catch (error) {
        if (response && response.statusText) {
            Toast.Fail(response.statusText)
        } else {
            Toast.Fail(error.message)
        }
        //return [false, null];
    }
};

const refreshToken = async (refreshToken) => {
    let response;
    let options = {
        method: 'POST',
        body: JSON.stringify({refresh: refreshToken}),
        headers: { 'Content-Type': 'application/json' },
    };
    let url = API_CONST.REFRESH_TOKEN;
    try {
        response = await fetch(url, options);
        let body = await response.json();
        return [body.errorCode , body]
    }
    catch (error) {
        if (response && response.statusText) {
            Toast.Fail(response.statusText)
        } else {
            Toast.Fail(error.message)
        }
        //return [false, null];
    }
};
const getProductList = (pageNumber, pageSize, productType) => {
    let getObject = {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    };
    let url = API_CONST.GET_PRODUCT_LIST(pageNumber, pageSize, productType);

    return fetch(url, getObject)
        .then(async responseData => {
            if (responseData.status === 401){
                await common.handleExpiredToken();
            }
            if (responseData.status >= 400) {
                throw new Error("Bad response from server");
            }
            return responseData.json();
        })
        .then(data => {
            return data;
        })
        .catch(err => {
            console.log(err);
        });
}

const learningProductList =(pageNumber,pageSize,productType) => {
    let getObject = {
        method: 'GET',
        headers:{
            'Content-Type': 'application/json'
        }  
    };
    let url = API_CONST.GET_PRODUCT_LIST(pageNumber, pageSize, productType);
    return fetch(url,getObject)
    .then(responseData=>{
        if(responseData.status >= 400){
            throw new Error("Bad from server")
        }
        return responseData.json();
    })
    .then(data=>{
        return data;
    })
    .catch(err =>{
        console.log(err);
    })

    
}
const gettLoadAvatars = ()=>{
    let options ={
        method: 'GET',
      headers: { 'Content-Type': 'application/json','Authorization': 'Bearer ' + localStorage.getItem('token')  },
    };
    let url = API_CONST.LOAD_AVATAR
    return fetch(url, options)
    .then(responseData => {
        if (responseData.status >= 400) {
            throw new Error("Bad response from server");
        }
        return responseData.json();
    })
    .then(data => {
        return data;
    })
    .catch(err => {
        console.log(err);
    });
}
const getProductGroups = (currentPage,pageSize) =>{
    let getObject = {
        method: "GET",
        headers: {
            'Content-Type': 'application/json',
        }
    };
    getObject = common.createTokenHeader(getObject);
    let url = API_CONST.GET_PRODUCT_GROUPS(currentPage, pageSize);
    return fetch(url, getObject)
        .then(async responseData => {
            if (responseData.status === 401){
                await common.handleExpiredToken();
            }
            if (responseData.status >= 400) {
                throw new Error("Bad response from server");
            }
            return responseData.json();
        })
        .then(data => {
            return data;
        })
        .catch(err => {
            console.log(err);
        });
}

const getProducType = (currentPage,pageSize) =>{
    let getObject = {
        method: "GET",
        headers: {
            'Content-Type': 'application/json',
        }
    };
    getObject = common.createTokenHeader(getObject);
    let url = API_CONST.GET_PRODUCT_TYPE(currentPage, pageSize);
    return fetch(url, getObject)
        .then(responseData => {
            if (responseData.status >= 400) {
                throw new Error("Bad response from server");
            }
            return responseData.json();
        })
        .then(data => {
            return data;
            
        })
        .catch(err => {
            console.log(err);
        });
}

const createDeviceToken = async (token)=>{
    let response;
    let options = {
        method: 'POST',
        body: JSON.stringify({
            "device_type": "web_app",
            "device_token": token,
            "device_imei": "string",
            "device_model": "string",
            "user_agent": "provider_insurance",
            "user_ip": "string",
            "metadata": "string"
        }),
        headers: {
            'Content-Type': 'application/json',
        }
    };
    options = common.createTokenHeader(options);
    let url = API_CONST.POST_TOKEN;
    try {
        response = await fetch(url, options);
        let body = await response.json();
        return [body.errorCode === 0, body];
    }
    catch (error) {
        if (response && response.statusText) {
            Toast.Fail(response.statusText)
        } else {
            Toast.Fail(error.message)
        }
        return [false, null];
    }
};

const getProvider = async () => {
    let response;
    let options = {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' },
    };
    options = common.createTokenHeader(options);
    let url = API_CONST.GET_UID_PROVIDER;
    try {
        response = await fetch(url, options);
        let body = await response.json();
        return [body.errorCode === 0, body]
    }
    catch (error) {
        if (response && response.statusText) {
            Toast.Fail(response.statusText)
        } else {
            Toast.Fail(error.message)
        }
        return [false, null];
    }
};

const getProviderDetails = async (uid) => {
    let response;
    let options = {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' },
    };
    options = common.createTokenHeader(options);
    let url = API_CONST.GET_PROVIDER_DETAIL(uid);
    try {
        response = await fetch(url, options);
        let body = await response.json();
        return [body.errorCode === 0, body]
    }
    catch (error) {
        if (response && response.statusText) {
            Toast.Fail(response.statusText)
        } else {
            Toast.Fail(error.message)
        }
        return [false, null];
    }
};


export const UserServices = {
    login,
    getProductList,
    getProductGroups,
    getProducType,
    verifyToken,
    refreshToken,
    gettLoadAvatars,
    createDeviceToken,
    learningProductList,
    getProvider,
    getProviderDetails,
};