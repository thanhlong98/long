import {API_CONST} from "../API";
import { common } from "../utils/Common";
import {Toast} from "../utils/Toast";

const postContractDetailt = (data) =>{
    let getObject = {
        method: 'POST',
        body: data,
        headers: {  'Authorization': 'Bearer ' + localStorage.getItem('token') }
    };
    let url = API_CONST.POST_CONTRACT_DETAIL;
    return fetch(url, getObject)
        .then(responseData => {
            if (responseData.status >= 400) {
                 throw new Error("Bad response from server");
            }
            return responseData.json();
        })
        .then(data => {
            return data;
        })
        .catch(err => {
            console.log(err);
        });
}
const getContractList = (currentPage, pageSize) => {
    let getObject = {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    };
    let url = API_CONST.GET_CONTRACT_LIST(currentPage, pageSize);
    getObject = common.createTokenHeader(getObject);

    return fetch(url, getObject)
        .then(responseData => {
            if (responseData.status >= 400) {
                throw new Error("Bad response from server");
            }
            return responseData.json();
        })
        .then(data => {
            return data;
        })
        .catch(err => {
            console.log(err);
        });
}
const getCustomerDetail = async (customerId) =>{
    let response;
    let options = {
        method: 'GET',
        headers: {'Content-Type': 'application/json'}
    }
    options = common.createTokenHeader(options);
    let url = API_CONST.GET_CUSTOMER_LIST(customerId);
    try {
        response = await fetch(url, options);
        let body = await response.json();
        return [body.errorCode === 0, body];
    }
    catch (error) {
        if (response && response.statusText) {
            Toast.Fail(response.statusText)
        } else {
            Toast.Fail(error.message)
        }
        return [false, null];
    }
};
const getPayMentList=(currentPage,pageSize)=>{
    let getObject={
        method:'GET',
        headers:{
            'Content-Type': 'application/json',
        }
    };
    getObject = common.createTokenHeader(getObject);
    console.log(getObject);
    let url = API_CONST.GET_PAY_MENT_LIST(currentPage,pageSize);
    console.log(url);
    return fetch(url, getObject)
        .then(responseData => {
            if (responseData.status >= 400) {
                throw new Error("Bad response from server");
            }
            return responseData.json();
        })
        .then(data => {
            return data;
        })
        .catch(err => {
            console.log(err);
        });
};
export const ContractServices ={
    postContractDetailt,
    getContractList,
    getCustomerDetail,
    getPayMentList,
}