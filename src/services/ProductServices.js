import {API_CONST} from "../API";
import {common} from "../utils/Common";
import {Toast} from "../utils/Toast"

const getProductDetail = async (productID) => {
    let response;
    let options = {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' },
    };
    let url = API_CONST.GET_PRODUCT_DETAILS(productID);
    try {
        response = await fetch(url, options);
        let body = await response.json();
        if(body.errorCode === 401){
            common.handleExpiredToken();
        }
        return [body.errorCode === 0, body]
    }
    catch (error) {
        if (response && response.statusText) {
            Toast.Fail(response.statusText)
        } else {
            Toast.Fail(error.message)
        }
        return [false, null];
    }
};

const postExtraProduct = async (data)=>{
    let response;
    let options = {
        method: 'POST',
        body: JSON.stringify(data),
        headers: { 'Content-Type': 'application/json' },
    };
    options = common.createTokenHeader(options);
    let url = API_CONST.POST_EXTRA_PRODUCT;
    try {
        response = await fetch(url, options);
        let body = await response.json();
        return [body.errorCode === 0, body]
    }
    catch (error) {
        if (response && response.statusText) {
            Toast.Fail(response.statusText)
        } else {
            Toast.Fail(error.message)
        }
        return [false, null];
    }
};
const updateProduct = async (productID, data)=>{
    let response;
    let options = {
        method: 'PUT',
        body: data,
        headers: {  'Authorization': 'Bearer ' + localStorage.getItem('token') }
    };
    let url = API_CONST.UPDATE_PRODUCT(productID);
    try {
        response = await fetch(url, options);
        let body = await response.json();
        return [body.errorCode === 0, body]
    }
    catch (error) {
        if (response && response.statusText) {
            Toast.Fail(response.statusText)
        } else {
            Toast.Fail(error.message)
        }
        return [false, null];
    }
};
const addProduct = async (data)=>{
    let response;
    let options = {
        method: 'POST',
        body: data,
        headers: {  'Authorization': 'Bearer ' + localStorage.getItem('token') }
    };
    let url = API_CONST.ADD_PRODUCT;
    try {
        response = await fetch(url, options);
        let body = await response.json();
        return [body.errorCode === 0, body]
    }
    catch (error) {
        if (response && response.statusText) {
            Toast.Fail(response.statusText)
        } else {
            Toast.Fail(error.message)
        }
        return [false, null];
    }
};

const getReviewList =  (product_uid, currentPage, pageSize)=>{
    let options ={
        method: 'GET',
        headers: { 'Content-Type': 'application/json','Authorization': 'Bearer ' + localStorage.getItem('token')  },
    };
    let url = API_CONST.GET_REVIEW_PRODUCT(product_uid, currentPage, pageSize)
    //console.logreview url ” +url);
    return fetch(url, options)
    .then(responseData => {
        if (responseData.status >= 400) {
            throw new Error("Bad response from server");
        }
        return responseData.json();
    })
    .then(data => {
        return data;
    })
    .catch(err => {
        console.log(err);
    });
};

// nonlife house insurance
const addHouseInsurance = async (data)=>{
    let response;
    let options = {
        method: 'POST',
        body: data,
        headers: {  'Authorization': 'Bearer ' + localStorage.getItem('token') }
    };
    let url = API_CONST.ADD_HOUSE_INSURANCE;
    try {
        response = await fetch(url, options);
        let body = await response.json();
        return [body.errorCode === 0, body]
    }
    catch (error) {
        if (response && response.statusText) {
            Toast.Fail(response.statusText)
        } else {
            Toast.Fail(error.message)
        }
        return [false, null];
    }
};

const getNonLifeInsuranceList = async (page,sizePerPage)=>{
    let response;
    let options = {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' },
    };
    options = common.createTokenHeader(options);
    let url = API_CONST.GET_NON_LIFE_INSURANCE(page,sizePerPage);
    try {
        response = await fetch(url, options);
        let body = await response.json();
        return [body.errorCode === 0, body]
    }
    catch (error) {
        if (response && response.statusText) {
            Toast.Fail(response.statusText)
        } else {
            Toast.Fail(error.message)
        }
        return [false, null];
    }
};

const getHouseInsuranceDetail = async (productID) => {
    let response;
    let options = {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' },
    };
    let url = API_CONST.GET_HOUSE_INSURANCE_DETAIL(productID);
    try {
        response = await fetch(url, options);
        let body = await response.json();
        if(body.errorCode === 401){
            await common.handleExpiredToken();
        }
        return [body.errorCode === 0, body]
    }
    catch (error) {
        if (response && response.statusText) {
            Toast.Fail(response.statusText)
        } else {
            Toast.Fail(error.message)
        }
        return [false, null];
    }
};
const updateHouseInsurance = async (productID, data)=>{
    let response;
    let options = {
        method: 'PUT',
        body: data,
        headers: {  'Authorization': 'Bearer ' + localStorage.getItem('token') }
    };
    let url = API_CONST.UPDATE_HOUSE_INSURANCE(productID);
    try {
        response = await fetch(url, options);
        let body = await response.json();
        return [body.errorCode === 0, body]
    }
    catch (error) {
        if (response && response.statusText) {
            Toast.Fail(response.statusText)
        } else {
            Toast.Fail(error.message)
        }
        return [false, null];
    }
};

//nonlife motorbike insurance
const addMotorbikeInsurance = async (data)=>{
    let response;
    let options = {
        method: 'POST',
        body: data,
        headers: {  'Authorization': 'Bearer ' + localStorage.getItem('token') }
    };
    let url = API_CONST.ADD_MOTORBIKE_INSURANCE;
    try {
        response = await fetch(url, options);
        let body = await response.json();
        return [body.errorCode === 0, body]
    }
    catch (error) {
        if (response && response.statusText) {
            Toast.Fail(response.statusText)
        } else {
            Toast.Fail(error.message)
        }
        return [false, null];
    }
};

const getMotorbikeInsuranceDetail = async (productID) => {
    let response;
    let options = {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' },
    };
    let url = API_CONST.GET_MOTORBIKE_INSURANCE_DETAIL(productID);
    try {
        response = await fetch(url, options);
        let body = await response.json();
        if(body.errorCode === 401){
            await common.handleExpiredToken();
        }
        return [body.errorCode === 0, body]
    }
    catch (error) {
        if (response && response.statusText) {
            Toast.Fail(response.statusText)
        } else {
            Toast.Fail(error.message)
        }
        return [false, null];
    }
};

const updateMotorbikeInsurance = async (productID, data)=>{
    let response;
    let options = {
        method: 'PUT',
        body: data,
        headers: {  'Authorization': 'Bearer ' + localStorage.getItem('token') }
    };
    let url = API_CONST.UPDATE_MOTORBIKE_INSURANCE(productID);
    try {
        response = await fetch(url, options);
        let body = await response.json();
        return [body.errorCode === 0, body]
    }
    catch (error) {
        if (response && response.statusText) {
            Toast.Fail(response.statusText)
        } else {
            Toast.Fail(error.message)
        }
        return [false, null];
    }
};

const deleteImage = async (fileName,containerType) => {
    let response;
    let options = {
        method: 'DELETE',
        headers: { 'Content-Type': 'application/json' },
    };
    options = common.createTokenHeader(options);
    let url = API_CONST.DELETE_IMAGE(fileName,containerType);
    try {
        response = await fetch(url, options);
        let body = await response.json();
        return [body.errorCode === 0, body]
    }
    catch (error) {
        if (response && response.statusText) {
            Toast.Fail(response.statusText)
        } else {
            Toast.Fail(error.message)
        }
        return [false, null];
    }
};
export const ProductServices = {
    getProductDetail,
    postExtraProduct,
    updateProduct,
    addProduct,
    getReviewList,
    addHouseInsurance,
    getNonLifeInsuranceList,
    getHouseInsuranceDetail,
    updateHouseInsurance,
    addMotorbikeInsurance,
    getMotorbikeInsuranceDetail,
    updateMotorbikeInsurance,
    deleteImage,
};