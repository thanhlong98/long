import {API_CONST} from '../API'
import { common } from "../utils/Common";
import {Toast} from "../utils/Toast";

const getETransactionList = (currentPage,pageSize) =>{
    let getObject = {
        method: "GET",
        headers: {
            'Content-Type': 'application/json',
        }
    };
    getObject = common.createTokenHeader(getObject);
    let url = API_CONST.GET_E_TRANSACTION_LIST(currentPage,pageSize);
    return fetch(url, getObject)
        .then( async responseData => {
             if (responseData.status === 401){
                await common.handleExpiredToken();
            }
            if (responseData.status >= 400) {
                throw new Error("Bad response from server");
            }
            return responseData.json();
        })
        .then(data => {
            return data;
        })
        .catch(err => {
            console.log(err);
        });
};

const getTransactionDetails = async (uid)=>{
    let response;
    let options = {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' },
    };
    options = common.createTokenHeader(options);
    let url = API_CONST.GET_E_TRANSACTION_DETAILS(uid);
    try {
        response = await fetch(url, options);
        let body = await response.json();
        return [body.errorCode === 0, body]
    }
    catch (error) {
        if (response && response.statusText) {
            Toast.Fail(response.statusText)
        } else {
            Toast.Fail(error.message)
        }
        return [false, null];
    }
};

const updateStatusETransaction = async (uid, type)=>{
    let response;
    let options = {
        method: 'PUT',
        body: JSON.stringify({
            "status": type,
            "canceled_type": 'provider',
        }),
        headers: { 'Content-Type': 'application/json' },
    };
    options = common.createTokenHeader(options);
    let url = API_CONST.UPDATE_STATUS_E_TRANSACTION(uid);
    try {
        response = await fetch(url, options);
        let body = await response.json();
        return [body.errorCode === 0, body]
    }
    catch (error) {
        if (response && response.statusText) {
            Toast.Fail(response.statusText)

        } else {
            Toast.Fail(error.message)
        }
        return [false, null];
    }
};
const getMotorbikeTransactionDetails = async (uid)=>{
    let response;
    let options = {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' },
    };
    options = common.createTokenHeader(options);
    let url = API_CONST.GET_MOTORBIKE_TRANSACTION_DETAILS(uid);
    try {
        response = await fetch(url, options);
        let body = await response.json();
        return [body.errorCode === 0, body]
    }
    catch (error) {
        if (response && response.statusText) {
            Toast.Fail(response.statusText)
        } else {
            Toast.Fail(error.message)
        }
        return [false, null];
    }
};
const getHouseTransactionDetails = async (uid)=>{
    let response;
    let options = {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' },
    };
    options = common.createTokenHeader(options);
    let url = API_CONST.GET_HOUSE_TRANSACTION_DETAILS(uid);
    try {
        response = await fetch(url, options);
        let body = await response.json();
        return [body.errorCode === 0, body]
    }
    catch (error) {
        if (response && response.statusText) {
            Toast.Fail(response.statusText)
        } else {
            Toast.Fail(error.message)
        }
        return [false, null];
    }
};

const updateHouseInsuranceStatus = async (uid, data)=>{
    let response;
    let options = {
        method: 'PUT',
        body: data,
        headers: {  'Authorization': 'Bearer ' + localStorage.getItem('token') }
    };
    let url = API_CONST.UPDATE_HOUSE_INSURANCE_STATUS(uid);
    try {
        response = await fetch(url, options);
        let body = await response.json();
        return [body.errorCode === 0, body]
    }
    catch (error) {
        if (response && response.statusText) {
            Toast.Fail(response.statusText)

        } else {
            Toast.Fail(error.message)
        }
        return [false, null];
    }
};

const updateMotorbikeInsuranceStatus = async (uid, data)=>{
    let response;
    let options = {
        method: 'PUT',
        body: data,
        headers: {  'Authorization': 'Bearer ' + localStorage.getItem('token') }
    };
    let url = API_CONST.UPDATE_MOTORBIKE_INSURANCE_STATUS(uid);
    try {
        response = await fetch(url, options);
        let body = await response.json();
        return [body.errorCode === 0, body]
    }
    catch (error) {
        if (response && response.statusText) {
            Toast.Fail(response.statusText)

        } else {
            Toast.Fail(error.message)
        }
        return [false, null];
    }
};
export const transactionServices = {
    getETransactionList,
    getTransactionDetails,
    updateStatusETransaction,
    getMotorbikeTransactionDetails,
    getHouseTransactionDetails,
    updateHouseInsuranceStatus,
    updateMotorbikeInsuranceStatus,
};
