import { API_CONST } from "../API";
import { common } from "../utils/Common";

const getCustomer = (currentPage,pageSize) =>{
    let getObject = {
        method: "GET",
        headers: {
            'Content-Type': 'application/json',
        }
    };
    getObject = common.createTokenHeader(getObject);
    let url = API_CONST.GET_CUSTOMER(currentPage,pageSize);
    return fetch(url, getObject)
        .then(responseData => {
            if (responseData.status >= 400) {
                throw new Error("Bad response from server");
            }
            return responseData.json();
        })
        .then(data => {
            return data;
        })
        .catch(err => {
            console.log(err);
        });
};
export const CustomerServices = {
    getCustomer,
}