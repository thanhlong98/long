import {common} from "../utils/Common";
import {API_CONST} from "../API";
import {Toast} from "../utils/Toast";

const getNotificationList = async (pageNumber, pageSize) => {
    let response;
    let options = {
        method: 'GET',
        headers: {'Content-Type': 'application/json'}
    }
    options = common.createTokenHeader(options);
    let url = API_CONST.GET_NOTIFICATION_LIST(pageNumber, pageSize);
    try {
        response = await fetch(url, options);
        let body = await response.json();
        return [body.errorCode === 0, body];
    }
    catch (error) {
        if (response && response.statusText) {
            Toast.Fail(response.statusText)
        } else {
            Toast.Fail(error.message)
        }
        return [false, null];
    }
};

const readNotification = async () => {
    let response;
    let options = {
        method: 'PUT',
        body: JSON.stringify({
            "read": true
        }),
        headers: {'Content-Type': 'application/json'}
    };
    options = common.createTokenHeader(options);
    let url = API_CONST.READ_NOTIFICATION;
    try {
        response = await fetch(url, options);
        let body = await response.json();
        return [body.errorCode === 0, body];
    }
    catch (error) {
        if (response && response.statusText) {
            Toast.Fail(response.statusText)
        } else {
            Toast.Fail(error.message)
        }
        return [false, null];
    }
};
export const NotificationServices = {
    getNotificationList,
    readNotification,
};