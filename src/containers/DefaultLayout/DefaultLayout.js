import React, { Component } from 'react';
import SidebarNav from "./SidebarNav";
import DefaultFooter from './DefaultFooter';
import DefaultHeader from './DefaultHeader'
import Routes from "./Routes";
import {UserServices} from "../../services/UserServices";
import {Toast} from "../../utils/Toast.js";
import {common} from "../../utils/Common";

class DefaultLayout extends Component {
    constructor(props){
        super(props);
        this.state = {
            loading: false,
            collapse:false,
            providerUid: null,
        }
    }

    checkToken = () => {
        let token = localStorage.getItem('token');
        if(!token){
            window.location = "#login";
        } else {
            this.setState({loading: true});
        }
    };

    handleClickCollapse = () => {
        this.setState({collapse: !this.state.collapse})
    };

    componentDidMount() {
        this.checkToken();
        this.getProfile();
    }

    getProfile = async () =>{
        let [success, body] = await UserServices.getProvider();
        if(success){
            this.setState({providerUid: body.data.provider_uid})
        } else {
            if(body&&body.errorCode === 401){
                await common.handleExpiredToken();
            } else {
                Toast.Fail(body && body.message);
            }
        }
    };

    render() {
        return (
            <div>
                { this.state.loading && this.state.providerUid &&
                <div className={this.state.collapse ? 'default-layout-expand' : "default-layout"}>
                    <DefaultHeader
                        collapse={this.state.collapse}
                    />
                    <SidebarNav
                        providerUid={this.state.providerUid}
                        unread = {this.props.unread}
                        collapse={this.state.collapse}
                        handleClickCollapse ={this.handleClickCollapse}
                    />
                    <div className='main-content'>
                    <Routes
                        providerUid={this.state.providerUid}
                        unread = {this.props.unread}
                        onRead = {this.props.onRead}
                    />
                    </div>
                    <DefaultFooter/>
                </div>
                }
            </div>
        );
    }
}

export default DefaultLayout;