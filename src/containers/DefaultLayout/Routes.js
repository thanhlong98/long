import React from "react";
import {constants} from "../../utils/Constant";
import Actives from "../../views/Actives/Actives";
import Product from "../../views/Product/ProductList";
import AddProduct from "../../views/Product/AddProduct";
import ProductGroups from "../../views/ProductGroups/ProductGroups";
import AddProductGroups from "../../views/ProductGroups/AddProductGroups";
import ProductDetails from "../../views/Product/ProductDetails";
import ETransactionList from "../../views/Transaction/ETransactionList";
import ContractList from "../../views/Contract/ContractList";
import ETransactionDetails from "../../views/Transaction/ETransactionDetails";
import ContractDetails from "../../views/Contract/ContractDetails";
import Notifications from "../../views/Notification/Notifications";
import { Switch, Route, Redirect} from "react-router-dom";
import LearningProductList from "../../views/Product/LearningProductList"
import HouseInsurance from "../../views/Product/HouseInsurance";
import MotorbikeInsurance from "../../views/Product/MotorbikeInsurance";
import TravelInsurance from "../../views/Product/TravelInsurance";
import CustomerList from "../../views/Customer/CustomerList";
import CustomerDetail from "../../views/Customer/CustomerDetail";
import HouseInsuranceDetail from "../../views/Product/HouseInsuranceDetail";
import MotorbikeInsuranceDetails from "../../views/Product/MotorbikeInsuranceDetails";
import MotorbikeTransaction from "../../views/Transaction/MotorbikeTransaction";
import HouseTransaction from "../../views/Transaction/HouseTransaction";
import UserProfile from "../../views/UserProfile/UserProfile";


class Routes extends React.Component{
    render() {
        return(
            <Switch>
                <Switch>
                    <Redirect exact from="/" to={constants.ROUTE_ACTIVE} />
                    <Route exact path = {constants.ROUTE_USER_PROFILE} name='User Profile' component={UserProfile}/>
                    <Route exact path = {constants.ROUTE_ACTIVE} name = 'Active' component = {Actives}/>
                    <Route exact path = {constants.ROUTE_PRODUCT} name = 'Product' component = {Product}/>
                    <Route exact path = {constants.ROUTE_LIFE_INSURANCE} name='Add Life Insurance '
                           render = {() => {
                               return <AddProduct
                                   providerUid={this.props.providerUid}
                               />
                           }}
                    />
                    <Route exact path = {constants.ROUTE_HOUSE_INSURANCE} name = 'Add House Insurance'
                           render = {() => {
                               return <HouseInsurance
                                   providerUid={this.props.providerUid}
                               />
                           }}
                    />

                    <Route exact path = {constants.ROUTE_MOTORBIKE_INSURANCE} name = 'Add Motorbike Insurance'
                           render = {() => {
                               return <MotorbikeInsurance
                                   providerUid={this.props.providerUid}
                               />
                           }}
                    />
                    <Route exact path = {constants.ROUTE_TRAVEL_INSURANCE} name = 'Add Travel Insurance' component = {TravelInsurance}/>
                    <Route exact path = {constants.ROUTE_PRODUCT_GROUP} name = 'Product Groups' component = {ProductGroups}/>
                    <Route exact path = {constants.ROUTE_ADD_PRODUCT_GROUP} name = 'Add Product Groups' component = {AddProductGroups}/>
                    <Route exact path = {constants.ROUTE_MOTORBIKE_INSURANCE_DETAIL} name = 'Motorbike Insurance Detail' component={MotorbikeInsuranceDetails}/>
                    <Route exact path = {constants.ROUTE_HOUSE_INSURANCE_DETAIL} name = 'House Insurance Detail' component={HouseInsuranceDetail}/>
                    <Route exact path = {constants.ROUTE_PRODUCT_DETAILS} name ='Product Details' component = {ProductDetails}/>
                    <Route exact path = {constants.ROUTE_TRANSACTION} name = 'E-Transaction List' component = {ETransactionList}/>
                    <Route exact path = {constants.ROUTE_CONTRACT_LIST} name = 'Contract-List' component = {ContractList}/>
                    <Route exact path = {constants.ROUTE_E_TRANSACTION_DETAILS} name = 'ETransaction Details' component = {ETransactionDetails}/>
                    <Route exact path = {constants.ROUTE_MOTORBIKE_TRANSACTION_DETAILS} name = 'Motorbike Transaction Details' component = {MotorbikeTransaction}/>
                    <Route exact path = {constants.ROUTE_HOUSE_TRANSACTION_DETAILS} name = 'House Transaction Details' component={HouseTransaction}/>
                    <Route exact path = {constants.ROUTE_CONTRACT_DETAIL} name = 'Contract-Detail' component = {ContractDetails}/>
                    <Route exact path = {constants.ROUTE_CUSTOMER} name = 'Customer' component = {CustomerList}/>
                    <Route exact path = {constants.ROUTE_CUSTOMER_DETAIL} name = 'Customer Detail' component = {CustomerDetail}/>
                    <Route exact path = {constants.ROUTE_NOTIFICATIONS} name = 'Notifications'
                           render = {()=>{
                               return (
                                   <Notifications
                                       unread = {this.props.unread}
                                       onRead = {this.props.onRead}
                                   />
                               )
                           }}
                    />
                </Switch>
            </Switch>
        )
    }
}

export default Routes;