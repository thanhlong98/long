import React from 'react';
import {HashRouter as Router, NavLink} from "react-router-dom";
import {constants} from "../../utils/Constant";
import {UserServices} from "../../services/UserServices";
import {common} from "../../utils/Common";
import {Toast} from "../../utils/Toast";

class SidebarNav extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            providerAvatar: null,
        }
    }

    renderUnreadNotification = () => {
        if (this.props.unread && this.props.unread > 0) {
            return (
                <span className='noti-number'>{this.props.unread}</span>
            )
        }
    };

    componentDidMount() {
        this.getProviderDetails();
    }

    getProviderDetails = async () => {
        let [success, body] = await UserServices.getProviderDetails(this.props.providerUid);
        if(success){
            this.setState({providerAvatar:body.data.logo});
        } else {
            if(body.errorCode === 401){
                await common.handleExpiredToken();
            } else {
                Toast.Fail(body && body.message);
            }
        }
    };

    render() {
        return(
            <div>
                {this.state.providerAvatar &&
                    <Router>
                    {this.props.collapse ?
                        <div className='sidebar-nav-collapse'>
                            <i className='icon-sidebar icon-menu' onClick={this.props.handleClickCollapse}/>
                            <NavLink to={constants.ROUTE_ACTIVE} className='sidebar-link' activeClassName="active">
                                <i className='icon-sidebar icon-active'/>
                            </NavLink>
                            <NavLink to={constants.ROUTE_PRODUCT}
                                     className='sidebar-link'
                                     activeClassName="active">
                                <i className='icon-sidebar icon-product'/>
                            </NavLink>
                            <NavLink to={constants.ROUTE_TRANSACTION} className='sidebar-link' activeClassName="active">
                                <i className='icon-sidebar icon-transaction'/>
                            </NavLink>
                            <NavLink to={constants.ROUTE_GIFT} className='sidebar-link' activeClassName="active">
                                <i className='icon-sidebar icon-gift'/>
                            </NavLink>
                            <NavLink to={constants.ROUTE_CUSTOMER} className='sidebar-link' activeClassName="active">
                                <i className='icon-sidebar icon-customer'/>
                            </NavLink>
                            <NavLink to={constants.ROUTE_CONTRACT_LIST} className='sidebar-link'
                                     activeClassName="active">
                                <i className='icon-sidebar icon-contract-list'/>
                            </NavLink>
                            {/* <NavLink to={constants.ROUTE_TEXT} className='sidebar-link' activeClassName="active">
                            <i className='icon-sidebar icon-text'/>
                        </NavLink> */}
                            <NavLink to={constants.ROUTE_NOTIFICATIONS} className='sidebar-link show-noti'
                                     activeClassName="active">
                                <i className='icon-sidebar icon-notification'/>
                                {this.renderUnreadNotification()}
                            </NavLink>
                            <NavLink to={constants.ROUTE_SETTING} className='sidebar-link' activeClassName="active">
                                <i className='icon-sidebar icon-setting'/>
                            </NavLink>
                        </div>
                        :
                        <div className='sidebar-nav'>
                            <div className='d-flex logo-image align-items-center'>
                                <i className='icon-sidebar icon-menu' onClick={this.props.handleClickCollapse}/>
                                <img
                                    src={this.state.providerAvatar ? this.state.providerAvatar : require('../../assets/images/logo-image.svg')} width='36px' height='36px'
                                />
                            </div>
                            <NavLink to={constants.ROUTE_ACTIVE} className='sidebar-link' activeClassName="active">
                                <i className='icon-sidebar icon-active'/>
                                <span>Hoạt động</span>
                            </NavLink>
                            <NavLink to={constants.ROUTE_PRODUCT}
                                     className='sidebar-link '
                                     activeClassName="active">
                                <i className='icon-sidebar icon-product'/>
                                <span>Sản phẩm</span>
                            </NavLink>
                            <NavLink to={constants.ROUTE_TRANSACTION} className='sidebar-link' activeClassName="active">
                                <i className='icon-sidebar icon-transaction'/>
                                <span>Giao dịch</span>
                            </NavLink>
                            <NavLink to={constants.ROUTE_GIFT} className='sidebar-link' activeClassName="active">
                                <i className='icon-sidebar icon-gift'/>
                                <span>Ưu đãi</span>
                            </NavLink>
                            <NavLink to={constants.ROUTE_CUSTOMER} className='sidebar-link' activeClassName="active">
                                <i className='icon-sidebar icon-customer'/>
                                <span>Khách hàng</span>
                            </NavLink>
                            <NavLink to={constants.ROUTE_CONTRACT_LIST} className='sidebar-link'
                                     activeClassName="active">
                                <i className='icon-sidebar icon-contract-list'/>
                                <span>Hợp đồng</span>
                            </NavLink>
                            <NavLink to={constants.ROUTE_NOTIFICATIONS} className='sidebar-link show-noti'
                                     activeClassName="active">
                                <i className='icon-sidebar icon-notification'/>
                                {this.renderUnreadNotification()}
                                <span>Thông báo</span>
                            </NavLink>
                            <NavLink to={constants.ROUTE_SETTING} className='sidebar-link' activeClassName="active">
                                <i className='icon-sidebar icon-setting'/>
                                <span>Cài đặt</span>
                            </NavLink>
                        </div>
                    }
                </Router>
                }
            </div>
        )
    }
}
export default SidebarNav;

