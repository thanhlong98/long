import React, { Component } from 'react';
import { UserServices } from '../../services/UserServices';
import {Dropdown, DropdownItem, DropdownMenu, DropdownToggle} from "reactstrap";
import {constants} from "../../utils/Constant";
class DefaultHeader extends Component {
    constructor(props) {
        super(props);
        this.state = {
            avatars: null,
            isOpen: false,
        }
    };

    componentDidMount() {
        this.getLoadAvatar()
    };

    getLoadAvatar = async () => {
        UserServices.gettLoadAvatars()
            .then(data => {
                this.setState({
                    avatars: data.data
                });
            })
            .catch(err => {
                console.log(err);
            })
    };

    toggle = () => {
        this.setState({isOpen: !this.state.isOpen})
    };

    handleLogout = () => {
        localStorage.removeItem('refreshToken');
        localStorage.removeItem('token');
        window.location = '#login';
    };

    render() {
        const { avatars } = this.state;
        return (
            <div>
                {avatars &&
                    <div className={this.props.collapse ? 'header-content header-content-collapse' : "header-content"} >
                        <div className="search-icons">
                            <img src={require("../../assets/icons/search.svg")} alt="" width='16px'
                                height='16px' />
                        </div>
                        <div className='search-mobile-product'>
                            <input className="search-mobile" type="text" name="search"
                                placeholder="Tìm kiếm"
                                id='input-search'
                            />
                        </div>
                        <div className="image-header">
                            <Dropdown isOpen={this.state.isOpen} toggle={this.toggle}>
                                <DropdownToggle className='avatar-button'>
                                    <img
                                        onClick={this.handleClick}
                                        src={avatars.profile && avatars.profile.avatar? avatars.profile.avatar : require("../../assets/images/avatar.svg")}
                                        alt="" width='36px'
                                        height='36px'
                                    />
                                </DropdownToggle>
                                <DropdownMenu right className='avatar-menu'>
                                    <DropdownItem className='avatar-item'>
                                        <div className='d-flex align-items-center'>
                                            <i className='icon-type icon-user-profile'/>
                                            <a href={constants.HASH_USER_PROFILE} className='user-profile-link'>User Profile</a>
                                        </div>
                                    </DropdownItem>
                                    <DropdownItem className='avatar-item' onClick={this.handleLogout}>
                                        <div className='d-flex align-items-center'>
                                            <i className='icon-type icon-logout'/>
                                            Logout
                                        </div>
                                    </DropdownItem>
                                </DropdownMenu>
                            </Dropdown>
                        </div>
                    </div>
                }
            </div>
        );
    }
}

export default DefaultHeader;