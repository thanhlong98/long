import React from 'react';
import BootstrapTable from 'react-bootstrap-table-next';
import paginationFactory from 'react-bootstrap-table2-paginator';
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import 'react-bootstrap-table2-toolkit/dist/react-bootstrap-table2-toolkit.min.css';
import 'react-bootstrap-table2-paginator/dist/react-bootstrap-table2-paginator.min.css';

const RemotePagination = ({ data, columns, page, sizePerPage, onTableChange, totalSize, prePageText, nextPageText})=> (
    <div className='ims-table'>
        <BootstrapTable
            remote
            keyField='id'
            data={data}
            columns={columns}
            rowClasses="table-row"
            headerClasses="table-header"
            pagination={paginationFactory({page, sizePerPage, totalSize, prePageText, nextPageText})}
            onTableChange={onTableChange}
        />
    </div>
);

class IMSTable extends React.Component{
    render() {
        return(
            <div>
                <RemotePagination
                    data={this.props.data}
                    columns={this.props.columns}
                    page={this.props.page}
                    sizePerPage={this.props.sizePerPage}
                    totalSize={this.props.totalRow}
                    onTableChange={this.props.onTableChange}
                    /*prePageText=<i className='icon-previous'/>
                    nextPageText= <i className='icon-next'/>*/
                />
            </div>
        )
    }
}
export default IMSTable;