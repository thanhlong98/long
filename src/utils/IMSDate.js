import React from 'react';
import {SingleDatePicker} from "react-dates";
import moment from "moment";

class IMSDate extends React.Component{
    constructor(props) {
        super(props);
        this.state = { focused: null };
    }

    render() {
        return(
            <div className='date-time'>
                <SingleDatePicker
                    date={this.props.date && moment(this.props.date,"YYYY/MM/DD")}
                    onDateChange={(date)=>this.props.onDateChange(date)}
                    focused={this.state.focused}
                    onFocusChange={({focused}) => this.setState({focused})}
                    id="date"
                    numberOfMonths={1}
                    placeholder="dd/mm/yyyy"
                    showDefaultInputIcon={true}
                    inputIconPosition='after'
                    displayFormat="DD/MM/YYYY"
                    isOutsideRange={() => false}
                />
            </div>
        )
    }
}
export default IMSDate;