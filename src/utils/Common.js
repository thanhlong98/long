import {UserServices} from "../services/UserServices";
import {constants} from "./Constant";

const getToken = () => {
    return localStorage[constants.TOKEN_VARIABLE];
}

const createTokenHeader = (header) => {
    let token = getToken();
    console.log(token);
    // token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNTcyNDkyNzcxLCJqdGkiOiJmNTE0MTQzYTIwNTU0ODkwOTNkNWEyZDQ2YTMxMjU2OCIsInVzZXJfaWQiOjh9.X1VfjHNv450z4uyQ2-YSuPB2p5ma5G0_OPdpcSFQ8yA';
    if(token){
        header.headers.Authorization = `Bearer ${token}`;
    }
    return header;
}

const handleExpiredToken = async () => {
    let [errorCode,body] = await UserServices.verifyToken(localStorage.getItem('refreshToken'));
    if(errorCode === 401){
        window.location = '#/login';
    }
    if(errorCode === 0){
        let [errorCode,body] = await UserServices.refreshToken(localStorage.getItem('refreshToken'));
        if(errorCode === 401){
            window.location = '#/login';
        }
        if(errorCode === 0){
            let token = body.data.access;
            localStorage.setItem('token', token);
        }
    }
};
export const common = {
    createTokenHeader,
    handleExpiredToken
}