
const TOKEN_VARIABLE = 'token';
const ROUTE_ACTIVE = '/active';
const ROUTE_PRODUCT = '/products';
const ROUTE_PRODUCT_GROUP= '/product-group';
const ROUTE_ADD_PRODUCT_GROUP = '/add-product-group';
const ROUTE_TRANSACTION= '/transactions';
const ROUTE_GIFT= '/gift';
const ROUTE_SETTING= '/setting';
const ROUTE_TEXT= '/text';
const ROUTE_CUSTOMER= '/customer';
const ROUTE_NOTIFICATIONS = '/notifications';
const ROUTE_PRODUCT_DETAILS = '/products/nhan_tho/:productId';
const ROUTE_HOUSE_INSURANCE_DETAIL = '/products/nha_cua/:productId';
const ROUTE_MOTORBIKE_INSURANCE_DETAIL = '/products/mo_to/:productId';
const ROUTE_CONTRACT_LIST = '/contract-list';
const ROUTE_CONTRACT_DETAIL ='/contract-detail/:uid';
const ROUTE_E_TRANSACTION_DETAILS = '/transactions/nhan_tho/:uid';
const ROUTE_MOTORBIKE_TRANSACTION_DETAILS = '/transactions/mo_to/:uid';
const ROUTE_HOUSE_TRANSACTION_DETAILS = '/transactions/nha_cua/:uid';
const PAGE_SIZE =10;
const ROUTE_LIFE_INSURANCE='/products/nhan_tho';
const ROUTE_HOUSE_INSURANCE='/products/nha_cua';
const ROUTE_MOTORBIKE_INSURANCE='/products/mo_to';
const ROUTE_TRAVEL_INSURANCE='/products/du_lich';
const ROUTE_CUSTOMER_DETAIL= '/customer/:uid';
const ROUTE_USER_PROFILE = '/user-profile';
const HASH_USER_PROFILE = '#user-profile';

export const constants = {
    TOKEN_VARIABLE,
    ROUTE_ACTIVE,
    ROUTE_PRODUCT,
    ROUTE_PRODUCT_GROUP,
    ROUTE_TRANSACTION,
    ROUTE_GIFT,
    ROUTE_SETTING,
    ROUTE_TEXT,
    ROUTE_CUSTOMER,
    ROUTE_NOTIFICATIONS,
    ROUTE_PRODUCT_DETAILS,
    ROUTE_ADD_PRODUCT_GROUP,
    ROUTE_LIFE_INSURANCE,
    ROUTE_CONTRACT_LIST,
    ROUTE_CONTRACT_DETAIL,
    ROUTE_E_TRANSACTION_DETAILS,
    PAGE_SIZE,
    ROUTE_HOUSE_INSURANCE,
    ROUTE_MOTORBIKE_INSURANCE,
    ROUTE_TRAVEL_INSURANCE,
    ROUTE_CUSTOMER_DETAIL,
    ROUTE_HOUSE_INSURANCE_DETAIL,
    ROUTE_MOTORBIKE_INSURANCE_DETAIL,
    ROUTE_MOTORBIKE_TRANSACTION_DETAILS,
    ROUTE_HOUSE_TRANSACTION_DETAILS,
    ROUTE_USER_PROFILE,
    HASH_USER_PROFILE,
};